# p4est interface for the Julia language

This is a [Julia] interface to the [p4est] library.
The p4est software library enables the dynamic management
of a collection of adaptive octrees, conveniently called a
forest of octrees. p4est is designed to work in parallel
and scales to hundreds of thousands of processor cores.

[Julia]: http://julialang.org/
[p4est]: http://p4est.github.io/
