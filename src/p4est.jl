module p4est

using MPI

const MPI_Comm    = MPI.CComm
const MPI_Request = MPI.Request
const MPI_Group   = Int32
const MPI_Op      = MPI.Op

# Try to load the binary dependency
if isfile(joinpath(dirname(@__FILE__),"..","deps","deps.jl"))
    include("../deps/deps.jl")
else
    error("p4est not properly installed. Please run Pkg.build(\"p4est\")")
end 

#typedef void        (*sc_log_handler_t) (FILE * log_stream,
#                                         const char *filename, int lineno,
#                                         int package, int category,
#                                         int priority, const char *msg);
#

mutable struct sc_log_handler_t
end
mutable struct FILE
end

include("sc_common.jl")
include("p4est_common.jl")
include("p4est_lib.jl")

end
