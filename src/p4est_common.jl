# Automatically generated using Clang.jl wrap_c, version 0.0.0

using Compat

const p4est_qcoord_t = Int32
const p4est_topidx_t = Int32
const p4est_locidx_t = Int32
const p4est_gloidx_t = Int64

# begin enum p4est_comm_tag
const p4est_comm_tag = UInt32
const P4EST_COMM_TAG_FIRST = (UInt32)(214)
const P4EST_COMM_COUNT_PERTREE = (UInt32)(253)
const P4EST_COMM_BALANCE_FIRST_COUNT = (UInt32)(254)
const P4EST_COMM_BALANCE_FIRST_LOAD = (UInt32)(255)
const P4EST_COMM_BALANCE_SECOND_COUNT = (UInt32)(256)
const P4EST_COMM_BALANCE_SECOND_LOAD = (UInt32)(257)
const P4EST_COMM_PARTITION_GIVEN = (UInt32)(258)
const P4EST_COMM_PARTITION_WEIGHTED_LOW = (UInt32)(259)
const P4EST_COMM_PARTITION_WEIGHTED_HIGH = (UInt32)(260)
const P4EST_COMM_PARTITION_CORRECTION = (UInt32)(261)
const P4EST_COMM_GHOST_COUNT = (UInt32)(262)
const P4EST_COMM_GHOST_LOAD = (UInt32)(263)
const P4EST_COMM_GHOST_EXCHANGE = (UInt32)(264)
const P4EST_COMM_GHOST_EXPAND_COUNT = (UInt32)(265)
const P4EST_COMM_GHOST_EXPAND_LOAD = (UInt32)(266)
const P4EST_COMM_GHOST_SUPPORT_COUNT = (UInt32)(267)
const P4EST_COMM_GHOST_SUPPORT_LOAD = (UInt32)(268)
const P4EST_COMM_GHOST_CHECKSUM = (UInt32)(269)
const P4EST_COMM_NODES_QUERY = (UInt32)(270)
const P4EST_COMM_NODES_REPLY = (UInt32)(271)
const P4EST_COMM_SAVE = (UInt32)(272)
const P4EST_COMM_LNODES_TEST = (UInt32)(273)
const P4EST_COMM_LNODES_PASS = (UInt32)(274)
const P4EST_COMM_LNODES_OWNED = (UInt32)(275)
const P4EST_COMM_LNODES_ALL = (UInt32)(276)
const P4EST_COMM_TAG_LAST = (UInt32)(277)
# end enum p4est_comm_tag

const p4est_comm_tag_t = Void

# begin enum ANONYMOUS_1
const ANONYMOUS_1 = UInt32
const P4EST_CONNECT_FACE = (UInt32)(21)
const P4EST_CONNECT_CORNER = (UInt32)(22)
const P4EST_CONNECT_FULL = (UInt32)(22)
# end enum ANONYMOUS_1

const p4est_connect_type_t = Void
const p4est_connectivity_encode_t = Void

mutable struct p4est_connectivity_t
    num_vertices::p4est_topidx_t
    num_trees::p4est_topidx_t
    num_corners::p4est_topidx_t
    vertices::Ptr{Float64}
    tree_to_vertex::Ptr{p4est_topidx_t}
    tree_attr_bytes::UInt
    tree_to_attr::Cstring
    tree_to_tree::Ptr{p4est_topidx_t}
    tree_to_face::Ptr{Int8}
    tree_to_corner::Ptr{p4est_topidx_t}
    ctt_offset::Ptr{p4est_topidx_t}
    corner_to_tree::Ptr{p4est_topidx_t}
    corner_to_corner::Ptr{Int8}
end

const p4est_corner_transform_t = Void
const p4est_corner_info_t = Void

mutable struct p4est_quadrant_t
    x::p4est_qcoord_t
    y::p4est_qcoord_t
    level::Int8
    pad8::Int8
    pad16::Int16
    p::Void
end

mutable struct p4est_tree_t
    quadrants::sc_array_t
    first_desc::p4est_quadrant_t
    last_desc::p4est_quadrant_t
    quadrants_offset::p4est_locidx_t
    quadrants_per_level::NTuple{31, p4est_locidx_t}
    maxlevel::Int8
end

mutable struct p4est_inspect_t
end

mutable struct p4est_t
    mpicomm::MPI_Comm
    mpisize::Int32
    mpirank::Int32
    mpicomm_owned::Int32
    data_size::UInt
    user_pointer::Ptr{Void}
    revision::Int64
    first_local_tree::p4est_topidx_t
    last_local_tree::p4est_topidx_t
    local_num_quadrants::p4est_locidx_t
    global_num_quadrants::p4est_gloidx_t
    global_first_quadrant::Ptr{p4est_gloidx_t}
    global_first_position::Ptr{p4est_quadrant_t}
    connectivity::Ptr{p4est_connectivity_t}
    trees::Ptr{sc_array_t}
    user_data_pool::Ptr{sc_mempool_t}
    quadrant_pool::Ptr{sc_mempool_t}
    inspect::Ptr{p4est_inspect_t}
end

const p4est_init_t = Ptr{Void}
const p4est_refine_t = Ptr{Void}
const p4est_coarsen_t = Ptr{Void}
const p4est_weight_t = Ptr{Void}
const p8est_connect_type_t = Void
const p8est_connectivity_encode_t = Void

mutable struct p8est_connectivity_t
    num_vertices::p4est_topidx_t
    num_trees::p4est_topidx_t
    num_edges::p4est_topidx_t
    num_corners::p4est_topidx_t
    vertices::Ptr{Float64}
    tree_to_vertex::Ptr{p4est_topidx_t}
    tree_attr_bytes::UInt
    tree_to_attr::Cstring
    tree_to_tree::Ptr{p4est_topidx_t}
    tree_to_face::Ptr{Int8}
    tree_to_edge::Ptr{p4est_topidx_t}
    ett_offset::Ptr{p4est_topidx_t}
    edge_to_tree::Ptr{p4est_topidx_t}
    edge_to_edge::Ptr{Int8}
    tree_to_corner::Ptr{p4est_topidx_t}
    ctt_offset::Ptr{p4est_topidx_t}
    corner_to_tree::Ptr{p4est_topidx_t}
    corner_to_corner::Ptr{Int8}
end

const p8est_edge_transform_t = Void
const p8est_edge_info_t = Void
const p8est_corner_transform_t = Void
const p8est_corner_info_t = Void

mutable struct p6est_connectivity_t
    conn4::Ptr{p4est_connectivity_t}
    top_vertices::Ptr{Float64}
    height::NTuple{3, Float64}
end

mutable struct p2est_quadrant_t
    z::p4est_qcoord_t
    level::Int8
    pad8::Int8
    pad16::Int16
    p::Void
end

mutable struct p6est_t
    mpicomm::MPI_Comm
    mpisize::Int32
    mpirank::Int32
    mpicomm_owned::Int32
    data_size::UInt
    user_pointer::Ptr{Void}
    connectivity::Ptr{p6est_connectivity_t}
    columns::Ptr{p4est_t}
    layers::Ptr{sc_array_t}
    user_data_pool::Ptr{sc_mempool_t}
    layer_pool::Ptr{sc_mempool_t}
    global_first_layer::Ptr{p4est_gloidx_t}
    root_len::p4est_qcoord_t
end

const p6est_init_t = Ptr{Void}
const p6est_replace_t = Ptr{Void}
const p6est_refine_column_t = Ptr{Void}
const p6est_refine_layer_t = Ptr{Void}
const p6est_coarsen_column_t = Ptr{Void}
const p6est_coarsen_layer_t = Ptr{Void}
const p6est_weight_t = Ptr{Void}
const p6est_comm_tag_t = Void
const p4est_ghost_t = Void

mutable struct p4est_ghost_exchange_t
    is_custom::Int32
    is_levels::Int32
    p4est::Ptr{p4est_t}
    ghost::Ptr{p4est_ghost_t}
    minlevel::Int32
    maxlevel::Int32
    data_size::UInt
    ghost_data::Ptr{Void}
    qactive::Ptr{Int32}
    qbuffer::Ptr{Int32}
    requests::sc_array_t
    sbuffers::sc_array_t
    rrequests::sc_array_t
    rbuffers::sc_array_t
end

mutable struct p6est_ghost_t
    mpisize::Int32
    num_trees::p4est_topidx_t
    btype::p4est_connect_type_t
    column_ghost::Ptr{p4est_ghost_t}
    column_layer_offsets::Ptr{sc_array_t}
    ghosts::sc_array_t
    tree_offsets::Ptr{p4est_locidx_t}
    proc_offsets::Ptr{p4est_locidx_t}
    mirrors::sc_array_t
    mirror_tree_offsets::Ptr{p4est_locidx_t}
    mirror_proc_mirrors::Ptr{p4est_locidx_t}
    mirror_proc_offsets::Ptr{p4est_locidx_t}
    mirror_proc_fronts::Ptr{p4est_locidx_t}
    mirror_proc_front_offsets::Ptr{p4est_locidx_t}
end

const p4est_lnodes_code_t = Int8

mutable struct p4est_lnodes_t
    mpicomm::MPI_Comm
    num_local_nodes::p4est_locidx_t
    owned_count::p4est_locidx_t
    global_offset::p4est_gloidx_t
    nonlocal_nodes::Ptr{p4est_gloidx_t}
    sharers::Ptr{sc_array_t}
    global_owned_count::Ptr{p4est_locidx_t}
    degree::Int32
    vnodes::Int32
    num_local_elements::p4est_locidx_t
    face_code::Ptr{p4est_lnodes_code_t}
    element_nodes::Ptr{p4est_locidx_t}
end

mutable struct p4est_lnodes_rank_t
    rank::Int32
    shared_nodes::sc_array_t
    shared_mine_offset::p4est_locidx_t
    shared_mine_count::p4est_locidx_t
    owned_offset::p4est_locidx_t
    owned_count::p4est_locidx_t
end

mutable struct p4est_lnodes_buffer_t
    requests::Ptr{sc_array_t}
    send_buffers::Ptr{sc_array_t}
    recv_buffers::Ptr{sc_array_t}
end

mutable struct p8est_quadrant_t
    x::p4est_qcoord_t
    y::p4est_qcoord_t
    z::p4est_qcoord_t
    level::Int8
    pad8::Int8
    pad16::Int16
    p::Void
end

mutable struct p8est_tree_t
    quadrants::sc_array_t
    first_desc::p8est_quadrant_t
    last_desc::p8est_quadrant_t
    quadrants_offset::p4est_locidx_t
    quadrants_per_level::NTuple{20, p4est_locidx_t}
    maxlevel::Int8
end

mutable struct p8est_inspect_t
end

mutable struct p8est_t
    mpicomm::MPI_Comm
    mpisize::Int32
    mpirank::Int32
    mpicomm_owned::Int32
    data_size::UInt
    user_pointer::Ptr{Void}
    revision::Int64
    first_local_tree::p4est_topidx_t
    last_local_tree::p4est_topidx_t
    local_num_quadrants::p4est_locidx_t
    global_num_quadrants::p4est_gloidx_t
    global_first_quadrant::Ptr{p4est_gloidx_t}
    global_first_position::Ptr{p8est_quadrant_t}
    connectivity::Ptr{p8est_connectivity_t}
    trees::Ptr{sc_array_t}
    user_data_pool::Ptr{sc_mempool_t}
    quadrant_pool::Ptr{sc_mempool_t}
    inspect::Ptr{p8est_inspect_t}
end

const p8est_init_t = Ptr{Void}
const p8est_refine_t = Ptr{Void}
const p8est_coarsen_t = Ptr{Void}
const p8est_weight_t = Ptr{Void}
const p8est_ghost_t = Void

mutable struct p8est_ghost_exchange_t
    is_custom::Int32
    is_levels::Int32
    p4est::Ptr{p8est_t}
    ghost::Ptr{p8est_ghost_t}
    minlevel::Int32
    maxlevel::Int32
    data_size::UInt
    ghost_data::Ptr{Void}
    qactive::Ptr{Int32}
    qbuffer::Ptr{Int32}
    requests::sc_array_t
    sbuffers::sc_array_t
    rrequests::sc_array_t
    rbuffers::sc_array_t
end

const p8est_lnodes_code_t = Int16

mutable struct p8est_lnodes_t
    mpicomm::MPI_Comm
    num_local_nodes::p4est_locidx_t
    owned_count::p4est_locidx_t
    global_offset::p4est_gloidx_t
    nonlocal_nodes::Ptr{p4est_gloidx_t}
    sharers::Ptr{sc_array_t}
    global_owned_count::Ptr{p4est_locidx_t}
    degree::Int32
    vnodes::Int32
    num_local_elements::p4est_locidx_t
    face_code::Ptr{p8est_lnodes_code_t}
    element_nodes::Ptr{p4est_locidx_t}
end

mutable struct p8est_lnodes_rank_t
    rank::Int32
    shared_nodes::sc_array_t
    shared_mine_offset::p4est_locidx_t
    shared_mine_count::p4est_locidx_t
    owned_offset::p4est_locidx_t
    owned_count::p4est_locidx_t
end

mutable struct p8est_lnodes_buffer_t
    requests::Ptr{sc_array_t}
    send_buffers::Ptr{sc_array_t}
    recv_buffers::Ptr{sc_array_t}
end

const p6est_lnodes_t = p8est_lnodes_t
const p6est_lnodes_code_t = p8est_lnodes_code_t
const p6est_lnodes_rank_t = p8est_lnodes_rank_t
const p6est_lnodes_buffer_t = p8est_lnodes_buffer_t

mutable struct p8est_geometry_t
end

const p8est_geometry_X_t = Ptr{Void}
const p8est_geometry_destroy_t = Ptr{Void}
const p4est_mesh_t = Void
const p4est_mesh_face_neighbor_t = Void

mutable struct p4est_geometry_t
end

const p4est_geometry_X_t = Ptr{Void}
const p4est_geometry_destroy_t = Ptr{Void}

mutable struct p4est_iter_volume_info_t
    p4est::Ptr{p4est_t}
    ghost_layer::Ptr{p4est_ghost_t}
    quad::Ptr{p4est_quadrant_t}
    quadid::p4est_locidx_t
    treeid::p4est_topidx_t
end

const p4est_iter_volume_t = Ptr{Void}

mutable struct p4est_iter_face_side_t
    treeid::p4est_topidx_t
    face::Int8
    is_hanging::Int8
    is::Void
end

mutable struct p4est_iter_face_info_t
    p4est::Ptr{p4est_t}
    ghost_layer::Ptr{p4est_ghost_t}
    orientation::Int8
    tree_boundary::Int8
    sides::sc_array_t
end

const p4est_iter_face_t = Ptr{Void}

mutable struct p4est_iter_corner_side_t
    treeid::p4est_topidx_t
    corner::Int8
    is_ghost::Int8
    quad::Ptr{p4est_quadrant_t}
    quadid::p4est_locidx_t
    faces::NTuple{2, Int8}
end

mutable struct p4est_iter_corner_info_t
    p4est::Ptr{p4est_t}
    ghost_layer::Ptr{p4est_ghost_t}
    tree_boundary::Int8
    sides::sc_array_t
end

const p4est_iter_corner_t = Ptr{Void}
const p8est_mesh_t = Void
const p8est_mesh_face_neighbor_t = Void

mutable struct p8est_iter_volume_info_t
    p4est::Ptr{p8est_t}
    ghost_layer::Ptr{p8est_ghost_t}
    quad::Ptr{p8est_quadrant_t}
    quadid::p4est_locidx_t
    treeid::p4est_topidx_t
end

const p8est_iter_volume_t = Ptr{Void}

mutable struct p8est_iter_face_side_t
    treeid::p4est_topidx_t
    face::Int8
    is_hanging::Int8
    is::Void
end

mutable struct p8est_iter_face_info_t
    p4est::Ptr{p8est_t}
    ghost_layer::Ptr{p8est_ghost_t}
    orientation::Int8
    tree_boundary::Int8
    sides::sc_array_t
end

const p8est_iter_face_t = Ptr{Void}

mutable struct p8est_iter_edge_side_t
    treeid::p4est_topidx_t
    edge::Int8
    orientation::Int8
    is_hanging::Int8
    is::Void
    faces::NTuple{2, Int8}
end

mutable struct p8est_iter_edge_info_t
    p4est::Ptr{p8est_t}
    ghost_layer::Ptr{p8est_ghost_t}
    tree_boundary::Int8
    sides::sc_array_t
end

const p8est_iter_edge_t = Ptr{Void}

mutable struct p8est_iter_corner_side_t
    treeid::p4est_topidx_t
    corner::Int8
    is_ghost::Int8
    quad::Ptr{p8est_quadrant_t}
    quadid::p4est_locidx_t
    faces::NTuple{3, Int8}
    edges::NTuple{3, Int8}
end

mutable struct p8est_iter_corner_info_t
    p4est::Ptr{p8est_t}
    ghost_layer::Ptr{p8est_ghost_t}
    tree_boundary::Int8
    sides::sc_array_t
end

const p8est_iter_corner_t = Ptr{Void}
const p8est_replace_t = Ptr{Void}
const p4est_replace_t = Ptr{Void}

mutable struct p8est_tets_t
    nodes::Ptr{sc_array_t}
    tets::Ptr{sc_array_t}
    tet_attributes::Ptr{sc_array_t}
end

const p4est_search_query_t = Ptr{Void}
const p8est_search_query_t = Ptr{Void}
const p8est_traverse_query_t = Ptr{Void}

# begin enum p4est_wrap_flags
const p4est_wrap_flags = UInt32
const P4EST_WRAP_NONE = (UInt32)(0)
const P4EST_WRAP_REFINE = (UInt32)(1)
const P4EST_WRAP_COARSEN = (UInt32)(2)
# end enum p4est_wrap_flags

const p4est_wrap_flags_t = Void

mutable struct p4est_wrap_t
    user_pointer::Ptr{Void}
    hollow::Int32
    coarsen_delay::Int32
    coarsen_affect::Int32
    conn_rc::sc_refcount_t
    conn::Ptr{p4est_connectivity_t}
    conn_owner::Ptr{Void}
    p4est_dim::Int32
    p4est_half::Int32
    p4est_faces::Int32
    p4est_children::Int32
    btype::p4est_connect_type_t
    replace_fn::p4est_replace_t
    p4est::Ptr{p4est_t}
    weight_exponent::Int32
    flags::Ptr{UInt8}
    temp_flags::Ptr{UInt8}
    num_refine_flags::p4est_locidx_t
    inside_counter::p4est_locidx_t
    num_replaced::p4est_locidx_t
    ghost::Ptr{p4est_ghost_t}
    mesh::Ptr{p4est_mesh_t}
    ghost_aux::Ptr{p4est_ghost_t}
    mesh_aux::Ptr{p4est_mesh_t}
    match_aux::Int32
end

mutable struct p4est_wrap_leaf_t
    pp::Ptr{p4est_wrap_t}
    which_tree::p4est_topidx_t
    which_quad::p4est_locidx_t
    local_quad::p4est_locidx_t
    tree::Ptr{p4est_tree_t}
    tquadrants::Ptr{sc_array_t}
    quad::Ptr{p4est_quadrant_t}
    is_mirror::Int32
    mirrors::Ptr{sc_array_t}
    nm::p4est_locidx_t
    next_mirror_quadrant::p4est_locidx_t
end

mutable struct p8est_indep_t
    x::p4est_qcoord_t
    y::p4est_qcoord_t
    z::p4est_qcoord_t
    level::Int8
    pad8::Int8
    pad16::Int16
    p::Void
end

mutable struct p8est_hang2_t
    x::p4est_qcoord_t
    y::p4est_qcoord_t
    z::p4est_qcoord_t
    level::Int8
    pad8::Int8
    pad16::Int16
    p::Void
end

mutable struct p8est_hang4_t
    x::p4est_qcoord_t
    y::p4est_qcoord_t
    z::p4est_qcoord_t
    level::Int8
    pad8::Int8
    pad16::Int16
    p::Void
end

mutable struct p8est_nodes_t
    num_local_quadrants::p4est_locidx_t
    num_owned_indeps::p4est_locidx_t
    num_owned_shared::p4est_locidx_t
    offset_owned_indeps::p4est_locidx_t
    indep_nodes::sc_array_t
    face_hangings::sc_array_t
    edge_hangings::sc_array_t
    local_nodes::Ptr{p4est_locidx_t}
    shared_indeps::sc_array_t
    shared_offsets::Ptr{p4est_locidx_t}
    nonlocal_ranks::Ptr{Int32}
    global_owned_indeps::Ptr{p4est_locidx_t}
end

mutable struct p8est_transfer_context_t
    variable::Int32
    num_senders::Int32
    num_receivers::Int32
    recv_req::Ptr{MPI_Request}
    send_req::Ptr{MPI_Request}
end

mutable struct p4est_indep_t
    x::p4est_qcoord_t
    y::p4est_qcoord_t
    level::Int8
    pad8::Int8
    pad16::Int16
    p::Void
end

mutable struct p4est_hang2_t
    x::p4est_qcoord_t
    y::p4est_qcoord_t
    level::Int8
    pad8::Int8
    pad16::Int16
    p::Void
end

mutable struct p4est_nodes_t
    num_local_quadrants::p4est_locidx_t
    num_owned_indeps::p4est_locidx_t
    num_owned_shared::p4est_locidx_t
    offset_owned_indeps::p4est_locidx_t
    indep_nodes::sc_array_t
    face_hangings::sc_array_t
    local_nodes::Ptr{p4est_locidx_t}
    shared_indeps::sc_array_t
    shared_offsets::Ptr{p4est_locidx_t}
    nonlocal_ranks::Ptr{Int32}
    global_owned_indeps::Ptr{p4est_locidx_t}
end

mutable struct p4est_vtk_context_t
end

# begin enum p8est_wrap_flags
const p8est_wrap_flags = UInt32
const P8EST_WRAP_NONE = (UInt32)(0)
const P8EST_WRAP_REFINE = (UInt32)(1)
const P8EST_WRAP_COARSEN = (UInt32)(2)
# end enum p8est_wrap_flags

const p8est_wrap_flags_t = Void

mutable struct p8est_wrap_t
    user_pointer::Ptr{Void}
    hollow::Int32
    coarsen_delay::Int32
    coarsen_affect::Int32
    conn_rc::sc_refcount_t
    conn::Ptr{p8est_connectivity_t}
    conn_owner::Ptr{Void}
    p4est_dim::Int32
    p4est_half::Int32
    p4est_faces::Int32
    p4est_children::Int32
    btype::p8est_connect_type_t
    replace_fn::p8est_replace_t
    p4est::Ptr{p8est_t}
    weight_exponent::Int32
    flags::Ptr{UInt8}
    temp_flags::Ptr{UInt8}
    num_refine_flags::p4est_locidx_t
    inside_counter::p4est_locidx_t
    num_replaced::p4est_locidx_t
    ghost::Ptr{p8est_ghost_t}
    mesh::Ptr{p8est_mesh_t}
    ghost_aux::Ptr{p8est_ghost_t}
    mesh_aux::Ptr{p8est_mesh_t}
    match_aux::Int32
end

mutable struct p8est_wrap_leaf_t
    pp::Ptr{p8est_wrap_t}
    which_tree::p4est_topidx_t
    which_quad::p4est_locidx_t
    local_quad::p4est_locidx_t
    tree::Ptr{p8est_tree_t}
    tquadrants::Ptr{sc_array_t}
    quad::Ptr{p8est_quadrant_t}
    is_mirror::Int32
    mirrors::Ptr{sc_array_t}
    nm::p4est_locidx_t
    next_mirror_quadrant::p4est_locidx_t
end

mutable struct p4est_transfer_context_t
    variable::Int32
    num_senders::Int32
    num_receivers::Int32
    recv_req::Ptr{MPI_Request}
    send_req::Ptr{MPI_Request}
end

mutable struct p8est_vtk_context_t
end

const p6est_profile_type_t = Void

mutable struct p6est_profile_t
    ptype::p6est_profile_type_t
    btype::p8est_connect_type_t
    lnodes::Ptr{p4est_lnodes_t}
    cghost::Ptr{p4est_ghost_t}
    ghost_owned::Int32
    lnode_ranges::Ptr{p4est_locidx_t}
    lnode_columns::Ptr{sc_array_t}
    lnode_changed::NTuple{2, Ptr{Int32}}
    enode_counts::Ptr{p4est_locidx_t}
    evenodd::Int32
    diff::p4est_qcoord_t
end
