#
# This script will automatically wrap the relevant parts of p4est using Clang.jl (kudos!)
#

using Compat
using Clang.wrap_c
using Clang.cindex

withdebug=false

# Set the dirs
workdir = dirname(@__FILE__)
srcdir = joinpath(workdir, "../src")

p4est_include = "/home/iagbole/Codeentwicklung/p4est/local/include"
mpi_include   = "/opt/openmpi/openmpi-2.1.0/gnu6.3.0/include"

# Set up include paths
clang_includes = String[]
push!(clang_includes, p4est_include)
push!(clang_includes, mpi_include)
push!(clang_includes, "/usr/include")

# Collect all header files, exclude p4est_to_p8est
p4est_header=Array{String,1}(split(readstring(`find $p4est_include -iname 'p*est*.h'`),"\n"))
deleteat!(p4est_header,length(p4est_header))
for (i,v) in enumerate(p4est_header)
  if contains(v,"p4est_to_p8est")
    deleteat!(p4est_header,i)
  end
end

# Clang arguments
clang_extraargs = ["-v"]
#clang_extraargs = ["-D", "__STDC_LIMIT_MACROS", "-D", "__STDC_CONSTANT_MACROS"]


namelist= String[]
structs = CLCursor[]
# Check if cursor should be wrapped
function wrap_cursor(name::String, cursor)

  # do not wrap macros
  if(typeof(cursor) == Clang.cindex.MacroDefinition ||
     typeof(cursor) == Clang.cindex.MacroInstantiation )
    return false
  end

  # do not wrap twice
  if !(name in namelist)
    push!(namelist,name) 
  else
    return false
  end

  # do not wrap struct typedefs
  istypedefstruct = false
  if(typeof(cursor) == Clang.cindex.StructDecl )
    push!(structs,cursor)
  end
  if(typeof(cursor) == Clang.cindex.TypedefDecl )
    for s in structs
      if cindex.name(s)*"_t" == cindex.name(cursor)
        istypedefstruct=true
      end
    end
  end
  return !istypedefstruct
end

# Callback to test if a header should actually be wrapped (for exclusion)
flag = "p"
function wrap_header(top_hdr::String, cursor_header::String)
  return startswith(cursor_header, joinpath(p4est_include,flag))
end

## 1. variant: drop _t, make types caps, the more Julian way
#function rewriter(e)
#  for ex in e
#    if typeof(ex) == Expr
#      rewriter(ex)
#    end
#  end
#  e
#end
#function rewriter2(e::Expr)
#  for (i,ex) in enumerate(e.args)
#    if typeof(ex) == Expr
#      rewriter(ex)
#    elseif typeof(ex) == Symbol
#      for s in structs
#        if ex == Symbol(cindex.name(s)*"_t")
#          e.args[i] = Symbol(cindex.name(s))
#        end 
#      end
#    end
#  end
#  e
#end

 # WARNING: These mappings are for better readability, 
 #          but may cause problems on some architectures
c2j = Dict(:Cint   =>:Int32,
           :Csize_t=>:UInt,
           :Cdouble=>:Float64,
           :Clong  =>:Int64,
           :Clonglong =>:Int64)

# 2. variant: add _t to structs, the simple way
function rewriter(e)
  for ex in e
    if typeof(ex) == Expr
      rewriter(ex)
    end
  end
  e
end
function rewriter(e::Expr)
  # rename structs to C type names
  if e.head == :type
    for (i,ex) in enumerate(e.args)
      if typeof(ex) == Symbol
        for s in structs
          if ex == Symbol(cindex.name(s))
            e.args[i] = Symbol(cindex.name(s)*"_t")
          end 
        end
      end
    end
  end
  # replace C wrapper types by Julia standard types
  for (i,ex) in enumerate(e.args)
    if typeof(ex) == Expr
      rewriter(ex)
    elseif typeof(ex) == Symbol
      a=get(c2j,ex,0)
      if a != 0
        e.args[i] = a
      end
    end
  end
  e
end

ctxp4 = wrap_c.init( 
                      headers             = p4est_header,
                      output_dir          = srcdir,
                      output_file         = "p4est_lib.jl",
                      common_file         = "p4est_common.jl",
                      clang_includes      = clang_includes,
                      clang_args          = clang_extraargs,
                      header_wrapped      = wrap_header,
                      rewriter            = rewriter,
                      header_library      = x->"libp4est",
#                      header_outputfile   = x->"p4est_func.jl",
                      cursor_wrapped      = wrap_cursor,
                      clang_diagnostics   = withdebug)

run(ctxp4)


# Now wrap SC

# TODO: search for sc stuff in p4est (maybe in wrap_header routine?) and add them to a list
#       only wrap relevant parts of sc
#       we do not need libsc.jl

sc_header=Array{String,1}(split(readstring(`find $p4est_include -iname 'sc_*.h'`),"\n"))
deleteat!(sc_header,length(sc_header))
flag = "sc_"
namelist=String[]

ctxsc = wrap_c.init( 
                      headers             = sc_header,
                      output_dir          = srcdir,
                      output_file         = "sc_dummy.jl",
                      common_file         = "sc_common.jl", # we only want the common file
                      clang_includes      = clang_includes,
                      clang_args          = clang_extraargs,
                      header_wrapped      = wrap_header,
                      rewriter            = rewriter,
                      header_library      = x->"libsc",
                      #header_outputfile   = x->"sc_func.jl",
                      cursor_wrapped      = wrap_cursor,
                      clang_diagnostics   = withdebug)

run(ctxsc)

rm(joinpath(srcdir, "sc_dummy.jl")) # delete sc funcs
