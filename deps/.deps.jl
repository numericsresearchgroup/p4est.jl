# This is an auto-generated file; do not edit

# Pre-hooks
Base.Libdl.dlopen_e("/home/iagbole/.julia/v0.6/p4est/deps/usr/lib/libsc.so",Libdl.RTLD_LAZY | Libdl.RTLD_GLOBAL)

# Macro to load a library
macro checked_lib(libname, path)
    ((VERSION >= v"0.4.0-dev+3844" ? Base.Libdl.dlopen_e : Base.dlopen_e)(path) == C_NULL) && error("Unable to load \n\n$libname ($path)\n\nPlease re-run Pkg.build(package), and restart Julia.")
    quote const $(esc(libname)) = $path end
end

# Load dependencies
@checked_lib libp4est "/home/iagbole/.julia/v0.6/p4est/deps/usr/lib/libp4est.so"

# Load-hooks

