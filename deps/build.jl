using Compat
using BinDeps

# Define SHA of sc
sc_path = "https://github.com/cburstedde/libsc.git"
sc_sha="9bc1129b42e73014e84d8304b68d7338e139a116"
# Define SHA of p4est
p4est_path = "https://github.com/cburstedde/p4est.git"
p4est_sha  = "1d49bc659263c6647aaa708b858fcabbe8c2d527"

@BinDeps.setup

libsc    = library_dependency("libsc",    aliases=["sc"])

if is_windows()
  error("The p4est wrapper does not provide Windows support.")
end

depsdir  = BinDeps.depsdir(libsc)
builddir = joinpath(depsdir, "build")
prefix   = joinpath(depsdir, "usr")
src      = joinpath(depsdir, "src")

if isdir(src)
  rm(src, recursive = true)
end
if isdir(prefix)
  rm(prefix, recursive = true)
end
mkdir(src)
mkdir(prefix)


LibGit2.clone(p4est_path, "$src")
cd(src) do
  LibGit2.checkout!(LibGit2.GitRepo("."), "$p4est_sha")
  run(`git submodule init`)
  run(`git submodule update`)
  run(`./bootstrap`)
end

opts = ["--enable-mpi"]
build_procs = min(4,Sys.CPU_CORES)
provides(BuildProcess,
     (@build_steps begin
       CreateDirectory(builddir)
       (@build_steps begin
         ChangeDirectory(builddir)
         `$src/configure --prefix=$prefix $opts`
         `make -j $build_procs install`
       end)
     end),
     libsc)

@BinDeps.install Dict(:libsc => :libsc)

# We have now built p4est and sc and now want to install p4est. Dis is très ugly

depsfile = joinpath(splitdir(Base.source_path())[1],"deps.jl")
f=open(depsfile)
lines=readlines(f)
close(f)
for (i,ln) in enumerate(lines)
  lines[i]=replace(ln,"(path)","(path,Base.Libdl.RTLD_LAZY|Base.Libdl.RTLD_DEEPBIND|Base.Libdl.RTLD_GLOBAL)")
  if startswith(ln,"@checked_lib libsc")
    push!(lines,replace(ln,"libsc","libp4est"))
  end
end
f=open(depsfile,"w")
for ln in lines
  write(f, ln*"\n")
end
close(f)


# TODO: provide a clean bindeps version, optimally by dynamically linking sc to p4est

#@BinDeps.setup
#
#
#group = library_group("p4est")
#
#deps = [
#         libsc    = library_dependency("libsc",    aliases=["sc"],                     group = group)
#         libp4est = library_dependency("libp4est", aliases=["p4est"], depends=[libsc], group = group)
#       ]
#
#if is_windows()
#  error("The p4est wrapper does not provide Windows support.")
#end
#
#depsdir  = BinDeps.depsdir(libp4est)
#builddir = joinpath(depsdir, "build")
#prefix   = joinpath(depsdir, "usr")
#src      = joinpath(depsdir, "src")
#
##if isdir(src)
##  rm(src, recursive = true)
##end
##if isdir(prefix)
##  rm(prefix, recursive = true)
##end
##mkdir(src)
##mkdir(prefix)
#
##LibGit2.clone("https://github.com/cburstedde/p4est.git", "$src")
##cd(src) do
##  LibGit2.checkout!(LibGit2.GitRepo("."), "$p4est_sha")
##  run(`git submodule init`)
##  run(`git submodule update`)
##  run(`./bootstrap`)
##end
#
#opts = ["--enable-mpi"]
#build_procs = (haskey(ENV, "CI") && ENV["CI"] == "true") ? 2 : Sys.CPU_CORES
#provides(BuildProcess,
#     (@build_steps begin
#       CreateDirectory(builddir)
#       (@build_steps begin
#         ChangeDirectory(builddir)
#                     (@build_steps begin
#                     setenv(`$src/configure --prefix=$prefix $opts`,env)
#                     setenv(`make -j $build_procs install LDFLAGS='-Wl,-rpath,\$$ORIGIN'`, env)
#         end)
#       end)
#     end),
#     [libsc,libp4est]) # this fails for p4est not being able to load sc
#
#@BinDeps.install Dict([:libsc    => :libsc,
#                       :libp4est => :libp4est])
