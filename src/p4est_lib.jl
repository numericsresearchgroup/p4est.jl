# Julia wrapper for header: /home/iagbole/Codeentwicklung/p4est/local/include/p6est_lnodes.h
# Automatically generated using Clang.jl wrap_c, version 0.0.0


function sc_extern_c_hack_3()
    ccall((:sc_extern_c_hack_3, libp4est), Void, ())
end

function p4est_log_indent_push()
    ccall((:p4est_log_indent_push, libp4est), Void, ())
end

function p4est_log_indent_pop()
    ccall((:p4est_log_indent_pop, libp4est), Void, ())
end

function p4est_init(log_handler::sc_log_handler_t, log_threshold::Int32)
    ccall((:p4est_init, libp4est), Void, (sc_log_handler_t, Int32), log_handler, log_threshold)
end

function p4est_topidx_hash2(tt)
    ccall((:p4est_topidx_hash2, libp4est), UInt32, (Ptr{p4est_topidx_t},), tt)
end

function p4est_topidx_hash3(tt)
    ccall((:p4est_topidx_hash3, libp4est), UInt32, (Ptr{p4est_topidx_t},), tt)
end

function p4est_topidx_hash4(tt)
    ccall((:p4est_topidx_hash4, libp4est), UInt32, (Ptr{p4est_topidx_t},), tt)
end

function p4est_topidx_is_sorted(t, length::Int32)
    ccall((:p4est_topidx_is_sorted, libp4est), Int32, (Ptr{p4est_topidx_t}, Int32), t, length)
end

function p4est_topidx_bsort(t, length::Int32)
    ccall((:p4est_topidx_bsort, libp4est), Void, (Ptr{p4est_topidx_t}, Int32), t, length)
end

function p4est_partition_cut_uint64(global_num::UInt64, p::Int32, num_procs::Int32)
    ccall((:p4est_partition_cut_uint64, libp4est), UInt64, (UInt64, Int32, Int32), global_num, p, num_procs)
end

function p4est_partition_cut_gloidx(global_num::p4est_gloidx_t, p::Int32, num_procs::Int32)
    ccall((:p4est_partition_cut_gloidx, libp4est), p4est_gloidx_t, (p4est_gloidx_t, Int32, Int32), global_num, p, num_procs)
end

function sc_extern_c_hack_4()
    ccall((:sc_extern_c_hack_4, libp4est), Void, ())
end

function p4est_connect_type_int(btype::p4est_connect_type_t)
    ccall((:p4est_connect_type_int, libp4est), Int32, (p4est_connect_type_t,), btype)
end

function p4est_connect_type_string(btype::p4est_connect_type_t)
    ccall((:p4est_connect_type_string, libp4est), Cstring, (p4est_connect_type_t,), btype)
end

function p4est_connectivity_memory_used()
    ccall((:p4est_connectivity_memory_used, libp4est), Int32, ())
end

function p4est_connectivity_face_neighbor_face_corner_orientation(fc::Int32, f::Int32, nf::Int32, o::Int32)
    ccall((:p4est_connectivity_face_neighbor_face_corner_orientation, libp4est), Int32, (Int32, Int32, Int32, Int32), fc, f, nf, o)
end

function p4est_connectivity_face_neighbor_corner_orientation(c::Int32, f::Int32, nf::Int32, o::Int32)
    ccall((:p4est_connectivity_face_neighbor_corner_orientation, libp4est), Int32, (Int32, Int32, Int32, Int32), c, f, nf, o)
end

function p4est_connectivity_new(num_vertices::p4est_topidx_t, num_trees::p4est_topidx_t, num_corners::p4est_topidx_t, num_ctt::p4est_topidx_t)
    ccall((:p4est_connectivity_new, libp4est), Ptr{p4est_connectivity_t}, (p4est_topidx_t, p4est_topidx_t, p4est_topidx_t, p4est_topidx_t), num_vertices, num_trees, num_corners, num_ctt)
end

function p4est_connectivity_new_copy(num_vertices::p4est_topidx_t, num_trees::p4est_topidx_t, num_corners::p4est_topidx_t, vertices, ttv, ttt, ttf, ttc, coff, ctt, ctc)
    ccall((:p4est_connectivity_new_copy, libp4est), Ptr{p4est_connectivity_t}, (p4est_topidx_t, p4est_topidx_t, p4est_topidx_t, Ptr{Float64}, Ptr{p4est_topidx_t}, Ptr{p4est_topidx_t}, Ptr{Int8}, Ptr{p4est_topidx_t}, Ptr{p4est_topidx_t}, Ptr{p4est_topidx_t}, Ptr{Int8}), num_vertices, num_trees, num_corners, vertices, ttv, ttt, ttf, ttc, coff, ctt, ctc)
end

function p4est_connectivity_bcast(conn_in, root::Int32, comm::MPI_Comm)
    ccall((:p4est_connectivity_bcast, libp4est), Ptr{p4est_connectivity_t}, (Ptr{p4est_connectivity_t}, Int32, MPI_Comm), conn_in, root, comm)
end

function p4est_connectivity_destroy(connectivity)
    ccall((:p4est_connectivity_destroy, libp4est), Void, (Ptr{p4est_connectivity_t},), connectivity)
end

function p4est_connectivity_set_attr(conn, bytes_per_tree::UInt)
    ccall((:p4est_connectivity_set_attr, libp4est), Void, (Ptr{p4est_connectivity_t}, UInt), conn, bytes_per_tree)
end

function p4est_connectivity_is_valid(connectivity)
    ccall((:p4est_connectivity_is_valid, libp4est), Int32, (Ptr{p4est_connectivity_t},), connectivity)
end

function p4est_connectivity_is_equal(conn1, conn2)
    ccall((:p4est_connectivity_is_equal, libp4est), Int32, (Ptr{p4est_connectivity_t}, Ptr{p4est_connectivity_t}), conn1, conn2)
end

function p4est_connectivity_sink(conn, sink)
    ccall((:p4est_connectivity_sink, libp4est), Int32, (Ptr{p4est_connectivity_t}, Ptr{sc_io_sink_t}), conn, sink)
end

function p4est_connectivity_deflate(conn, code::p4est_connectivity_encode_t)
    ccall((:p4est_connectivity_deflate, libp4est), Ptr{sc_array_t}, (Ptr{p4est_connectivity_t}, p4est_connectivity_encode_t), conn, code)
end

function p4est_connectivity_save(filename, connectivity)
    ccall((:p4est_connectivity_save, libp4est), Int32, (Cstring, Ptr{p4est_connectivity_t}), filename, connectivity)
end

function p4est_connectivity_source(source)
    ccall((:p4est_connectivity_source, libp4est), Ptr{p4est_connectivity_t}, (Ptr{sc_io_source_t},), source)
end

function p4est_connectivity_inflate(buffer)
    ccall((:p4est_connectivity_inflate, libp4est), Ptr{p4est_connectivity_t}, (Ptr{sc_array_t},), buffer)
end

function p4est_connectivity_load(filename, bytes)
    ccall((:p4est_connectivity_load, libp4est), Ptr{p4est_connectivity_t}, (Cstring, Ptr{UInt}), filename, bytes)
end

function p4est_connectivity_new_unitsquare()
    ccall((:p4est_connectivity_new_unitsquare, libp4est), Ptr{p4est_connectivity_t}, ())
end

function p4est_connectivity_new_periodic()
    ccall((:p4est_connectivity_new_periodic, libp4est), Ptr{p4est_connectivity_t}, ())
end

function p4est_connectivity_new_rotwrap()
    ccall((:p4est_connectivity_new_rotwrap, libp4est), Ptr{p4est_connectivity_t}, ())
end

function p4est_connectivity_new_twotrees(l_face::Int32, r_face::Int32, orientation::Int32)
    ccall((:p4est_connectivity_new_twotrees, libp4est), Ptr{p4est_connectivity_t}, (Int32, Int32, Int32), l_face, r_face, orientation)
end

function p4est_connectivity_new_corner()
    ccall((:p4est_connectivity_new_corner, libp4est), Ptr{p4est_connectivity_t}, ())
end

function p4est_connectivity_new_pillow()
    ccall((:p4est_connectivity_new_pillow, libp4est), Ptr{p4est_connectivity_t}, ())
end

function p4est_connectivity_new_moebius()
    ccall((:p4est_connectivity_new_moebius, libp4est), Ptr{p4est_connectivity_t}, ())
end

function p4est_connectivity_new_star()
    ccall((:p4est_connectivity_new_star, libp4est), Ptr{p4est_connectivity_t}, ())
end

function p4est_connectivity_new_cubed()
    ccall((:p4est_connectivity_new_cubed, libp4est), Ptr{p4est_connectivity_t}, ())
end

function p4est_connectivity_new_disk()
    ccall((:p4est_connectivity_new_disk, libp4est), Ptr{p4est_connectivity_t}, ())
end

function p4est_connectivity_new_brick(mi::Int32, ni::Int32, periodic_a::Int32, periodic_b::Int32)
    ccall((:p4est_connectivity_new_brick, libp4est), Ptr{p4est_connectivity_t}, (Int32, Int32, Int32, Int32), mi, ni, periodic_a, periodic_b)
end

function p4est_connectivity_new_byname(name)
    ccall((:p4est_connectivity_new_byname, libp4est), Ptr{p4est_connectivity_t}, (Cstring,), name)
end

function p4est_connectivity_refine(conn, num_per_edge::Int32)
    ccall((:p4est_connectivity_refine, libp4est), Ptr{p4est_connectivity_t}, (Ptr{p4est_connectivity_t}, Int32), conn, num_per_edge)
end

function p4est_expand_face_transform(iface::Int32, nface::Int32, ftransform)
    ccall((:p4est_expand_face_transform, libp4est), Void, (Int32, Int32, Ptr{Int32}), iface, nface, ftransform)
end

function p4est_find_face_transform(connectivity, itree::p4est_topidx_t, iface::Int32, ftransform)
    ccall((:p4est_find_face_transform, libp4est), p4est_topidx_t, (Ptr{p4est_connectivity_t}, p4est_topidx_t, Int32, Ptr{Int32}), connectivity, itree, iface, ftransform)
end

function p4est_find_corner_transform(connectivity, itree::p4est_topidx_t, icorner::Int32, ci)
    ccall((:p4est_find_corner_transform, libp4est), Void, (Ptr{p4est_connectivity_t}, p4est_topidx_t, Int32, Ptr{p4est_corner_info_t}), connectivity, itree, icorner, ci)
end

function p4est_connectivity_complete(conn)
    ccall((:p4est_connectivity_complete, libp4est), Void, (Ptr{p4est_connectivity_t},), conn)
end

function p4est_connectivity_reduce(conn)
    ccall((:p4est_connectivity_reduce, libp4est), Void, (Ptr{p4est_connectivity_t},), conn)
end

function p4est_connectivity_permute(conn, perm, is_current_to_new::Int32)
    ccall((:p4est_connectivity_permute, libp4est), Void, (Ptr{p4est_connectivity_t}, Ptr{sc_array_t}, Int32), conn, perm, is_current_to_new)
end

function p4est_connectivity_join_faces(conn, tree_left::p4est_topidx_t, tree_right::p4est_topidx_t, face_left::Int32, face_right::Int32, orientation::Int32)
    ccall((:p4est_connectivity_join_faces, libp4est), Void, (Ptr{p4est_connectivity_t}, p4est_topidx_t, p4est_topidx_t, Int32, Int32, Int32), conn, tree_left, tree_right, face_left, face_right, orientation)
end

function p4est_connectivity_is_equivalent(conn1, conn2)
    ccall((:p4est_connectivity_is_equivalent, libp4est), Int32, (Ptr{p4est_connectivity_t}, Ptr{p4est_connectivity_t}), conn1, conn2)
end

function p4est_corner_array_index(array, it::UInt)
    ccall((:p4est_corner_array_index, libp4est), Ptr{p4est_corner_transform_t}, (Ptr{sc_array_t}, UInt), array, it)
end

function p4est_connectivity_read_inp_stream(stream, num_vertices, num_trees, vertices, tree_to_vertex)
    ccall((:p4est_connectivity_read_inp_stream, libp4est), Int32, (Ptr{FILE}, Ptr{p4est_topidx_t}, Ptr{p4est_topidx_t}, Ptr{Float64}, Ptr{p4est_topidx_t}), stream, num_vertices, num_trees, vertices, tree_to_vertex)
end

function p4est_connectivity_read_inp(filename)
    ccall((:p4est_connectivity_read_inp, libp4est), Ptr{p4est_connectivity_t}, (Cstring,), filename)
end

function p4est_memory_used()
    ccall((:p4est_memory_used, libp4est), Int32, ())
end

function p4est_revision(p4est)
    ccall((:p4est_revision, libp4est), Int64, (Ptr{p4est_t},), p4est)
end

function p4est_qcoord_to_vertex(connectivity, treeid::p4est_topidx_t, x::p4est_qcoord_t, y::p4est_qcoord_t, vxyz::NTuple{3, Float64})
    ccall((:p4est_qcoord_to_vertex, libp4est), Void, (Ptr{p4est_connectivity_t}, p4est_topidx_t, p4est_qcoord_t, p4est_qcoord_t, NTuple{3, Float64}), connectivity, treeid, x, y, vxyz)
end

function p4est_new(mpicomm::MPI_Comm, connectivity, data_size::UInt, init_fn::p4est_init_t, user_pointer)
    ccall((:p4est_new, libp4est), Ptr{p4est_t}, (MPI_Comm, Ptr{p4est_connectivity_t}, UInt, p4est_init_t, Ptr{Void}), mpicomm, connectivity, data_size, init_fn, user_pointer)
end

function p4est_destroy(p4est)
    ccall((:p4est_destroy, libp4est), Void, (Ptr{p4est_t},), p4est)
end

function p4est_copy(input, copy_data::Int32)
    ccall((:p4est_copy, libp4est), Ptr{p4est_t}, (Ptr{p4est_t}, Int32), input, copy_data)
end

function p4est_reset_data(p4est, data_size::UInt, init_fn::p4est_init_t, user_pointer)
    ccall((:p4est_reset_data, libp4est), Void, (Ptr{p4est_t}, UInt, p4est_init_t, Ptr{Void}), p4est, data_size, init_fn, user_pointer)
end

function p4est_refine(p4est, refine_recursive::Int32, refine_fn::p4est_refine_t, init_fn::p4est_init_t)
    ccall((:p4est_refine, libp4est), Void, (Ptr{p4est_t}, Int32, p4est_refine_t, p4est_init_t), p4est, refine_recursive, refine_fn, init_fn)
end

function p4est_coarsen(p4est, coarsen_recursive::Int32, coarsen_fn::p4est_coarsen_t, init_fn::p4est_init_t)
    ccall((:p4est_coarsen, libp4est), Void, (Ptr{p4est_t}, Int32, p4est_coarsen_t, p4est_init_t), p4est, coarsen_recursive, coarsen_fn, init_fn)
end

function p4est_balance(p4est, btype::p4est_connect_type_t, init_fn::p4est_init_t)
    ccall((:p4est_balance, libp4est), Void, (Ptr{p4est_t}, p4est_connect_type_t, p4est_init_t), p4est, btype, init_fn)
end

function p4est_partition(p4est, allow_for_coarsening::Int32, weight_fn::p4est_weight_t)
    ccall((:p4est_partition, libp4est), Void, (Ptr{p4est_t}, Int32, p4est_weight_t), p4est, allow_for_coarsening, weight_fn)
end

function p4est_checksum(p4est)
    ccall((:p4est_checksum, libp4est), UInt32, (Ptr{p4est_t},), p4est)
end

function p4est_save(filename, p4est, save_data::Int32)
    ccall((:p4est_save, libp4est), Void, (Cstring, Ptr{p4est_t}, Int32), filename, p4est, save_data)
end

function p4est_load(filename, mpicomm::MPI_Comm, data_size::UInt, load_data::Int32, user_pointer, connectivity)
    ccall((:p4est_load, libp4est), Ptr{p4est_t}, (Cstring, MPI_Comm, UInt, Int32, Ptr{Void}, Ptr{Ptr{p4est_connectivity_t}}), filename, mpicomm, data_size, load_data, user_pointer, connectivity)
end

function p4est_tree_array_index(array, it::p4est_topidx_t)
    ccall((:p4est_tree_array_index, libp4est), Ptr{p4est_tree_t}, (Ptr{sc_array_t}, p4est_topidx_t), array, it)
end

function p4est_quadrant_array_index(array, it::UInt)
    ccall((:p4est_quadrant_array_index, libp4est), Ptr{p4est_quadrant_t}, (Ptr{sc_array_t}, UInt), array, it)
end

function p4est_quadrant_array_push(array)
    ccall((:p4est_quadrant_array_push, libp4est), Ptr{p4est_quadrant_t}, (Ptr{sc_array_t},), array)
end

function p4est_quadrant_mempool_alloc(mempool)
    ccall((:p4est_quadrant_mempool_alloc, libp4est), Ptr{p4est_quadrant_t}, (Ptr{sc_mempool_t},), mempool)
end

function p4est_quadrant_list_pop(list)
    ccall((:p4est_quadrant_list_pop, libp4est), Ptr{p4est_quadrant_t}, (Ptr{sc_list_t},), list)
end

function p8est_connect_type_int(btype::p8est_connect_type_t)
    ccall((:p8est_connect_type_int, libp4est), Int32, (p8est_connect_type_t,), btype)
end

function p8est_connect_type_string(btype::p8est_connect_type_t)
    ccall((:p8est_connect_type_string, libp4est), Cstring, (p8est_connect_type_t,), btype)
end

function p8est_connectivity_memory_used()
    ccall((:p8est_connectivity_memory_used, libp4est), Int32, ())
end

function p8est_connectivity_face_neighbor_corner_set(c::Int32, f::Int32, nf::Int32, set::Int32)
    ccall((:p8est_connectivity_face_neighbor_corner_set, libp4est), Int32, (Int32, Int32, Int32, Int32), c, f, nf, set)
end

function p8est_connectivity_face_neighbor_face_corner_orientation(fc::Int32, f::Int32, nf::Int32, o::Int32)
    ccall((:p8est_connectivity_face_neighbor_face_corner_orientation, libp4est), Int32, (Int32, Int32, Int32, Int32), fc, f, nf, o)
end

function p8est_connectivity_face_neighbor_corner_orientation(c::Int32, f::Int32, nf::Int32, o::Int32)
    ccall((:p8est_connectivity_face_neighbor_corner_orientation, libp4est), Int32, (Int32, Int32, Int32, Int32), c, f, nf, o)
end

function p8est_connectivity_face_neighbor_edge_orientation(e::Int32, f::Int32, nf::Int32, o::Int32)
    ccall((:p8est_connectivity_face_neighbor_edge_orientation, libp4est), Int32, (Int32, Int32, Int32, Int32), e, f, nf, o)
end

function p8est_connectivity_edge_neighbor_edge_corner_orientation(ec::Int32, e::Int32, ne::Int32, o::Int32)
    ccall((:p8est_connectivity_edge_neighbor_edge_corner_orientation, libp4est), Int32, (Int32, Int32, Int32, Int32), ec, e, ne, o)
end

function p8est_connectivity_edge_neighbor_corner_orientation(c::Int32, e::Int32, ne::Int32, o::Int32)
    ccall((:p8est_connectivity_edge_neighbor_corner_orientation, libp4est), Int32, (Int32, Int32, Int32, Int32), c, e, ne, o)
end

function p8est_connectivity_new(num_vertices::p4est_topidx_t, num_trees::p4est_topidx_t, num_edges::p4est_topidx_t, num_ett::p4est_topidx_t, num_corners::p4est_topidx_t, num_ctt::p4est_topidx_t)
    ccall((:p8est_connectivity_new, libp4est), Ptr{p8est_connectivity_t}, (p4est_topidx_t, p4est_topidx_t, p4est_topidx_t, p4est_topidx_t, p4est_topidx_t, p4est_topidx_t), num_vertices, num_trees, num_edges, num_ett, num_corners, num_ctt)
end

function p8est_connectivity_new_copy(num_vertices::p4est_topidx_t, num_trees::p4est_topidx_t, num_edges::p4est_topidx_t, num_corners::p4est_topidx_t, vertices, ttv, ttt, ttf, tte, eoff, ett, ete, ttc, coff, ctt, ctc)
    ccall((:p8est_connectivity_new_copy, libp4est), Ptr{p8est_connectivity_t}, (p4est_topidx_t, p4est_topidx_t, p4est_topidx_t, p4est_topidx_t, Ptr{Float64}, Ptr{p4est_topidx_t}, Ptr{p4est_topidx_t}, Ptr{Int8}, Ptr{p4est_topidx_t}, Ptr{p4est_topidx_t}, Ptr{p4est_topidx_t}, Ptr{Int8}, Ptr{p4est_topidx_t}, Ptr{p4est_topidx_t}, Ptr{p4est_topidx_t}, Ptr{Int8}), num_vertices, num_trees, num_edges, num_corners, vertices, ttv, ttt, ttf, tte, eoff, ett, ete, ttc, coff, ctt, ctc)
end

function p8est_connectivity_bcast(conn_in, root::Int32, comm::MPI_Comm)
    ccall((:p8est_connectivity_bcast, libp4est), Ptr{p8est_connectivity_t}, (Ptr{p8est_connectivity_t}, Int32, MPI_Comm), conn_in, root, comm)
end

function p8est_connectivity_destroy(connectivity)
    ccall((:p8est_connectivity_destroy, libp4est), Void, (Ptr{p8est_connectivity_t},), connectivity)
end

function p8est_connectivity_set_attr(conn, bytes_per_tree::UInt)
    ccall((:p8est_connectivity_set_attr, libp4est), Void, (Ptr{p8est_connectivity_t}, UInt), conn, bytes_per_tree)
end

function p8est_connectivity_is_valid(connectivity)
    ccall((:p8est_connectivity_is_valid, libp4est), Int32, (Ptr{p8est_connectivity_t},), connectivity)
end

function p8est_connectivity_is_equal(conn1, conn2)
    ccall((:p8est_connectivity_is_equal, libp4est), Int32, (Ptr{p8est_connectivity_t}, Ptr{p8est_connectivity_t}), conn1, conn2)
end

function p8est_connectivity_sink(conn, sink)
    ccall((:p8est_connectivity_sink, libp4est), Int32, (Ptr{p8est_connectivity_t}, Ptr{sc_io_sink_t}), conn, sink)
end

function p8est_connectivity_deflate(conn, code::p8est_connectivity_encode_t)
    ccall((:p8est_connectivity_deflate, libp4est), Ptr{sc_array_t}, (Ptr{p8est_connectivity_t}, p8est_connectivity_encode_t), conn, code)
end

function p8est_connectivity_save(filename, connectivity)
    ccall((:p8est_connectivity_save, libp4est), Int32, (Cstring, Ptr{p8est_connectivity_t}), filename, connectivity)
end

function p8est_connectivity_source(source)
    ccall((:p8est_connectivity_source, libp4est), Ptr{p8est_connectivity_t}, (Ptr{sc_io_source_t},), source)
end

function p8est_connectivity_inflate(buffer)
    ccall((:p8est_connectivity_inflate, libp4est), Ptr{p8est_connectivity_t}, (Ptr{sc_array_t},), buffer)
end

function p8est_connectivity_load(filename, bytes)
    ccall((:p8est_connectivity_load, libp4est), Ptr{p8est_connectivity_t}, (Cstring, Ptr{UInt}), filename, bytes)
end

function p8est_connectivity_new_unitcube()
    ccall((:p8est_connectivity_new_unitcube, libp4est), Ptr{p8est_connectivity_t}, ())
end

function p8est_connectivity_new_periodic()
    ccall((:p8est_connectivity_new_periodic, libp4est), Ptr{p8est_connectivity_t}, ())
end

function p8est_connectivity_new_rotwrap()
    ccall((:p8est_connectivity_new_rotwrap, libp4est), Ptr{p8est_connectivity_t}, ())
end

function p8est_connectivity_new_twocubes()
    ccall((:p8est_connectivity_new_twocubes, libp4est), Ptr{p8est_connectivity_t}, ())
end

function p8est_connectivity_new_twotrees(l_face::Int32, r_face::Int32, orientation::Int32)
    ccall((:p8est_connectivity_new_twotrees, libp4est), Ptr{p8est_connectivity_t}, (Int32, Int32, Int32), l_face, r_face, orientation)
end

function p8est_connectivity_new_twowrap()
    ccall((:p8est_connectivity_new_twowrap, libp4est), Ptr{p8est_connectivity_t}, ())
end

function p8est_connectivity_new_rotcubes()
    ccall((:p8est_connectivity_new_rotcubes, libp4est), Ptr{p8est_connectivity_t}, ())
end

function p8est_connectivity_new_brick(m::Int32, n::Int32, p::Int32, periodic_a::Int32, periodic_b::Int32, periodic_c::Int32)
    ccall((:p8est_connectivity_new_brick, libp4est), Ptr{p8est_connectivity_t}, (Int32, Int32, Int32, Int32, Int32, Int32), m, n, p, periodic_a, periodic_b, periodic_c)
end

function p8est_connectivity_new_shell()
    ccall((:p8est_connectivity_new_shell, libp4est), Ptr{p8est_connectivity_t}, ())
end

function p8est_connectivity_new_sphere()
    ccall((:p8est_connectivity_new_sphere, libp4est), Ptr{p8est_connectivity_t}, ())
end

function p8est_connectivity_new_byname(name)
    ccall((:p8est_connectivity_new_byname, libp4est), Ptr{p8est_connectivity_t}, (Cstring,), name)
end

function p8est_connectivity_refine(conn, num_per_edge::Int32)
    ccall((:p8est_connectivity_refine, libp4est), Ptr{p8est_connectivity_t}, (Ptr{p8est_connectivity_t}, Int32), conn, num_per_edge)
end

function p8est_expand_face_transform(iface::Int32, nface::Int32, ftransform)
    ccall((:p8est_expand_face_transform, libp4est), Void, (Int32, Int32, Ptr{Int32}), iface, nface, ftransform)
end

function p8est_find_face_transform(connectivity, itree::p4est_topidx_t, iface::Int32, ftransform)
    ccall((:p8est_find_face_transform, libp4est), p4est_topidx_t, (Ptr{p8est_connectivity_t}, p4est_topidx_t, Int32, Ptr{Int32}), connectivity, itree, iface, ftransform)
end

function p8est_find_edge_transform(connectivity, itree::p4est_topidx_t, iedge::Int32, ei)
    ccall((:p8est_find_edge_transform, libp4est), Void, (Ptr{p8est_connectivity_t}, p4est_topidx_t, Int32, Ptr{p8est_edge_info_t}), connectivity, itree, iedge, ei)
end

function p8est_find_corner_transform(connectivity, itree::p4est_topidx_t, icorner::Int32, ci)
    ccall((:p8est_find_corner_transform, libp4est), Void, (Ptr{p8est_connectivity_t}, p4est_topidx_t, Int32, Ptr{p8est_corner_info_t}), connectivity, itree, icorner, ci)
end

function p8est_connectivity_complete(conn)
    ccall((:p8est_connectivity_complete, libp4est), Void, (Ptr{p8est_connectivity_t},), conn)
end

function p8est_connectivity_reduce(conn)
    ccall((:p8est_connectivity_reduce, libp4est), Void, (Ptr{p8est_connectivity_t},), conn)
end

function p8est_connectivity_permute(conn, perm, is_current_to_new::Int32)
    ccall((:p8est_connectivity_permute, libp4est), Void, (Ptr{p8est_connectivity_t}, Ptr{sc_array_t}, Int32), conn, perm, is_current_to_new)
end

function p8est_connectivity_join_faces(conn, tree_left::p4est_topidx_t, tree_right::p4est_topidx_t, face_left::Int32, face_right::Int32, orientation::Int32)
    ccall((:p8est_connectivity_join_faces, libp4est), Void, (Ptr{p8est_connectivity_t}, p4est_topidx_t, p4est_topidx_t, Int32, Int32, Int32), conn, tree_left, tree_right, face_left, face_right, orientation)
end

function p8est_connectivity_is_equivalent(conn1, conn2)
    ccall((:p8est_connectivity_is_equivalent, libp4est), Int32, (Ptr{p8est_connectivity_t}, Ptr{p8est_connectivity_t}), conn1, conn2)
end

function p8est_edge_array_index(array, it::UInt)
    ccall((:p8est_edge_array_index, libp4est), Ptr{p8est_edge_transform_t}, (Ptr{sc_array_t}, UInt), array, it)
end

function p8est_corner_array_index(array, it::UInt)
    ccall((:p8est_corner_array_index, libp4est), Ptr{p8est_corner_transform_t}, (Ptr{sc_array_t}, UInt), array, it)
end

function p8est_connectivity_read_inp_stream(stream, num_vertices, num_trees, vertices, tree_to_vertex)
    ccall((:p8est_connectivity_read_inp_stream, libp4est), Int32, (Ptr{FILE}, Ptr{p4est_topidx_t}, Ptr{p4est_topidx_t}, Ptr{Float64}, Ptr{p4est_topidx_t}), stream, num_vertices, num_trees, vertices, tree_to_vertex)
end

function p8est_connectivity_read_inp(filename)
    ccall((:p8est_connectivity_read_inp, libp4est), Ptr{p8est_connectivity_t}, (Cstring,), filename)
end

function p6est_connectivity_new(conn4, top_vertices, height::NTuple{3, Float64})
    ccall((:p6est_connectivity_new, libp4est), Ptr{p6est_connectivity_t}, (Ptr{p4est_connectivity_t}, Ptr{Float64}, NTuple{3, Float64}), conn4, top_vertices, height)
end

function p6est_connectivity_destroy(conn)
    ccall((:p6est_connectivity_destroy, libp4est), Void, (Ptr{p6est_connectivity_t},), conn)
end

function p6est_tree_get_vertices(conn, which_tree::p4est_topidx_t, vertices::NTuple{24, Float64})
    ccall((:p6est_tree_get_vertices, libp4est), Void, (Ptr{p6est_connectivity_t}, p4est_topidx_t, NTuple{24, Float64}), conn, which_tree, vertices)
end

function p6est_qcoord_to_vertex(connectivity, treeid::p4est_topidx_t, x::p4est_qcoord_t, y::p4est_qcoord_t, z::p4est_qcoord_t, vxyz::NTuple{3, Float64})
    ccall((:p6est_qcoord_to_vertex, libp4est), Void, (Ptr{p6est_connectivity_t}, p4est_topidx_t, p4est_qcoord_t, p4est_qcoord_t, p4est_qcoord_t, NTuple{3, Float64}), connectivity, treeid, x, y, z, vxyz)
end

function p6est_new(mpicomm::MPI_Comm, connectivity, data_size::UInt, init_fn::p6est_init_t, user_pointer)
    ccall((:p6est_new, libp4est), Ptr{p6est_t}, (MPI_Comm, Ptr{p6est_connectivity_t}, UInt, p6est_init_t, Ptr{Void}), mpicomm, connectivity, data_size, init_fn, user_pointer)
end

function p6est_new_from_p4est(p4est, top_vertices, height::NTuple{3, Float64}, min_zlevel::Int32, data_size::UInt, init_fn::p6est_init_t, user_pointer)
    ccall((:p6est_new_from_p4est, libp4est), Ptr{p6est_t}, (Ptr{p4est_t}, Ptr{Float64}, NTuple{3, Float64}, Int32, UInt, p6est_init_t, Ptr{Void}), p4est, top_vertices, height, min_zlevel, data_size, init_fn, user_pointer)
end

function p6est_destroy(p6est)
    ccall((:p6est_destroy, libp4est), Void, (Ptr{p6est_t},), p6est)
end

function p6est_copy(input, copy_data::Int32)
    ccall((:p6est_copy, libp4est), Ptr{p6est_t}, (Ptr{p6est_t}, Int32), input, copy_data)
end

function p6est_reset_data(p6est, data_size::UInt, init_fn::p6est_init_t, user_pointer)
    ccall((:p6est_reset_data, libp4est), Void, (Ptr{p6est_t}, UInt, p6est_init_t, Ptr{Void}), p6est, data_size, init_fn, user_pointer)
end

function p6est_refine_columns(p6est, refine_recursive::Int32, refine_fn::p6est_refine_column_t, init_fn::p6est_init_t)
    ccall((:p6est_refine_columns, libp4est), Void, (Ptr{p6est_t}, Int32, p6est_refine_column_t, p6est_init_t), p6est, refine_recursive, refine_fn, init_fn)
end

function p6est_refine_layers(p6est, refine_recursive::Int32, refine_fn::p6est_refine_layer_t, init_fn::p6est_init_t)
    ccall((:p6est_refine_layers, libp4est), Void, (Ptr{p6est_t}, Int32, p6est_refine_layer_t, p6est_init_t), p6est, refine_recursive, refine_fn, init_fn)
end

function p6est_coarsen_columns(p6est, coarsen_recursive::Int32, coarsen_fn::p6est_coarsen_column_t, init_fn::p6est_init_t)
    ccall((:p6est_coarsen_columns, libp4est), Void, (Ptr{p6est_t}, Int32, p6est_coarsen_column_t, p6est_init_t), p6est, coarsen_recursive, coarsen_fn, init_fn)
end

function p6est_coarsen_layers(p6est, coarsen_recursive::Int32, coarsen_fn::p6est_coarsen_layer_t, init_fn::p6est_init_t)
    ccall((:p6est_coarsen_layers, libp4est), Void, (Ptr{p6est_t}, Int32, p6est_coarsen_layer_t, p6est_init_t), p6est, coarsen_recursive, coarsen_fn, init_fn)
end

function p6est_balance(p6est, btype::p8est_connect_type_t, init_fn::p6est_init_t)
    ccall((:p6est_balance, libp4est), Void, (Ptr{p6est_t}, p8est_connect_type_t, p6est_init_t), p6est, btype, init_fn)
end

function p6est_partition(p6est, weight_fn::p6est_weight_t)
    ccall((:p6est_partition, libp4est), p4est_gloidx_t, (Ptr{p6est_t}, p6est_weight_t), p6est, weight_fn)
end

function p6est_partition_correct(p6est, num_layers_in_proc)
    ccall((:p6est_partition_correct, libp4est), Void, (Ptr{p6est_t}, Ptr{p4est_locidx_t}), p6est, num_layers_in_proc)
end

function p6est_partition_to_p4est_partition(p6est, num_layers_in_proc, num_columns_in_proc)
    ccall((:p6est_partition_to_p4est_partition, libp4est), Void, (Ptr{p6est_t}, Ptr{p4est_locidx_t}, Ptr{p4est_locidx_t}), p6est, num_layers_in_proc, num_columns_in_proc)
end

function p4est_partition_to_p6est_partition(p6est, num_columns_in_proc, num_layers_in_proc)
    ccall((:p4est_partition_to_p6est_partition, libp4est), Void, (Ptr{p6est_t}, Ptr{p4est_locidx_t}, Ptr{p4est_locidx_t}), p6est, num_columns_in_proc, num_layers_in_proc)
end

function p6est_partition_for_coarsening(p6est, num_layers_in_proc)
    ccall((:p6est_partition_for_coarsening, libp4est), p4est_gloidx_t, (Ptr{p6est_t}, Ptr{p4est_locidx_t}), p6est, num_layers_in_proc)
end

function p6est_partition_given(p6est, num_layers_in_proc)
    ccall((:p6est_partition_given, libp4est), p4est_gloidx_t, (Ptr{p6est_t}, Ptr{p4est_locidx_t}), p6est, num_layers_in_proc)
end

function p6est_checksum(p6est)
    ccall((:p6est_checksum, libp4est), UInt32, (Ptr{p6est_t},), p6est)
end

function p6est_save(filename, p6est, save_data::Int32)
    ccall((:p6est_save, libp4est), Void, (Cstring, Ptr{p6est_t}, Int32), filename, p6est, save_data)
end

function p6est_load(filename, mpicomm::MPI_Comm, data_size::UInt, load_data::Int32, user_pointer, connectivity)
    ccall((:p6est_load, libp4est), Ptr{p6est_t}, (Cstring, MPI_Comm, UInt, Int32, Ptr{Void}, Ptr{Ptr{p6est_connectivity_t}}), filename, mpicomm, data_size, load_data, user_pointer, connectivity)
end

function p2est_quadrant_array_index(array, it::UInt)
    ccall((:p2est_quadrant_array_index, libp4est), Ptr{p2est_quadrant_t}, (Ptr{sc_array_t}, UInt), array, it)
end

function p2est_quadrant_array_push(array)
    ccall((:p2est_quadrant_array_push, libp4est), Ptr{p2est_quadrant_t}, (Ptr{sc_array_t},), array)
end

function p2est_quadrant_mempool_alloc(mempool)
    ccall((:p2est_quadrant_mempool_alloc, libp4est), Ptr{p2est_quadrant_t}, (Ptr{sc_mempool_t},), mempool)
end

function p2est_quadrant_list_pop(list)
    ccall((:p2est_quadrant_list_pop, libp4est), Ptr{p2est_quadrant_t}, (Ptr{sc_list_t},), list)
end

function p6est_layer_init_data(p6est, which_tree::p4est_topidx_t, column, layer, init_fn::p6est_init_t)
    ccall((:p6est_layer_init_data, libp4est), Void, (Ptr{p6est_t}, p4est_topidx_t, Ptr{p4est_quadrant_t}, Ptr{p2est_quadrant_t}, p6est_init_t), p6est, which_tree, column, layer, init_fn)
end

function p6est_layer_free_data(p6est, layer)
    ccall((:p6est_layer_free_data, libp4est), Void, (Ptr{p6est_t}, Ptr{p2est_quadrant_t}), p6est, layer)
end

function p6est_compress_columns(p6est)
    ccall((:p6est_compress_columns, libp4est), Void, (Ptr{p6est_t},), p6est)
end

function p6est_update_offsets(p6est)
    ccall((:p6est_update_offsets, libp4est), Void, (Ptr{p6est_t},), p6est)
end

function p4est_ghost_is_valid(p4est, ghost)
    ccall((:p4est_ghost_is_valid, libp4est), Int32, (Ptr{p4est_t}, Ptr{p4est_ghost_t}), p4est, ghost)
end

function p4est_ghost_memory_used()
    ccall((:p4est_ghost_memory_used, libp4est), Int32, ())
end

function p4est_quadrant_find_owner(p4est, treeid::p4est_topidx_t, face::Int32, q)
    ccall((:p4est_quadrant_find_owner, libp4est), Int32, (Ptr{p4est_t}, p4est_topidx_t, Int32, Ptr{p4est_quadrant_t}), p4est, treeid, face, q)
end

function p4est_ghost_new(p4est, btype::p4est_connect_type_t)
    ccall((:p4est_ghost_new, libp4est), Ptr{p4est_ghost_t}, (Ptr{p4est_t}, p4est_connect_type_t), p4est, btype)
end

function p4est_ghost_destroy(ghost)
    ccall((:p4est_ghost_destroy, libp4est), Void, (Ptr{p4est_ghost_t},), ghost)
end

function p4est_ghost_bsearch(ghost, which_proc::Int32, which_tree::p4est_topidx_t, q)
    ccall((:p4est_ghost_bsearch, libp4est), Cssize_t, (Ptr{p4est_ghost_t}, Int32, p4est_topidx_t, Ptr{p4est_quadrant_t}), ghost, which_proc, which_tree, q)
end

function p4est_ghost_contains(ghost, which_proc::Int32, which_tree::p4est_topidx_t, q)
    ccall((:p4est_ghost_contains, libp4est), Cssize_t, (Ptr{p4est_ghost_t}, Int32, p4est_topidx_t, Ptr{p4est_quadrant_t}), ghost, which_proc, which_tree, q)
end

function p4est_face_quadrant_exists(p4est, ghost, treeid::p4est_topidx_t, q, face, hang, owner_rank)
    ccall((:p4est_face_quadrant_exists, libp4est), p4est_locidx_t, (Ptr{p4est_t}, Ptr{p4est_ghost_t}, p4est_topidx_t, Ptr{p4est_quadrant_t}, Ptr{Int32}, Ptr{Int32}, Ptr{Int32}), p4est, ghost, treeid, q, face, hang, owner_rank)
end

function p4est_quadrant_exists(p4est, ghost, treeid::p4est_topidx_t, q, exists_arr, rproc_arr, rquad_arr)
    ccall((:p4est_quadrant_exists, libp4est), Int32, (Ptr{p4est_t}, Ptr{p4est_ghost_t}, p4est_topidx_t, Ptr{p4est_quadrant_t}, Ptr{sc_array_t}, Ptr{sc_array_t}, Ptr{sc_array_t}), p4est, ghost, treeid, q, exists_arr, rproc_arr, rquad_arr)
end

function p4est_is_balanced(p4est, btype::p4est_connect_type_t)
    ccall((:p4est_is_balanced, libp4est), Int32, (Ptr{p4est_t}, p4est_connect_type_t), p4est, btype)
end

function p4est_ghost_checksum(p4est, ghost)
    ccall((:p4est_ghost_checksum, libp4est), UInt32, (Ptr{p4est_t}, Ptr{p4est_ghost_t}), p4est, ghost)
end

function p4est_ghost_exchange_data(p4est, ghost, ghost_data)
    ccall((:p4est_ghost_exchange_data, libp4est), Void, (Ptr{p4est_t}, Ptr{p4est_ghost_t}, Ptr{Void}), p4est, ghost, ghost_data)
end

function p4est_ghost_exchange_data_begin(p4est, ghost, ghost_data)
    ccall((:p4est_ghost_exchange_data_begin, libp4est), Ptr{p4est_ghost_exchange_t}, (Ptr{p4est_t}, Ptr{p4est_ghost_t}, Ptr{Void}), p4est, ghost, ghost_data)
end

function p4est_ghost_exchange_data_end(exc)
    ccall((:p4est_ghost_exchange_data_end, libp4est), Void, (Ptr{p4est_ghost_exchange_t},), exc)
end

function p4est_ghost_exchange_custom(p4est, ghost, data_size::UInt, mirror_data, ghost_data)
    ccall((:p4est_ghost_exchange_custom, libp4est), Void, (Ptr{p4est_t}, Ptr{p4est_ghost_t}, UInt, Ptr{Ptr{Void}}, Ptr{Void}), p4est, ghost, data_size, mirror_data, ghost_data)
end

function p4est_ghost_exchange_custom_begin(p4est, ghost, data_size::UInt, mirror_data, ghost_data)
    ccall((:p4est_ghost_exchange_custom_begin, libp4est), Ptr{p4est_ghost_exchange_t}, (Ptr{p4est_t}, Ptr{p4est_ghost_t}, UInt, Ptr{Ptr{Void}}, Ptr{Void}), p4est, ghost, data_size, mirror_data, ghost_data)
end

function p4est_ghost_exchange_custom_end(exc)
    ccall((:p4est_ghost_exchange_custom_end, libp4est), Void, (Ptr{p4est_ghost_exchange_t},), exc)
end

function p4est_ghost_exchange_custom_levels(p4est, ghost, minlevel::Int32, maxlevel::Int32, data_size::UInt, mirror_data, ghost_data)
    ccall((:p4est_ghost_exchange_custom_levels, libp4est), Void, (Ptr{p4est_t}, Ptr{p4est_ghost_t}, Int32, Int32, UInt, Ptr{Ptr{Void}}, Ptr{Void}), p4est, ghost, minlevel, maxlevel, data_size, mirror_data, ghost_data)
end

function p4est_ghost_exchange_custom_levels_begin(p4est, ghost, minlevel::Int32, maxlevel::Int32, data_size::UInt, mirror_data, ghost_data)
    ccall((:p4est_ghost_exchange_custom_levels_begin, libp4est), Ptr{p4est_ghost_exchange_t}, (Ptr{p4est_t}, Ptr{p4est_ghost_t}, Int32, Int32, UInt, Ptr{Ptr{Void}}, Ptr{Void}), p4est, ghost, minlevel, maxlevel, data_size, mirror_data, ghost_data)
end

function p4est_ghost_exchange_custom_levels_end(exc)
    ccall((:p4est_ghost_exchange_custom_levels_end, libp4est), Void, (Ptr{p4est_ghost_exchange_t},), exc)
end

function p4est_ghost_expand(p4est, ghost)
    ccall((:p4est_ghost_expand, libp4est), Void, (Ptr{p4est_t}, Ptr{p4est_ghost_t}), p4est, ghost)
end

function p6est_ghost_memory_used()
    ccall((:p6est_ghost_memory_used, libp4est), Int32, ())
end

function p6est_ghost_new(p4est, btype::p4est_connect_type_t)
    ccall((:p6est_ghost_new, libp4est), Ptr{p6est_ghost_t}, (Ptr{p6est_t}, p4est_connect_type_t), p4est, btype)
end

function p6est_ghost_expand(p6est, ghost)
    ccall((:p6est_ghost_expand, libp4est), Void, (Ptr{p6est_t}, Ptr{p6est_ghost_t}), p6est, ghost)
end

function p6est_ghost_destroy(ghost)
    ccall((:p6est_ghost_destroy, libp4est), Void, (Ptr{p6est_ghost_t},), ghost)
end

function p6est_ghost_bsearch(ghost, which_proc::Int32, which_tree::p4est_topidx_t, column, layer)
    ccall((:p6est_ghost_bsearch, libp4est), Cssize_t, (Ptr{p6est_ghost_t}, Int32, p4est_topidx_t, Ptr{p4est_quadrant_t}, Ptr{p2est_quadrant_t}), ghost, which_proc, which_tree, column, layer)
end

function p6est_ghost_contains(ghost, which_proc::Int32, which_tree::p4est_topidx_t, column, layer)
    ccall((:p6est_ghost_contains, libp4est), Cssize_t, (Ptr{p6est_ghost_t}, Int32, p4est_topidx_t, Ptr{p4est_quadrant_t}, Ptr{p2est_quadrant_t}), ghost, which_proc, which_tree, column, layer)
end

function p6est_layer_exists(p6est, ghost, treeid::p4est_topidx_t, column, layer, exists_arr, rproc_arr, rquad_arr)
    ccall((:p6est_layer_exists, libp4est), Int32, (Ptr{p6est_t}, Ptr{p6est_ghost_t}, p4est_topidx_t, Ptr{p4est_quadrant_t}, Ptr{p2est_quadrant_t}, Ptr{sc_array_t}, Ptr{sc_array_t}, Ptr{sc_array_t}), p6est, ghost, treeid, column, layer, exists_arr, rproc_arr, rquad_arr)
end

function p6est_is_balanced(p6est, btype::p8est_connect_type_t)
    ccall((:p6est_is_balanced, libp4est), Int32, (Ptr{p6est_t}, p8est_connect_type_t), p6est, btype)
end

function p6est_ghost_checksum(p6est, ghost)
    ccall((:p6est_ghost_checksum, libp4est), UInt32, (Ptr{p6est_t}, Ptr{p6est_ghost_t}), p6est, ghost)
end

function p4est_lnodes_decode(face_code::p4est_lnodes_code_t, hanging_face::NTuple{4, Int32})
    ccall((:p4est_lnodes_decode, libp4est), Int32, (p4est_lnodes_code_t, NTuple{4, Int32}), face_code, hanging_face)
end

function p4est_lnodes_new(p4est, ghost_layer, degree::Int32)
    ccall((:p4est_lnodes_new, libp4est), Ptr{p4est_lnodes_t}, (Ptr{p4est_t}, Ptr{p4est_ghost_t}, Int32), p4est, ghost_layer, degree)
end

function p4est_lnodes_destroy(lnodes)
    ccall((:p4est_lnodes_destroy, libp4est), Void, (Ptr{p4est_lnodes_t},), lnodes)
end

function p4est_ghost_support_lnodes(p4est, lnodes, ghost)
    ccall((:p4est_ghost_support_lnodes, libp4est), Void, (Ptr{p4est_t}, Ptr{p4est_lnodes_t}, Ptr{p4est_ghost_t}), p4est, lnodes, ghost)
end

function p4est_ghost_expand_by_lnodes(p4est, lnodes, ghost)
    ccall((:p4est_ghost_expand_by_lnodes, libp4est), Void, (Ptr{p4est_t}, Ptr{p4est_lnodes_t}, Ptr{p4est_ghost_t}), p4est, lnodes, ghost)
end

function p4est_partition_lnodes(p4est, ghost, degree::Int32, partition_for_coarsening::Int32)
    ccall((:p4est_partition_lnodes, libp4est), Void, (Ptr{p4est_t}, Ptr{p4est_ghost_t}, Int32, Int32), p4est, ghost, degree, partition_for_coarsening)
end

function p4est_partition_lnodes_detailed(p4est, ghost, nodes_per_volume::Int32, nodes_per_face::Int32, nodes_per_corner::Int32, partition_for_coarsening::Int32)
    ccall((:p4est_partition_lnodes_detailed, libp4est), Void, (Ptr{p4est_t}, Ptr{p4est_ghost_t}, Int32, Int32, Int32, Int32), p4est, ghost, nodes_per_volume, nodes_per_face, nodes_per_corner, partition_for_coarsening)
end

function p4est_lnodes_share_owned_begin(node_data, lnodes)
    ccall((:p4est_lnodes_share_owned_begin, libp4est), Ptr{p4est_lnodes_buffer_t}, (Ptr{sc_array_t}, Ptr{p4est_lnodes_t}), node_data, lnodes)
end

function p4est_lnodes_share_owned_end(buffer)
    ccall((:p4est_lnodes_share_owned_end, libp4est), Void, (Ptr{p4est_lnodes_buffer_t},), buffer)
end

function p4est_lnodes_share_owned(node_data, lnodes)
    ccall((:p4est_lnodes_share_owned, libp4est), Void, (Ptr{sc_array_t}, Ptr{p4est_lnodes_t}), node_data, lnodes)
end

function p4est_lnodes_share_all_begin(node_data, lnodes)
    ccall((:p4est_lnodes_share_all_begin, libp4est), Ptr{p4est_lnodes_buffer_t}, (Ptr{sc_array_t}, Ptr{p4est_lnodes_t}), node_data, lnodes)
end

function p4est_lnodes_share_all_end(buffer)
    ccall((:p4est_lnodes_share_all_end, libp4est), Void, (Ptr{p4est_lnodes_buffer_t},), buffer)
end

function p4est_lnodes_share_all(node_data, lnodes)
    ccall((:p4est_lnodes_share_all, libp4est), Ptr{p4est_lnodes_buffer_t}, (Ptr{sc_array_t}, Ptr{p4est_lnodes_t}), node_data, lnodes)
end

function p4est_lnodes_buffer_destroy(buffer)
    ccall((:p4est_lnodes_buffer_destroy, libp4est), Void, (Ptr{p4est_lnodes_buffer_t},), buffer)
end

function p4est_lnodes_rank_array_index_int(array, it::Int32)
    ccall((:p4est_lnodes_rank_array_index_int, libp4est), Ptr{p4est_lnodes_rank_t}, (Ptr{sc_array_t}, Int32), array, it)
end

function p4est_lnodes_rank_array_index(array, it::UInt)
    ccall((:p4est_lnodes_rank_array_index, libp4est), Ptr{p4est_lnodes_rank_t}, (Ptr{sc_array_t}, UInt), array, it)
end

function p4est_lnodes_global_index(lnodes, lidx::p4est_locidx_t)
    ccall((:p4est_lnodes_global_index, libp4est), p4est_gloidx_t, (Ptr{p4est_lnodes_t}, p4est_locidx_t), lnodes, lidx)
end

function p8est_memory_used()
    ccall((:p8est_memory_used, libp4est), Int32, ())
end

function p8est_revision(p8est)
    ccall((:p8est_revision, libp4est), Int64, (Ptr{p8est_t},), p8est)
end

function p8est_qcoord_to_vertex(connectivity, treeid::p4est_topidx_t, x::p4est_qcoord_t, y::p4est_qcoord_t, z::p4est_qcoord_t, vxyz::NTuple{3, Float64})
    ccall((:p8est_qcoord_to_vertex, libp4est), Void, (Ptr{p8est_connectivity_t}, p4est_topidx_t, p4est_qcoord_t, p4est_qcoord_t, p4est_qcoord_t, NTuple{3, Float64}), connectivity, treeid, x, y, z, vxyz)
end

function p8est_new(mpicomm::MPI_Comm, connectivity, data_size::UInt, init_fn::p8est_init_t, user_pointer)
    ccall((:p8est_new, libp4est), Ptr{p8est_t}, (MPI_Comm, Ptr{p8est_connectivity_t}, UInt, p8est_init_t, Ptr{Void}), mpicomm, connectivity, data_size, init_fn, user_pointer)
end

function p8est_destroy(p8est)
    ccall((:p8est_destroy, libp4est), Void, (Ptr{p8est_t},), p8est)
end

function p8est_copy(input, copy_data::Int32)
    ccall((:p8est_copy, libp4est), Ptr{p8est_t}, (Ptr{p8est_t}, Int32), input, copy_data)
end

function p8est_reset_data(p8est, data_size::UInt, init_fn::p8est_init_t, user_pointer)
    ccall((:p8est_reset_data, libp4est), Void, (Ptr{p8est_t}, UInt, p8est_init_t, Ptr{Void}), p8est, data_size, init_fn, user_pointer)
end

function p8est_refine(p8est, refine_recursive::Int32, refine_fn::p8est_refine_t, init_fn::p8est_init_t)
    ccall((:p8est_refine, libp4est), Void, (Ptr{p8est_t}, Int32, p8est_refine_t, p8est_init_t), p8est, refine_recursive, refine_fn, init_fn)
end

function p8est_coarsen(p8est, coarsen_recursive::Int32, coarsen_fn::p8est_coarsen_t, init_fn::p8est_init_t)
    ccall((:p8est_coarsen, libp4est), Void, (Ptr{p8est_t}, Int32, p8est_coarsen_t, p8est_init_t), p8est, coarsen_recursive, coarsen_fn, init_fn)
end

function p8est_balance(p8est, btype::p8est_connect_type_t, init_fn::p8est_init_t)
    ccall((:p8est_balance, libp4est), Void, (Ptr{p8est_t}, p8est_connect_type_t, p8est_init_t), p8est, btype, init_fn)
end

function p8est_partition(p8est, allow_for_coarsening::Int32, weight_fn::p8est_weight_t)
    ccall((:p8est_partition, libp4est), Void, (Ptr{p8est_t}, Int32, p8est_weight_t), p8est, allow_for_coarsening, weight_fn)
end

function p8est_checksum(p8est)
    ccall((:p8est_checksum, libp4est), UInt32, (Ptr{p8est_t},), p8est)
end

function p8est_save(filename, p8est, save_data::Int32)
    ccall((:p8est_save, libp4est), Void, (Cstring, Ptr{p8est_t}, Int32), filename, p8est, save_data)
end

function p8est_load(filename, mpicomm::MPI_Comm, data_size::UInt, load_data::Int32, user_pointer, connectivity)
    ccall((:p8est_load, libp4est), Ptr{p8est_t}, (Cstring, MPI_Comm, UInt, Int32, Ptr{Void}, Ptr{Ptr{p8est_connectivity_t}}), filename, mpicomm, data_size, load_data, user_pointer, connectivity)
end

function p8est_tree_array_index(array, it::p4est_topidx_t)
    ccall((:p8est_tree_array_index, libp4est), Ptr{p8est_tree_t}, (Ptr{sc_array_t}, p4est_topidx_t), array, it)
end

function p8est_quadrant_array_index(array, it::UInt)
    ccall((:p8est_quadrant_array_index, libp4est), Ptr{p8est_quadrant_t}, (Ptr{sc_array_t}, UInt), array, it)
end

function p8est_quadrant_array_push(array)
    ccall((:p8est_quadrant_array_push, libp4est), Ptr{p8est_quadrant_t}, (Ptr{sc_array_t},), array)
end

function p8est_quadrant_mempool_alloc(mempool)
    ccall((:p8est_quadrant_mempool_alloc, libp4est), Ptr{p8est_quadrant_t}, (Ptr{sc_mempool_t},), mempool)
end

function p8est_quadrant_list_pop(list)
    ccall((:p8est_quadrant_list_pop, libp4est), Ptr{p8est_quadrant_t}, (Ptr{sc_list_t},), list)
end

function p8est_ghost_is_valid(p8est, ghost)
    ccall((:p8est_ghost_is_valid, libp4est), Int32, (Ptr{p8est_t}, Ptr{p8est_ghost_t}), p8est, ghost)
end

function p8est_ghost_memory_used()
    ccall((:p8est_ghost_memory_used, libp4est), Int32, ())
end

function p8est_quadrant_find_owner(p8est, treeid::p4est_topidx_t, face::Int32, q)
    ccall((:p8est_quadrant_find_owner, libp4est), Int32, (Ptr{p8est_t}, p4est_topidx_t, Int32, Ptr{p8est_quadrant_t}), p8est, treeid, face, q)
end

function p8est_ghost_new(p8est, btype::p8est_connect_type_t)
    ccall((:p8est_ghost_new, libp4est), Ptr{p8est_ghost_t}, (Ptr{p8est_t}, p8est_connect_type_t), p8est, btype)
end

function p8est_ghost_destroy(ghost)
    ccall((:p8est_ghost_destroy, libp4est), Void, (Ptr{p8est_ghost_t},), ghost)
end

function p8est_ghost_bsearch(ghost, which_proc::Int32, which_tree::p4est_topidx_t, q)
    ccall((:p8est_ghost_bsearch, libp4est), Cssize_t, (Ptr{p8est_ghost_t}, Int32, p4est_topidx_t, Ptr{p8est_quadrant_t}), ghost, which_proc, which_tree, q)
end

function p8est_ghost_tree_contains(ghost, which_proc::Int32, which_tree::p4est_topidx_t, q)
    ccall((:p8est_ghost_tree_contains, libp4est), Cssize_t, (Ptr{p8est_ghost_t}, Int32, p4est_topidx_t, Ptr{p8est_quadrant_t}), ghost, which_proc, which_tree, q)
end

function p8est_face_quadrant_exists(p8est, ghost, treeid::p4est_topidx_t, q, face, hang, owner_rank)
    ccall((:p8est_face_quadrant_exists, libp4est), p4est_locidx_t, (Ptr{p8est_t}, Ptr{p8est_ghost_t}, p4est_topidx_t, Ptr{p8est_quadrant_t}, Ptr{Int32}, Ptr{Int32}, Ptr{Int32}), p8est, ghost, treeid, q, face, hang, owner_rank)
end

function p8est_quadrant_exists(p8est, ghost, treeid::p4est_topidx_t, q, exists_arr, rproc_arr, rquad_arr)
    ccall((:p8est_quadrant_exists, libp4est), Int32, (Ptr{p8est_t}, Ptr{p8est_ghost_t}, p4est_topidx_t, Ptr{p8est_quadrant_t}, Ptr{sc_array_t}, Ptr{sc_array_t}, Ptr{sc_array_t}), p8est, ghost, treeid, q, exists_arr, rproc_arr, rquad_arr)
end

function p8est_is_balanced(p8est, btype::p8est_connect_type_t)
    ccall((:p8est_is_balanced, libp4est), Int32, (Ptr{p8est_t}, p8est_connect_type_t), p8est, btype)
end

function p8est_ghost_checksum(p8est, ghost)
    ccall((:p8est_ghost_checksum, libp4est), UInt32, (Ptr{p8est_t}, Ptr{p8est_ghost_t}), p8est, ghost)
end

function p8est_ghost_exchange_data(p4est, ghost, ghost_data)
    ccall((:p8est_ghost_exchange_data, libp4est), Void, (Ptr{p8est_t}, Ptr{p8est_ghost_t}, Ptr{Void}), p4est, ghost, ghost_data)
end

function p8est_ghost_exchange_data_begin(p4est, ghost, ghost_data)
    ccall((:p8est_ghost_exchange_data_begin, libp4est), Ptr{p8est_ghost_exchange_t}, (Ptr{p8est_t}, Ptr{p8est_ghost_t}, Ptr{Void}), p4est, ghost, ghost_data)
end

function p8est_ghost_exchange_data_end(exc)
    ccall((:p8est_ghost_exchange_data_end, libp4est), Void, (Ptr{p8est_ghost_exchange_t},), exc)
end

function p8est_ghost_exchange_custom(p4est, ghost, data_size::UInt, mirror_data, ghost_data)
    ccall((:p8est_ghost_exchange_custom, libp4est), Void, (Ptr{p8est_t}, Ptr{p8est_ghost_t}, UInt, Ptr{Ptr{Void}}, Ptr{Void}), p4est, ghost, data_size, mirror_data, ghost_data)
end

function p8est_ghost_exchange_custom_begin(p4est, ghost, data_size::UInt, mirror_data, ghost_data)
    ccall((:p8est_ghost_exchange_custom_begin, libp4est), Ptr{p8est_ghost_exchange_t}, (Ptr{p8est_t}, Ptr{p8est_ghost_t}, UInt, Ptr{Ptr{Void}}, Ptr{Void}), p4est, ghost, data_size, mirror_data, ghost_data)
end

function p8est_ghost_exchange_custom_end(exc)
    ccall((:p8est_ghost_exchange_custom_end, libp4est), Void, (Ptr{p8est_ghost_exchange_t},), exc)
end

function p8est_ghost_exchange_custom_levels(p8est, ghost, minlevel::Int32, maxlevel::Int32, data_size::UInt, mirror_data, ghost_data)
    ccall((:p8est_ghost_exchange_custom_levels, libp4est), Void, (Ptr{p8est_t}, Ptr{p8est_ghost_t}, Int32, Int32, UInt, Ptr{Ptr{Void}}, Ptr{Void}), p8est, ghost, minlevel, maxlevel, data_size, mirror_data, ghost_data)
end

function p8est_ghost_exchange_custom_levels_begin(p4est, ghost, minlevel::Int32, maxlevel::Int32, data_size::UInt, mirror_data, ghost_data)
    ccall((:p8est_ghost_exchange_custom_levels_begin, libp4est), Ptr{p8est_ghost_exchange_t}, (Ptr{p8est_t}, Ptr{p8est_ghost_t}, Int32, Int32, UInt, Ptr{Ptr{Void}}, Ptr{Void}), p4est, ghost, minlevel, maxlevel, data_size, mirror_data, ghost_data)
end

function p8est_ghost_exchange_custom_levels_end(exc)
    ccall((:p8est_ghost_exchange_custom_levels_end, libp4est), Void, (Ptr{p8est_ghost_exchange_t},), exc)
end

function p8est_ghost_expand(p8est, ghost)
    ccall((:p8est_ghost_expand, libp4est), Void, (Ptr{p8est_t}, Ptr{p8est_ghost_t}), p8est, ghost)
end

function p8est_lnodes_decode(face_code::p8est_lnodes_code_t, hanging_face::NTuple{6, Int32}, hanging_edge::NTuple{12, Int32})
    ccall((:p8est_lnodes_decode, libp4est), Int32, (p8est_lnodes_code_t, NTuple{6, Int32}, NTuple{12, Int32}), face_code, hanging_face, hanging_edge)
end

function p8est_lnodes_new(p8est, ghost_layer, degree::Int32)
    ccall((:p8est_lnodes_new, libp4est), Ptr{p8est_lnodes_t}, (Ptr{p8est_t}, Ptr{p8est_ghost_t}, Int32), p8est, ghost_layer, degree)
end

function p8est_lnodes_destroy(lnodes)
    ccall((:p8est_lnodes_destroy, libp4est), Void, (Ptr{p8est_lnodes_t},), lnodes)
end

function p8est_partition_lnodes(p8est, ghost, degree::Int32, partition_for_coarsening::Int32)
    ccall((:p8est_partition_lnodes, libp4est), Void, (Ptr{p8est_t}, Ptr{p8est_ghost_t}, Int32, Int32), p8est, ghost, degree, partition_for_coarsening)
end

function p8est_partition_lnodes_detailed(p4est, ghost, nodes_per_volume::Int32, nodes_per_face::Int32, nodes_per_edge::Int32, nodes_per_corner::Int32, partition_for_coarsening::Int32)
    ccall((:p8est_partition_lnodes_detailed, libp4est), Void, (Ptr{p8est_t}, Ptr{p8est_ghost_t}, Int32, Int32, Int32, Int32, Int32), p4est, ghost, nodes_per_volume, nodes_per_face, nodes_per_edge, nodes_per_corner, partition_for_coarsening)
end

function p8est_ghost_support_lnodes(p8est, lnodes, ghost)
    ccall((:p8est_ghost_support_lnodes, libp4est), Void, (Ptr{p8est_t}, Ptr{p8est_lnodes_t}, Ptr{p8est_ghost_t}), p8est, lnodes, ghost)
end

function p8est_ghost_expand_by_lnodes(p4est, lnodes, ghost)
    ccall((:p8est_ghost_expand_by_lnodes, libp4est), Void, (Ptr{p8est_t}, Ptr{p8est_lnodes_t}, Ptr{p8est_ghost_t}), p4est, lnodes, ghost)
end

function p8est_lnodes_share_owned_begin(node_data, lnodes)
    ccall((:p8est_lnodes_share_owned_begin, libp4est), Ptr{p8est_lnodes_buffer_t}, (Ptr{sc_array_t}, Ptr{p8est_lnodes_t}), node_data, lnodes)
end

function p8est_lnodes_share_owned_end(buffer)
    ccall((:p8est_lnodes_share_owned_end, libp4est), Void, (Ptr{p8est_lnodes_buffer_t},), buffer)
end

function p8est_lnodes_share_owned(node_data, lnodes)
    ccall((:p8est_lnodes_share_owned, libp4est), Void, (Ptr{sc_array_t}, Ptr{p8est_lnodes_t}), node_data, lnodes)
end

function p8est_lnodes_share_all_begin(node_data, lnodes)
    ccall((:p8est_lnodes_share_all_begin, libp4est), Ptr{p8est_lnodes_buffer_t}, (Ptr{sc_array_t}, Ptr{p8est_lnodes_t}), node_data, lnodes)
end

function p8est_lnodes_share_all_end(buffer)
    ccall((:p8est_lnodes_share_all_end, libp4est), Void, (Ptr{p8est_lnodes_buffer_t},), buffer)
end

function p8est_lnodes_share_all(node_data, lnodes)
    ccall((:p8est_lnodes_share_all, libp4est), Ptr{p8est_lnodes_buffer_t}, (Ptr{sc_array_t}, Ptr{p8est_lnodes_t}), node_data, lnodes)
end

function p8est_lnodes_buffer_destroy(buffer)
    ccall((:p8est_lnodes_buffer_destroy, libp4est), Void, (Ptr{p8est_lnodes_buffer_t},), buffer)
end

function p8est_lnodes_rank_array_index_int(array, it::Int32)
    ccall((:p8est_lnodes_rank_array_index_int, libp4est), Ptr{p8est_lnodes_rank_t}, (Ptr{sc_array_t}, Int32), array, it)
end

function p8est_lnodes_rank_array_index(array, it::UInt)
    ccall((:p8est_lnodes_rank_array_index, libp4est), Ptr{p8est_lnodes_rank_t}, (Ptr{sc_array_t}, UInt), array, it)
end

function p8est_lnodes_global_index(lnodes, lidx::p4est_locidx_t)
    ccall((:p8est_lnodes_global_index, libp4est), p4est_gloidx_t, (Ptr{p8est_lnodes_t}, p4est_locidx_t), lnodes, lidx)
end

function p6est_lnodes_decode(face_code::p6est_lnodes_code_t, hanging_face::NTuple{6, Int32}, hanging_edge::NTuple{12, Int32})
    ccall((:p6est_lnodes_decode, libp4est), Int32, (p6est_lnodes_code_t, NTuple{6, Int32}, NTuple{12, Int32}), face_code, hanging_face, hanging_edge)
end

function p6est_lnodes_new(p6est, ghost_layer, degree::Int32)
    ccall((:p6est_lnodes_new, libp4est), Ptr{p6est_lnodes_t}, (Ptr{p6est_t}, Ptr{p6est_ghost_t}, Int32), p6est, ghost_layer, degree)
end

function p6est_lnodes_destroy(lnodes)
    ccall((:p6est_lnodes_destroy, libp4est), Void, (Ptr{p6est_lnodes_t},), lnodes)
end

function p6est_lnodes_share_owned_begin(node_data, lnodes)
    ccall((:p6est_lnodes_share_owned_begin, libp4est), Ptr{p6est_lnodes_buffer_t}, (Ptr{sc_array_t}, Ptr{p6est_lnodes_t}), node_data, lnodes)
end

function p6est_lnodes_share_owned_end(buffer)
    ccall((:p6est_lnodes_share_owned_end, libp4est), Void, (Ptr{p6est_lnodes_buffer_t},), buffer)
end

function p6est_lnodes_share_owned(node_data, lnodes)
    ccall((:p6est_lnodes_share_owned, libp4est), Void, (Ptr{sc_array_t}, Ptr{p6est_lnodes_t}), node_data, lnodes)
end

function p6est_lnodes_share_all_begin(node_data, lnodes)
    ccall((:p6est_lnodes_share_all_begin, libp4est), Ptr{p6est_lnodes_buffer_t}, (Ptr{sc_array_t}, Ptr{p6est_lnodes_t}), node_data, lnodes)
end

function p6est_lnodes_share_all_end(buffer)
    ccall((:p6est_lnodes_share_all_end, libp4est), Void, (Ptr{p6est_lnodes_buffer_t},), buffer)
end

function p6est_lnodes_share_all(node_data, lnodes)
    ccall((:p6est_lnodes_share_all, libp4est), Ptr{p6est_lnodes_buffer_t}, (Ptr{sc_array_t}, Ptr{p6est_lnodes_t}), node_data, lnodes)
end

function p6est_lnodes_buffer_destroy(buffer)
    ccall((:p6est_lnodes_buffer_destroy, libp4est), Void, (Ptr{p6est_lnodes_buffer_t},), buffer)
end

function p6est_lnodes_rank_array_index_int(array, it::Int32)
    ccall((:p6est_lnodes_rank_array_index_int, libp4est), Ptr{p6est_lnodes_rank_t}, (Ptr{sc_array_t}, Int32), array, it)
end

function p6est_lnodes_rank_array_index(array, it::UInt)
    ccall((:p6est_lnodes_rank_array_index, libp4est), Ptr{p6est_lnodes_rank_t}, (Ptr{sc_array_t}, UInt), array, it)
end

function p6est_lnodes_global_index(lnodes, lidx::p4est_locidx_t)
    ccall((:p6est_lnodes_global_index, libp4est), p4est_gloidx_t, (Ptr{p6est_lnodes_t}, p4est_locidx_t), lnodes, lidx)
end

function p6est_lnodes_get_column_labels(p6est, lnodes)
    ccall((:p6est_lnodes_get_column_labels, libp4est), Ptr{p4est_gloidx_t}, (Ptr{p6est_t}, Ptr{p8est_lnodes_t}), p6est, lnodes)
end
# Julia wrapper for header: /home/iagbole/Codeentwicklung/p4est/local/include/p8est.h
# Automatically generated using Clang.jl wrap_c, version 0.0.0

# Julia wrapper for header: /home/iagbole/Codeentwicklung/p4est/local/include/p4est_connectivity.h
# Automatically generated using Clang.jl wrap_c, version 0.0.0

# Julia wrapper for header: /home/iagbole/Codeentwicklung/p4est/local/include/p4est_ghost.h
# Automatically generated using Clang.jl wrap_c, version 0.0.0

# Julia wrapper for header: /home/iagbole/Codeentwicklung/p4est/local/include/p8est_connectivity.h
# Automatically generated using Clang.jl wrap_c, version 0.0.0

# Julia wrapper for header: /home/iagbole/Codeentwicklung/p4est/local/include/p8est_geometry.h
# Automatically generated using Clang.jl wrap_c, version 0.0.0


function p8est_geometry_destroy(geom)
    ccall((:p8est_geometry_destroy, libp4est), Void, (Ptr{p8est_geometry_t},), geom)
end

function p8est_geometry_new_connectivity(conn)
    ccall((:p8est_geometry_new_connectivity, libp4est), Ptr{p8est_geometry_t}, (Ptr{p8est_connectivity_t},), conn)
end

function p8est_geometry_new_shell(conn, R2::Float64, R1::Float64)
    ccall((:p8est_geometry_new_shell, libp4est), Ptr{p8est_geometry_t}, (Ptr{p8est_connectivity_t}, Float64, Float64), conn, R2, R1)
end

function p8est_geometry_new_sphere(conn, R2::Float64, R1::Float64, R0::Float64)
    ccall((:p8est_geometry_new_sphere, libp4est), Ptr{p8est_geometry_t}, (Ptr{p8est_connectivity_t}, Float64, Float64, Float64), conn, R2, R1, R0)
end
# Julia wrapper for header: /home/iagbole/Codeentwicklung/p4est/local/include/p4est.h
# Automatically generated using Clang.jl wrap_c, version 0.0.0

# Julia wrapper for header: /home/iagbole/Codeentwicklung/p4est/local/include/p4est_mesh.h
# Automatically generated using Clang.jl wrap_c, version 0.0.0


function p4est_mesh_memory_used()
    ccall((:p4est_mesh_memory_used, libp4est), Int32, ())
end

function p4est_mesh_new(p4est, ghost, btype::p4est_connect_type_t)
    ccall((:p4est_mesh_new, libp4est), Ptr{p4est_mesh_t}, (Ptr{p4est_t}, Ptr{p4est_ghost_t}, p4est_connect_type_t), p4est, ghost, btype)
end

function p4est_mesh_destroy(mesh)
    ccall((:p4est_mesh_destroy, libp4est), Void, (Ptr{p4est_mesh_t},), mesh)
end

function p4est_mesh_quadrant_cumulative(p4est, cumulative_id::p4est_locidx_t, which_tree, quadrant_id)
    ccall((:p4est_mesh_quadrant_cumulative, libp4est), Ptr{p4est_quadrant_t}, (Ptr{p4est_t}, p4est_locidx_t, Ptr{p4est_topidx_t}, Ptr{p4est_locidx_t}), p4est, cumulative_id, which_tree, quadrant_id)
end

function p4est_mesh_face_neighbor_init2(mfn, p4est, ghost, mesh, which_tree::p4est_topidx_t, quadrant_id::p4est_locidx_t)
    ccall((:p4est_mesh_face_neighbor_init2, libp4est), Void, (Ptr{p4est_mesh_face_neighbor_t}, Ptr{p4est_t}, Ptr{p4est_ghost_t}, Ptr{p4est_mesh_t}, p4est_topidx_t, p4est_locidx_t), mfn, p4est, ghost, mesh, which_tree, quadrant_id)
end

function p4est_mesh_face_neighbor_init(mfn, p4est, ghost, mesh, which_tree::p4est_topidx_t, quadrant)
    ccall((:p4est_mesh_face_neighbor_init, libp4est), Void, (Ptr{p4est_mesh_face_neighbor_t}, Ptr{p4est_t}, Ptr{p4est_ghost_t}, Ptr{p4est_mesh_t}, p4est_topidx_t, Ptr{p4est_quadrant_t}), mfn, p4est, ghost, mesh, which_tree, quadrant)
end

function p4est_mesh_face_neighbor_next(mfn, ntree, nquad, nface, nrank)
    ccall((:p4est_mesh_face_neighbor_next, libp4est), Ptr{p4est_quadrant_t}, (Ptr{p4est_mesh_face_neighbor_t}, Ptr{p4est_topidx_t}, Ptr{p4est_locidx_t}, Ptr{Int32}, Ptr{Int32}), mfn, ntree, nquad, nface, nrank)
end

function p4est_mesh_face_neighbor_data(mfn, ghost_data)
    ccall((:p4est_mesh_face_neighbor_data, libp4est), Ptr{Void}, (Ptr{p4est_mesh_face_neighbor_t}, Ptr{Void}), mfn, ghost_data)
end
# Julia wrapper for header: /home/iagbole/Codeentwicklung/p4est/local/include/p4est_geometry.h
# Automatically generated using Clang.jl wrap_c, version 0.0.0


function p4est_geometry_destroy(geom)
    ccall((:p4est_geometry_destroy, libp4est), Void, (Ptr{p4est_geometry_t},), geom)
end

function p4est_geometry_new_connectivity(conn)
    ccall((:p4est_geometry_new_connectivity, libp4est), Ptr{p4est_geometry_t}, (Ptr{p4est_connectivity_t},), conn)
end
# Julia wrapper for header: /home/iagbole/Codeentwicklung/p4est/local/include/p8est_ghost.h
# Automatically generated using Clang.jl wrap_c, version 0.0.0

# Julia wrapper for header: /home/iagbole/Codeentwicklung/p4est/local/include/p4est_iterate.h
# Automatically generated using Clang.jl wrap_c, version 0.0.0


function p4est_iterate(p4est, ghost_layer, user_data, iter_volume::p4est_iter_volume_t, iter_face::p4est_iter_face_t, iter_corner::p4est_iter_corner_t)
    ccall((:p4est_iterate, libp4est), Void, (Ptr{p4est_t}, Ptr{p4est_ghost_t}, Ptr{Void}, p4est_iter_volume_t, p4est_iter_face_t, p4est_iter_corner_t), p4est, ghost_layer, user_data, iter_volume, iter_face, iter_corner)
end

function p4est_iter_cside_array_index_int(array, it::Int32)
    ccall((:p4est_iter_cside_array_index_int, libp4est), Ptr{p4est_iter_corner_side_t}, (Ptr{sc_array_t}, Int32), array, it)
end

function p4est_iter_cside_array_index(array, it::UInt)
    ccall((:p4est_iter_cside_array_index, libp4est), Ptr{p4est_iter_corner_side_t}, (Ptr{sc_array_t}, UInt), array, it)
end

function p4est_iter_fside_array_index_int(array, it::Int32)
    ccall((:p4est_iter_fside_array_index_int, libp4est), Ptr{p4est_iter_face_side_t}, (Ptr{sc_array_t}, Int32), array, it)
end

function p4est_iter_fside_array_index(array, it::UInt)
    ccall((:p4est_iter_fside_array_index, libp4est), Ptr{p4est_iter_face_side_t}, (Ptr{sc_array_t}, UInt), array, it)
end
# Julia wrapper for header: /home/iagbole/Codeentwicklung/p4est/local/include/p8est_extended.h
# Automatically generated using Clang.jl wrap_c, version 0.0.0


function p8est_mesh_memory_used()
    ccall((:p8est_mesh_memory_used, libp4est), Int32, ())
end

function p8est_mesh_new(p8est, ghost, btype::p8est_connect_type_t)
    ccall((:p8est_mesh_new, libp4est), Ptr{p8est_mesh_t}, (Ptr{p8est_t}, Ptr{p8est_ghost_t}, p8est_connect_type_t), p8est, ghost, btype)
end

function p8est_mesh_destroy(mesh)
    ccall((:p8est_mesh_destroy, libp4est), Void, (Ptr{p8est_mesh_t},), mesh)
end

function p8est_mesh_quadrant_cumulative(p8est, cumulative_id::p4est_locidx_t, which_tree, quadrant_id)
    ccall((:p8est_mesh_quadrant_cumulative, libp4est), Ptr{p8est_quadrant_t}, (Ptr{p8est_t}, p4est_locidx_t, Ptr{p4est_topidx_t}, Ptr{p4est_locidx_t}), p8est, cumulative_id, which_tree, quadrant_id)
end

function p8est_mesh_face_neighbor_init2(mfn, p8est, ghost, mesh, which_tree::p4est_topidx_t, quadrant_id::p4est_locidx_t)
    ccall((:p8est_mesh_face_neighbor_init2, libp4est), Void, (Ptr{p8est_mesh_face_neighbor_t}, Ptr{p8est_t}, Ptr{p8est_ghost_t}, Ptr{p8est_mesh_t}, p4est_topidx_t, p4est_locidx_t), mfn, p8est, ghost, mesh, which_tree, quadrant_id)
end

function p8est_mesh_face_neighbor_init(mfn, p8est, ghost, mesh, which_tree::p4est_topidx_t, quadrant)
    ccall((:p8est_mesh_face_neighbor_init, libp4est), Void, (Ptr{p8est_mesh_face_neighbor_t}, Ptr{p8est_t}, Ptr{p8est_ghost_t}, Ptr{p8est_mesh_t}, p4est_topidx_t, Ptr{p8est_quadrant_t}), mfn, p8est, ghost, mesh, which_tree, quadrant)
end

function p8est_mesh_face_neighbor_next(mfn, ntree, nquad, nface, nrank)
    ccall((:p8est_mesh_face_neighbor_next, libp4est), Ptr{p8est_quadrant_t}, (Ptr{p8est_mesh_face_neighbor_t}, Ptr{p4est_topidx_t}, Ptr{p4est_locidx_t}, Ptr{Int32}, Ptr{Int32}), mfn, ntree, nquad, nface, nrank)
end

function p8est_mesh_face_neighbor_data(mfn, ghost_data)
    ccall((:p8est_mesh_face_neighbor_data, libp4est), Ptr{Void}, (Ptr{p8est_mesh_face_neighbor_t}, Ptr{Void}), mfn, ghost_data)
end

function p8est_iterate(p4est, ghost_layer, user_data, iter_volume::p8est_iter_volume_t, iter_face::p8est_iter_face_t, iter_edge::p8est_iter_edge_t, iter_corner::p8est_iter_corner_t)
    ccall((:p8est_iterate, libp4est), Void, (Ptr{p8est_t}, Ptr{p8est_ghost_t}, Ptr{Void}, p8est_iter_volume_t, p8est_iter_face_t, p8est_iter_edge_t, p8est_iter_corner_t), p4est, ghost_layer, user_data, iter_volume, iter_face, iter_edge, iter_corner)
end

function p8est_iter_cside_array_index_int(array, it::Int32)
    ccall((:p8est_iter_cside_array_index_int, libp4est), Ptr{p8est_iter_corner_side_t}, (Ptr{sc_array_t}, Int32), array, it)
end

function p8est_iter_cside_array_index(array, it::UInt)
    ccall((:p8est_iter_cside_array_index, libp4est), Ptr{p8est_iter_corner_side_t}, (Ptr{sc_array_t}, UInt), array, it)
end

function p8est_iter_eside_array_index_int(array, it::Int32)
    ccall((:p8est_iter_eside_array_index_int, libp4est), Ptr{p8est_iter_edge_side_t}, (Ptr{sc_array_t}, Int32), array, it)
end

function p8est_iter_eside_array_index(array, it::UInt)
    ccall((:p8est_iter_eside_array_index, libp4est), Ptr{p8est_iter_edge_side_t}, (Ptr{sc_array_t}, UInt), array, it)
end

function p8est_iter_fside_array_index_int(array, it::Int32)
    ccall((:p8est_iter_fside_array_index_int, libp4est), Ptr{p8est_iter_face_side_t}, (Ptr{sc_array_t}, Int32), array, it)
end

function p8est_iter_fside_array_index(array, it::UInt)
    ccall((:p8est_iter_fside_array_index, libp4est), Ptr{p8est_iter_face_side_t}, (Ptr{sc_array_t}, UInt), array, it)
end

function p8est_new_ext(mpicomm::MPI_Comm, connectivity, min_quadrants::p4est_locidx_t, min_level::Int32, fill_uniform::Int32, data_size::UInt, init_fn::p8est_init_t, user_pointer)
    ccall((:p8est_new_ext, libp4est), Ptr{p8est_t}, (MPI_Comm, Ptr{p8est_connectivity_t}, p4est_locidx_t, Int32, Int32, UInt, p8est_init_t, Ptr{Void}), mpicomm, connectivity, min_quadrants, min_level, fill_uniform, data_size, init_fn, user_pointer)
end

function p8est_mesh_new_ext(p4est, ghost, compute_tree_index::Int32, compute_level_lists::Int32, btype::p8est_connect_type_t)
    ccall((:p8est_mesh_new_ext, libp4est), Ptr{p8est_mesh_t}, (Ptr{p8est_t}, Ptr{p8est_ghost_t}, Int32, Int32, p8est_connect_type_t), p4est, ghost, compute_tree_index, compute_level_lists, btype)
end

function p8est_copy_ext(input, copy_data::Int32, duplicate_mpicomm::Int32)
    ccall((:p8est_copy_ext, libp4est), Ptr{p8est_t}, (Ptr{p8est_t}, Int32, Int32), input, copy_data, duplicate_mpicomm)
end

function p8est_refine_ext(p8est, refine_recursive::Int32, maxlevel::Int32, refine_fn::p8est_refine_t, init_fn::p8est_init_t, replace_fn::p8est_replace_t)
    ccall((:p8est_refine_ext, libp4est), Void, (Ptr{p8est_t}, Int32, Int32, p8est_refine_t, p8est_init_t, p8est_replace_t), p8est, refine_recursive, maxlevel, refine_fn, init_fn, replace_fn)
end

function p8est_coarsen_ext(p8est, coarsen_recursive::Int32, callback_orphans::Int32, coarsen_fn::p8est_coarsen_t, init_fn::p8est_init_t, replace_fn::p8est_replace_t)
    ccall((:p8est_coarsen_ext, libp4est), Void, (Ptr{p8est_t}, Int32, Int32, p8est_coarsen_t, p8est_init_t, p8est_replace_t), p8est, coarsen_recursive, callback_orphans, coarsen_fn, init_fn, replace_fn)
end

function p8est_balance_ext(p8est, btype::p8est_connect_type_t, init_fn::p8est_init_t, replace_fn::p8est_replace_t)
    ccall((:p8est_balance_ext, libp4est), Void, (Ptr{p8est_t}, p8est_connect_type_t, p8est_init_t, p8est_replace_t), p8est, btype, init_fn, replace_fn)
end

function p8est_balance_subtree_ext(p8est, btype::p8est_connect_type_t, which_tree::p4est_topidx_t, init_fn::p8est_init_t, replace_fn::p8est_replace_t)
    ccall((:p8est_balance_subtree_ext, libp4est), Void, (Ptr{p8est_t}, p8est_connect_type_t, p4est_topidx_t, p8est_init_t, p8est_replace_t), p8est, btype, which_tree, init_fn, replace_fn)
end

function p8est_partition_ext(p8est, partition_for_coarsening::Int32, weight_fn::p8est_weight_t)
    ccall((:p8est_partition_ext, libp4est), p4est_gloidx_t, (Ptr{p8est_t}, Int32, p8est_weight_t), p8est, partition_for_coarsening, weight_fn)
end

function p8est_partition_for_coarsening(p8est, num_quadrants_in_proc)
    ccall((:p8est_partition_for_coarsening, libp4est), p4est_gloidx_t, (Ptr{p8est_t}, Ptr{p4est_locidx_t}), p8est, num_quadrants_in_proc)
end

function p8est_iterate_ext(p8est, ghost_layer, user_data, iter_volume::p8est_iter_volume_t, iter_face::p8est_iter_face_t, iter_edge::p8est_iter_edge_t, iter_corner::p8est_iter_corner_t, remote::Int32)
    ccall((:p8est_iterate_ext, libp4est), Void, (Ptr{p8est_t}, Ptr{p8est_ghost_t}, Ptr{Void}, p8est_iter_volume_t, p8est_iter_face_t, p8est_iter_edge_t, p8est_iter_corner_t, Int32), p8est, ghost_layer, user_data, iter_volume, iter_face, iter_edge, iter_corner, remote)
end

function p8est_save_ext(filename, p8est, save_data::Int32, save_partition::Int32)
    ccall((:p8est_save_ext, libp4est), Void, (Cstring, Ptr{p8est_t}, Int32, Int32), filename, p8est, save_data, save_partition)
end

function p8est_load_ext(filename, mpicomm::MPI_Comm, data_size::UInt, load_data::Int32, autopartition::Int32, broadcasthead::Int32, user_pointer, connectivity)
    ccall((:p8est_load_ext, libp4est), Ptr{p8est_t}, (Cstring, MPI_Comm, UInt, Int32, Int32, Int32, Ptr{Void}, Ptr{Ptr{p8est_connectivity_t}}), filename, mpicomm, data_size, load_data, autopartition, broadcasthead, user_pointer, connectivity)
end

function p8est_source_ext(src, mpicomm::MPI_Comm, data_size::UInt, load_data::Int32, autopartition::Int32, broadcasthead::Int32, user_pointer, connectivity)
    ccall((:p8est_source_ext, libp4est), Ptr{p8est_t}, (Ptr{sc_io_source_t}, MPI_Comm, UInt, Int32, Int32, Int32, Ptr{Void}, Ptr{Ptr{p8est_connectivity_t}}), src, mpicomm, data_size, load_data, autopartition, broadcasthead, user_pointer, connectivity)
end

function p8est_get_plex_data_ext(p8est, ghost, lnodes, ctype::p8est_connect_type_t, overlap::Int32, first_local_quad, out_points_per_dim, out_cone_sizes, out_cones, out_cone_orientations, out_vertex_coords, out_children, out_parents, out_childids, out_leaves, out_remotes, custom_numbering::Int32)
    ccall((:p8est_get_plex_data_ext, libp4est), Void, (Ptr{p8est_t}, Ptr{Ptr{p8est_ghost_t}}, Ptr{Ptr{p8est_lnodes_t}}, p8est_connect_type_t, Int32, Ptr{p4est_locidx_t}, Ptr{sc_array_t}, Ptr{sc_array_t}, Ptr{sc_array_t}, Ptr{sc_array_t}, Ptr{sc_array_t}, Ptr{sc_array_t}, Ptr{sc_array_t}, Ptr{sc_array_t}, Ptr{sc_array_t}, Ptr{sc_array_t}, Int32), p8est, ghost, lnodes, ctype, overlap, first_local_quad, out_points_per_dim, out_cone_sizes, out_cones, out_cone_orientations, out_vertex_coords, out_children, out_parents, out_childids, out_leaves, out_remotes, custom_numbering)
end
# Julia wrapper for header: /home/iagbole/Codeentwicklung/p4est/local/include/p8est_iterate.h
# Automatically generated using Clang.jl wrap_c, version 0.0.0

# Julia wrapper for header: /home/iagbole/Codeentwicklung/p4est/local/include/p4est_base.h
# Automatically generated using Clang.jl wrap_c, version 0.0.0

# Julia wrapper for header: /home/iagbole/Codeentwicklung/p4est/local/include/p4est_extended.h
# Automatically generated using Clang.jl wrap_c, version 0.0.0


function p4est_new_ext(mpicomm::MPI_Comm, connectivity, min_quadrants::p4est_locidx_t, min_level::Int32, fill_uniform::Int32, data_size::UInt, init_fn::p4est_init_t, user_pointer)
    ccall((:p4est_new_ext, libp4est), Ptr{p4est_t}, (MPI_Comm, Ptr{p4est_connectivity_t}, p4est_locidx_t, Int32, Int32, UInt, p4est_init_t, Ptr{Void}), mpicomm, connectivity, min_quadrants, min_level, fill_uniform, data_size, init_fn, user_pointer)
end

function p4est_mesh_new_ext(p4est, ghost, compute_tree_index::Int32, compute_level_lists::Int32, btype::p4est_connect_type_t)
    ccall((:p4est_mesh_new_ext, libp4est), Ptr{p4est_mesh_t}, (Ptr{p4est_t}, Ptr{p4est_ghost_t}, Int32, Int32, p4est_connect_type_t), p4est, ghost, compute_tree_index, compute_level_lists, btype)
end

function p4est_copy_ext(input, copy_data::Int32, duplicate_mpicomm::Int32)
    ccall((:p4est_copy_ext, libp4est), Ptr{p4est_t}, (Ptr{p4est_t}, Int32, Int32), input, copy_data, duplicate_mpicomm)
end

function p4est_refine_ext(p4est, refine_recursive::Int32, maxlevel::Int32, refine_fn::p4est_refine_t, init_fn::p4est_init_t, replace_fn::p4est_replace_t)
    ccall((:p4est_refine_ext, libp4est), Void, (Ptr{p4est_t}, Int32, Int32, p4est_refine_t, p4est_init_t, p4est_replace_t), p4est, refine_recursive, maxlevel, refine_fn, init_fn, replace_fn)
end

function p4est_coarsen_ext(p4est, coarsen_recursive::Int32, callback_orphans::Int32, coarsen_fn::p4est_coarsen_t, init_fn::p4est_init_t, replace_fn::p4est_replace_t)
    ccall((:p4est_coarsen_ext, libp4est), Void, (Ptr{p4est_t}, Int32, Int32, p4est_coarsen_t, p4est_init_t, p4est_replace_t), p4est, coarsen_recursive, callback_orphans, coarsen_fn, init_fn, replace_fn)
end

function p4est_balance_ext(p4est, btype::p4est_connect_type_t, init_fn::p4est_init_t, replace_fn::p4est_replace_t)
    ccall((:p4est_balance_ext, libp4est), Void, (Ptr{p4est_t}, p4est_connect_type_t, p4est_init_t, p4est_replace_t), p4est, btype, init_fn, replace_fn)
end

function p4est_balance_subtree_ext(p4est, btype::p4est_connect_type_t, which_tree::p4est_topidx_t, init_fn::p4est_init_t, replace_fn::p4est_replace_t)
    ccall((:p4est_balance_subtree_ext, libp4est), Void, (Ptr{p4est_t}, p4est_connect_type_t, p4est_topidx_t, p4est_init_t, p4est_replace_t), p4est, btype, which_tree, init_fn, replace_fn)
end

function p4est_partition_ext(p4est, partition_for_coarsening::Int32, weight_fn::p4est_weight_t)
    ccall((:p4est_partition_ext, libp4est), p4est_gloidx_t, (Ptr{p4est_t}, Int32, p4est_weight_t), p4est, partition_for_coarsening, weight_fn)
end

function p4est_partition_for_coarsening(p4est, num_quadrants_in_proc)
    ccall((:p4est_partition_for_coarsening, libp4est), p4est_gloidx_t, (Ptr{p4est_t}, Ptr{p4est_locidx_t}), p4est, num_quadrants_in_proc)
end

function p4est_iterate_ext(p4est, ghost_layer, user_data, iter_volume::p4est_iter_volume_t, iter_face::p4est_iter_face_t, iter_corner::p4est_iter_corner_t, remote::Int32)
    ccall((:p4est_iterate_ext, libp4est), Void, (Ptr{p4est_t}, Ptr{p4est_ghost_t}, Ptr{Void}, p4est_iter_volume_t, p4est_iter_face_t, p4est_iter_corner_t, Int32), p4est, ghost_layer, user_data, iter_volume, iter_face, iter_corner, remote)
end

function p4est_save_ext(filename, p4est, save_data::Int32, save_partition::Int32)
    ccall((:p4est_save_ext, libp4est), Void, (Cstring, Ptr{p4est_t}, Int32, Int32), filename, p4est, save_data, save_partition)
end

function p4est_load_ext(filename, mpicomm::MPI_Comm, data_size::UInt, load_data::Int32, autopartition::Int32, broadcasthead::Int32, user_pointer, connectivity)
    ccall((:p4est_load_ext, libp4est), Ptr{p4est_t}, (Cstring, MPI_Comm, UInt, Int32, Int32, Int32, Ptr{Void}, Ptr{Ptr{p4est_connectivity_t}}), filename, mpicomm, data_size, load_data, autopartition, broadcasthead, user_pointer, connectivity)
end

function p4est_source_ext(src, mpicomm::MPI_Comm, data_size::UInt, load_data::Int32, autopartition::Int32, broadcasthead::Int32, user_pointer, connectivity)
    ccall((:p4est_source_ext, libp4est), Ptr{p4est_t}, (Ptr{sc_io_source_t}, MPI_Comm, UInt, Int32, Int32, Int32, Ptr{Void}, Ptr{Ptr{p4est_connectivity_t}}), src, mpicomm, data_size, load_data, autopartition, broadcasthead, user_pointer, connectivity)
end

function p4est_get_plex_data_ext(p4est, ghost, lnodes, ctype::p4est_connect_type_t, overlap::Int32, first_local_quad, out_points_per_dim, out_cone_sizes, out_cones, out_cone_orientations, out_vertex_coords, out_children, out_parents, out_childids, out_leaves, out_remotes, custom_numbering::Int32)
    ccall((:p4est_get_plex_data_ext, libp4est), Void, (Ptr{p4est_t}, Ptr{Ptr{p4est_ghost_t}}, Ptr{Ptr{p4est_lnodes_t}}, p4est_connect_type_t, Int32, Ptr{p4est_locidx_t}, Ptr{sc_array_t}, Ptr{sc_array_t}, Ptr{sc_array_t}, Ptr{sc_array_t}, Ptr{sc_array_t}, Ptr{sc_array_t}, Ptr{sc_array_t}, Ptr{sc_array_t}, Ptr{sc_array_t}, Ptr{sc_array_t}, Int32), p4est, ghost, lnodes, ctype, overlap, first_local_quad, out_points_per_dim, out_cone_sizes, out_cones, out_cone_orientations, out_vertex_coords, out_children, out_parents, out_childids, out_leaves, out_remotes, custom_numbering)
end
# Julia wrapper for header: /home/iagbole/Codeentwicklung/p4est/local/include/p6est.h
# Automatically generated using Clang.jl wrap_c, version 0.0.0

# Julia wrapper for header: /home/iagbole/Codeentwicklung/p4est/local/include/p6est_ghost.h
# Automatically generated using Clang.jl wrap_c, version 0.0.0

# Julia wrapper for header: /home/iagbole/Codeentwicklung/p4est/local/include/p4est_lnodes.h
# Automatically generated using Clang.jl wrap_c, version 0.0.0

# Julia wrapper for header: /home/iagbole/Codeentwicklung/p4est/local/include/p8est_mesh.h
# Automatically generated using Clang.jl wrap_c, version 0.0.0

# Julia wrapper for header: /home/iagbole/Codeentwicklung/p4est/local/include/p4est_config.h
# Automatically generated using Clang.jl wrap_c, version 0.0.0

# Julia wrapper for header: /home/iagbole/Codeentwicklung/p4est/local/include/p8est_lnodes.h
# Automatically generated using Clang.jl wrap_c, version 0.0.0

# Julia wrapper for header: /home/iagbole/Codeentwicklung/p4est/local/include/p8est_tets_hexes.h
# Automatically generated using Clang.jl wrap_c, version 0.0.0


function p8est_tets_read_node(nodefile)
    ccall((:p8est_tets_read_node, libp4est), Ptr{sc_array_t}, (Cstring,), nodefile)
end

function p8est_tets_read_ele(elefile, num_nodes::p4est_topidx_t, attributes)
    ccall((:p8est_tets_read_ele, libp4est), Ptr{sc_array_t}, (Cstring, p4est_topidx_t, Ptr{Ptr{sc_array_t}}), elefile, num_nodes, attributes)
end

function p8est_tets_read(tetgenbasename)
    ccall((:p8est_tets_read, libp4est), Ptr{p8est_tets_t}, (Cstring,), tetgenbasename)
end

function p8est_tets_destroy(ptg)
    ccall((:p8est_tets_destroy, libp4est), Void, (Ptr{p8est_tets_t},), ptg)
end

function p8est_tets_make_righthanded(ptg)
    ccall((:p8est_tets_make_righthanded, libp4est), p4est_topidx_t, (Ptr{p8est_tets_t},), ptg)
end

function p8est_connectivity_new_tets(ptg)
    ccall((:p8est_connectivity_new_tets, libp4est), Ptr{p8est_connectivity_t}, (Ptr{p8est_tets_t},), ptg)
end
# Julia wrapper for header: /home/iagbole/Codeentwicklung/p4est/local/include/p8est_balance.h
# Automatically generated using Clang.jl wrap_c, version 0.0.0


function p8est_balance_seeds(q, p, balance::p8est_connect_type_t, seeds)
    ccall((:p8est_balance_seeds, libp4est), Int32, (Ptr{p8est_quadrant_t}, Ptr{p8est_quadrant_t}, p8est_connect_type_t, Ptr{sc_array_t}), q, p, balance, seeds)
end

function p8est_balance_seeds_face(q, p, face::Int32, balance::p8est_connect_type_t, seeds)
    ccall((:p8est_balance_seeds_face, libp4est), Int32, (Ptr{p8est_quadrant_t}, Ptr{p8est_quadrant_t}, Int32, p8est_connect_type_t, Ptr{sc_array_t}), q, p, face, balance, seeds)
end

function p8est_balance_seeds_edge(q, p, face::Int32, balance::p8est_connect_type_t, seeds)
    ccall((:p8est_balance_seeds_edge, libp4est), Int32, (Ptr{p8est_quadrant_t}, Ptr{p8est_quadrant_t}, Int32, p8est_connect_type_t, Ptr{sc_array_t}), q, p, face, balance, seeds)
end

function p8est_balance_seeds_corner(q, p, face::Int32, balance::p8est_connect_type_t, seeds)
    ccall((:p8est_balance_seeds_corner, libp4est), Int32, (Ptr{p8est_quadrant_t}, Ptr{p8est_quadrant_t}, Int32, p8est_connect_type_t, Ptr{sc_array_t}), q, p, face, balance, seeds)
end
# Julia wrapper for header: /home/iagbole/Codeentwicklung/p4est/local/include/p4est_bits.h
# Automatically generated using Clang.jl wrap_c, version 0.0.0


function p4est_quadrant_print(log_priority::Int32, q)
    ccall((:p4est_quadrant_print, libp4est), Void, (Int32, Ptr{p4est_quadrant_t}), log_priority, q)
end

function p4est_quadrant_is_equal(q1, q2)
    ccall((:p4est_quadrant_is_equal, libp4est), Int32, (Ptr{p4est_quadrant_t}, Ptr{p4est_quadrant_t}), q1, q2)
end

function p4est_quadrant_overlaps(q1, q2)
    ccall((:p4est_quadrant_overlaps, libp4est), Int32, (Ptr{p4est_quadrant_t}, Ptr{p4est_quadrant_t}), q1, q2)
end

function p4est_quadrant_is_equal_piggy(q1, q2)
    ccall((:p4est_quadrant_is_equal_piggy, libp4est), Int32, (Ptr{p4est_quadrant_t}, Ptr{p4est_quadrant_t}), q1, q2)
end

function p4est_quadrant_compare(v1, v2)
    ccall((:p4est_quadrant_compare, libp4est), Int32, (Ptr{Void}, Ptr{Void}), v1, v2)
end

function p4est_quadrant_disjoint(v1, v2)
    ccall((:p4est_quadrant_disjoint, libp4est), Int32, (Ptr{Void}, Ptr{Void}), v1, v2)
end

function p4est_quadrant_compare_piggy(v1, v2)
    ccall((:p4est_quadrant_compare_piggy, libp4est), Int32, (Ptr{Void}, Ptr{Void}), v1, v2)
end

function p4est_quadrant_compare_local_num(v1, v2)
    ccall((:p4est_quadrant_compare_local_num, libp4est), Int32, (Ptr{Void}, Ptr{Void}), v1, v2)
end

function p4est_quadrant_equal_fn(v1, v2, u)
    ccall((:p4est_quadrant_equal_fn, libp4est), Int32, (Ptr{Void}, Ptr{Void}, Ptr{Void}), v1, v2, u)
end

function p4est_quadrant_hash_fn(v, u)
    ccall((:p4est_quadrant_hash_fn, libp4est), UInt32, (Ptr{Void}, Ptr{Void}), v, u)
end

function p4est_node_equal_piggy_fn(v1, v2, u)
    ccall((:p4est_node_equal_piggy_fn, libp4est), Int32, (Ptr{Void}, Ptr{Void}, Ptr{Void}), v1, v2, u)
end

function p4est_node_hash_piggy_fn(v, u)
    ccall((:p4est_node_hash_piggy_fn, libp4est), UInt32, (Ptr{Void}, Ptr{Void}), v, u)
end

function p4est_node_clamp_inside(n, r)
    ccall((:p4est_node_clamp_inside, libp4est), Void, (Ptr{p4est_quadrant_t}, Ptr{p4est_quadrant_t}), n, r)
end

function p4est_node_unclamp(n)
    ccall((:p4est_node_unclamp, libp4est), Void, (Ptr{p4est_quadrant_t},), n)
end

function p4est_node_to_quadrant(n, level::Int32, q)
    ccall((:p4est_node_to_quadrant, libp4est), Void, (Ptr{p4est_quadrant_t}, Int32, Ptr{p4est_quadrant_t}), n, level, q)
end

function p4est_quadrant_contains_node(q, n)
    ccall((:p4est_quadrant_contains_node, libp4est), Int32, (Ptr{p4est_quadrant_t}, Ptr{p4est_quadrant_t}), q, n)
end

function p4est_quadrant_ancestor_id(q, level::Int32)
    ccall((:p4est_quadrant_ancestor_id, libp4est), Int32, (Ptr{p4est_quadrant_t}, Int32), q, level)
end

function p4est_quadrant_child_id(q)
    ccall((:p4est_quadrant_child_id, libp4est), Int32, (Ptr{p4est_quadrant_t},), q)
end

function p4est_quadrant_is_inside_root(q)
    ccall((:p4est_quadrant_is_inside_root, libp4est), Int32, (Ptr{p4est_quadrant_t},), q)
end

function p4est_quadrant_is_inside_3x3(q)
    ccall((:p4est_quadrant_is_inside_3x3, libp4est), Int32, (Ptr{p4est_quadrant_t},), q)
end

function p4est_quadrant_is_outside_face(q)
    ccall((:p4est_quadrant_is_outside_face, libp4est), Int32, (Ptr{p4est_quadrant_t},), q)
end

function p4est_quadrant_is_outside_corner(q)
    ccall((:p4est_quadrant_is_outside_corner, libp4est), Int32, (Ptr{p4est_quadrant_t},), q)
end

function p4est_quadrant_is_node(q, inside::Int32)
    ccall((:p4est_quadrant_is_node, libp4est), Int32, (Ptr{p4est_quadrant_t}, Int32), q, inside)
end

function p4est_quadrant_is_valid(q)
    ccall((:p4est_quadrant_is_valid, libp4est), Int32, (Ptr{p4est_quadrant_t},), q)
end

function p4est_quadrant_is_extended(q)
    ccall((:p4est_quadrant_is_extended, libp4est), Int32, (Ptr{p4est_quadrant_t},), q)
end

function p4est_quadrant_is_sibling(q1, q2)
    ccall((:p4est_quadrant_is_sibling, libp4est), Int32, (Ptr{p4est_quadrant_t}, Ptr{p4est_quadrant_t}), q1, q2)
end

function p4est_quadrant_is_sibling_D(q1, q2)
    ccall((:p4est_quadrant_is_sibling_D, libp4est), Int32, (Ptr{p4est_quadrant_t}, Ptr{p4est_quadrant_t}), q1, q2)
end

function p4est_quadrant_is_family(q0, q1, q2, q3)
    ccall((:p4est_quadrant_is_family, libp4est), Int32, (Ptr{p4est_quadrant_t}, Ptr{p4est_quadrant_t}, Ptr{p4est_quadrant_t}, Ptr{p4est_quadrant_t}), q0, q1, q2, q3)
end

function p4est_quadrant_is_familyv(q)
    ccall((:p4est_quadrant_is_familyv, libp4est), Int32, (Ptr{p4est_quadrant_t},), q)
end

function p4est_quadrant_is_familypv(q)
    ccall((:p4est_quadrant_is_familypv, libp4est), Int32, (Ptr{Ptr{p4est_quadrant_t}},), q)
end

function p4est_quadrant_is_parent(q, r)
    ccall((:p4est_quadrant_is_parent, libp4est), Int32, (Ptr{p4est_quadrant_t}, Ptr{p4est_quadrant_t}), q, r)
end

function p4est_quadrant_is_parent_D(q, r)
    ccall((:p4est_quadrant_is_parent_D, libp4est), Int32, (Ptr{p4est_quadrant_t}, Ptr{p4est_quadrant_t}), q, r)
end

function p4est_quadrant_is_ancestor(q, r)
    ccall((:p4est_quadrant_is_ancestor, libp4est), Int32, (Ptr{p4est_quadrant_t}, Ptr{p4est_quadrant_t}), q, r)
end

function p4est_quadrant_is_ancestor_D(q, r)
    ccall((:p4est_quadrant_is_ancestor_D, libp4est), Int32, (Ptr{p4est_quadrant_t}, Ptr{p4est_quadrant_t}), q, r)
end

function p4est_quadrant_is_next(q, r)
    ccall((:p4est_quadrant_is_next, libp4est), Int32, (Ptr{p4est_quadrant_t}, Ptr{p4est_quadrant_t}), q, r)
end

function p4est_quadrant_is_next_D(q, r)
    ccall((:p4est_quadrant_is_next_D, libp4est), Int32, (Ptr{p4est_quadrant_t}, Ptr{p4est_quadrant_t}), q, r)
end

function p4est_quadrant_overlaps_tree(tree, q)
    ccall((:p4est_quadrant_overlaps_tree, libp4est), Int32, (Ptr{p4est_tree_t}, Ptr{p4est_quadrant_t}), tree, q)
end

function p4est_quadrant_is_inside_tree(tree, q)
    ccall((:p4est_quadrant_is_inside_tree, libp4est), Int32, (Ptr{p4est_tree_t}, Ptr{p4est_quadrant_t}), tree, q)
end

function p4est_quadrant_ancestor(q, level::Int32, r)
    ccall((:p4est_quadrant_ancestor, libp4est), Void, (Ptr{p4est_quadrant_t}, Int32, Ptr{p4est_quadrant_t}), q, level, r)
end

function p4est_quadrant_parent(q, r)
    ccall((:p4est_quadrant_parent, libp4est), Void, (Ptr{p4est_quadrant_t}, Ptr{p4est_quadrant_t}), q, r)
end

function p4est_quadrant_sibling(q, r, sibling_id::Int32)
    ccall((:p4est_quadrant_sibling, libp4est), Void, (Ptr{p4est_quadrant_t}, Ptr{p4est_quadrant_t}, Int32), q, r, sibling_id)
end

function p4est_quadrant_child(q, r, child_id::Int32)
    ccall((:p4est_quadrant_child, libp4est), Void, (Ptr{p4est_quadrant_t}, Ptr{p4est_quadrant_t}, Int32), q, r, child_id)
end

function p4est_quadrant_face_neighbor(q, face::Int32, r)
    ccall((:p4est_quadrant_face_neighbor, libp4est), Void, (Ptr{p4est_quadrant_t}, Int32, Ptr{p4est_quadrant_t}), q, face, r)
end

function p4est_quadrant_face_neighbor_extra(q, t::p4est_topidx_t, face::Int32, r, nface, conn)
    ccall((:p4est_quadrant_face_neighbor_extra, libp4est), p4est_topidx_t, (Ptr{p4est_quadrant_t}, p4est_topidx_t, Int32, Ptr{p4est_quadrant_t}, Ptr{Int32}, Ptr{p4est_connectivity_t}), q, t, face, r, nface, conn)
end

function p4est_quadrant_half_face_neighbors(q, face::Int32, n, nur)
    ccall((:p4est_quadrant_half_face_neighbors, libp4est), Void, (Ptr{p4est_quadrant_t}, Int32, Ptr{p4est_quadrant_t}, Ptr{p4est_quadrant_t}), q, face, n, nur)
end

function p4est_quadrant_all_face_neighbors(q, face::Int32, n)
    ccall((:p4est_quadrant_all_face_neighbors, libp4est), Void, (Ptr{p4est_quadrant_t}, Int32, Ptr{p4est_quadrant_t}), q, face, n)
end

function p4est_quadrant_corner_neighbor(q, corner::Int32, r)
    ccall((:p4est_quadrant_corner_neighbor, libp4est), Void, (Ptr{p4est_quadrant_t}, Int32, Ptr{p4est_quadrant_t}), q, corner, r)
end

function p4est_quadrant_corner_neighbor_extra(q, t::p4est_locidx_t, corner::Int32, quads, treeids, ncorners, conn)
    ccall((:p4est_quadrant_corner_neighbor_extra, libp4est), Void, (Ptr{p4est_quadrant_t}, p4est_locidx_t, Int32, Ptr{sc_array_t}, Ptr{sc_array_t}, Ptr{sc_array_t}, Ptr{p4est_connectivity_t}), q, t, corner, quads, treeids, ncorners, conn)
end

function p4est_quadrant_half_corner_neighbor(q, corner::Int32, r)
    ccall((:p4est_quadrant_half_corner_neighbor, libp4est), Void, (Ptr{p4est_quadrant_t}, Int32, Ptr{p4est_quadrant_t}), q, corner, r)
end

function p4est_quadrant_corner_node(q, corner::Int32, r)
    ccall((:p4est_quadrant_corner_node, libp4est), Void, (Ptr{p4est_quadrant_t}, Int32, Ptr{p4est_quadrant_t}), q, corner, r)
end

function p4est_quadrant_children(q, c0, c1, c2, c3)
    ccall((:p4est_quadrant_children, libp4est), Void, (Ptr{p4est_quadrant_t}, Ptr{p4est_quadrant_t}, Ptr{p4est_quadrant_t}, Ptr{p4est_quadrant_t}, Ptr{p4est_quadrant_t}), q, c0, c1, c2, c3)
end

function p4est_quadrant_childrenv(q, c)
    ccall((:p4est_quadrant_childrenv, libp4est), Void, (Ptr{p4est_quadrant_t}, Ptr{p4est_quadrant_t}), q, c)
end

function p4est_quadrant_childrenpv(q, c)
    ccall((:p4est_quadrant_childrenpv, libp4est), Void, (Ptr{p4est_quadrant_t}, Ptr{Ptr{p4est_quadrant_t}}), q, c)
end

function p4est_quadrant_first_descendant(q, fd, level::Int32)
    ccall((:p4est_quadrant_first_descendant, libp4est), Void, (Ptr{p4est_quadrant_t}, Ptr{p4est_quadrant_t}, Int32), q, fd, level)
end

function p4est_quadrant_last_descendant(q, ld, level::Int32)
    ccall((:p4est_quadrant_last_descendant, libp4est), Void, (Ptr{p4est_quadrant_t}, Ptr{p4est_quadrant_t}, Int32), q, ld, level)
end

function p4est_quadrant_corner_descendant(q, r, c::Int32, level::Int32)
    ccall((:p4est_quadrant_corner_descendant, libp4est), Void, (Ptr{p4est_quadrant_t}, Ptr{p4est_quadrant_t}, Int32, Int32), q, r, c, level)
end

function p4est_nearest_common_ancestor(q1, q2, r)
    ccall((:p4est_nearest_common_ancestor, libp4est), Void, (Ptr{p4est_quadrant_t}, Ptr{p4est_quadrant_t}, Ptr{p4est_quadrant_t}), q1, q2, r)
end

function p4est_nearest_common_ancestor_D(q1, q2, r)
    ccall((:p4est_nearest_common_ancestor_D, libp4est), Void, (Ptr{p4est_quadrant_t}, Ptr{p4est_quadrant_t}, Ptr{p4est_quadrant_t}), q1, q2, r)
end

function p4est_quadrant_transform_face(q, r, ftransform)
    ccall((:p4est_quadrant_transform_face, libp4est), Void, (Ptr{p4est_quadrant_t}, Ptr{p4est_quadrant_t}, Ptr{Int32}), q, r, ftransform)
end

function p4est_quadrant_touches_corner(q, corner::Int32, inside::Int32)
    ccall((:p4est_quadrant_touches_corner, libp4est), Int32, (Ptr{p4est_quadrant_t}, Int32, Int32), q, corner, inside)
end

function p4est_quadrant_transform_corner(q, icorner::Int32, inside::Int32)
    ccall((:p4est_quadrant_transform_corner, libp4est), Void, (Ptr{p4est_quadrant_t}, Int32, Int32), q, icorner, inside)
end

function p4est_quadrant_shift_corner(q, r, corner::Int32)
    ccall((:p4est_quadrant_shift_corner, libp4est), Void, (Ptr{p4est_quadrant_t}, Ptr{p4est_quadrant_t}, Int32), q, r, corner)
end

function p4est_quadrant_linear_id(quadrant, level::Int32)
    ccall((:p4est_quadrant_linear_id, libp4est), UInt64, (Ptr{p4est_quadrant_t}, Int32), quadrant, level)
end

function p4est_quadrant_set_morton(quadrant, level::Int32, id::UInt64)
    ccall((:p4est_quadrant_set_morton, libp4est), Void, (Ptr{p4est_quadrant_t}, Int32, UInt64), quadrant, level, id)
end
# Julia wrapper for header: /home/iagbole/Codeentwicklung/p4est/local/include/p8est_plex.h
# Automatically generated using Clang.jl wrap_c, version 0.0.0


function p8est_get_plex_data(p8est, ctype::p8est_connect_type_t, overlap::Int32, first_local_quad, out_points_per_dim, out_cone_sizes, out_cones, out_cone_orientations, out_vertex_coords, out_children, out_parents, out_childids, out_leaves, out_remotes)
    ccall((:p8est_get_plex_data, libp4est), Void, (Ptr{p8est_t}, p8est_connect_type_t, Int32, Ptr{p4est_locidx_t}, Ptr{sc_array_t}, Ptr{sc_array_t}, Ptr{sc_array_t}, Ptr{sc_array_t}, Ptr{sc_array_t}, Ptr{sc_array_t}, Ptr{sc_array_t}, Ptr{sc_array_t}, Ptr{sc_array_t}, Ptr{sc_array_t}), p8est, ctype, overlap, first_local_quad, out_points_per_dim, out_cone_sizes, out_cones, out_cone_orientations, out_vertex_coords, out_children, out_parents, out_childids, out_leaves, out_remotes)
end
# Julia wrapper for header: /home/iagbole/Codeentwicklung/p4est/local/include/p4est_search.h
# Automatically generated using Clang.jl wrap_c, version 0.0.0


function p4est_find_lower_bound(array, q, guess::UInt)
    ccall((:p4est_find_lower_bound, libp4est), Cssize_t, (Ptr{sc_array_t}, Ptr{p4est_quadrant_t}, UInt), array, q, guess)
end

function p4est_find_higher_bound(array, q, guess::UInt)
    ccall((:p4est_find_higher_bound, libp4est), Cssize_t, (Ptr{sc_array_t}, Ptr{p4est_quadrant_t}, UInt), array, q, guess)
end

function p4est_split_array(array, level::Int32, indices::Int32)
    ccall((:p4est_split_array, libp4est), Void, (Ptr{sc_array_t}, Int32, Int32), array, level, indices)
end

function p4est_find_range_boundaries(lq, uq, level::Int32, faces, corners)
    ccall((:p4est_find_range_boundaries, libp4est), Int32, (Ptr{p4est_quadrant_t}, Ptr{p4est_quadrant_t}, Int32, Ptr{Int32}, Ptr{Int32}), lq, uq, level, faces, corners)
end

function p4est_search(p4est, search_quadrant_fn::p4est_search_query_t, search_point_fn::p4est_search_query_t, points)
    ccall((:p4est_search, libp4est), Void, (Ptr{p4est_t}, p4est_search_query_t, p4est_search_query_t, Ptr{sc_array_t}), p4est, search_quadrant_fn, search_point_fn, points)
end
# Julia wrapper for header: /home/iagbole/Codeentwicklung/p4est/local/include/p8est_search.h
# Automatically generated using Clang.jl wrap_c, version 0.0.0


function p8est_find_lower_bound(array, q, guess::UInt)
    ccall((:p8est_find_lower_bound, libp4est), Cssize_t, (Ptr{sc_array_t}, Ptr{p8est_quadrant_t}, UInt), array, q, guess)
end

function p8est_find_higher_bound(array, q, guess::UInt)
    ccall((:p8est_find_higher_bound, libp4est), Cssize_t, (Ptr{sc_array_t}, Ptr{p8est_quadrant_t}, UInt), array, q, guess)
end

function p8est_split_array(array, level::Int32, indices::Int32)
    ccall((:p8est_split_array, libp4est), Void, (Ptr{sc_array_t}, Int32, Int32), array, level, indices)
end

function p8est_find_range_boundaries(lq, uq, level::Int32, faces, edges, corners)
    ccall((:p8est_find_range_boundaries, libp4est), Int32, (Ptr{p8est_quadrant_t}, Ptr{p8est_quadrant_t}, Int32, Ptr{Int32}, Ptr{Int32}, Ptr{Int32}), lq, uq, level, faces, edges, corners)
end

function p8est_search(p8est, search_quadrant_fn::p8est_search_query_t, search_point_fn::p8est_search_query_t, points)
    ccall((:p8est_search, libp4est), Void, (Ptr{p8est_t}, p8est_search_query_t, p8est_search_query_t, Ptr{sc_array_t}), p8est, search_quadrant_fn, search_point_fn, points)
end

function p8est_traverse(p8est, traverse_fn::p8est_traverse_query_t)
    ccall((:p8est_traverse, libp4est), Void, (Ptr{p8est_t}, p8est_traverse_query_t), p8est, traverse_fn)
end
# Julia wrapper for header: /home/iagbole/Codeentwicklung/p4est/local/include/p4est_wrap.h
# Automatically generated using Clang.jl wrap_c, version 0.0.0


function p4est_wrap_new_conn(mpicomm::MPI_Comm, conn, initial_level::Int32)
    ccall((:p4est_wrap_new_conn, libp4est), Ptr{p4est_wrap_t}, (MPI_Comm, Ptr{p4est_connectivity_t}, Int32), mpicomm, conn, initial_level)
end

function p4est_wrap_new_p4est(p4est, hollow::Int32, btype::p4est_connect_type_t, replace_fn::p4est_replace_t, user_pointer)
    ccall((:p4est_wrap_new_p4est, libp4est), Ptr{p4est_wrap_t}, (Ptr{p4est_t}, Int32, p4est_connect_type_t, p4est_replace_t, Ptr{Void}), p4est, hollow, btype, replace_fn, user_pointer)
end

function p4est_wrap_new_ext(mpicomm::MPI_Comm, conn, initial_level::Int32, hollow::Int32, btype::p4est_connect_type_t, replace_fn::p4est_replace_t, user_pointer)
    ccall((:p4est_wrap_new_ext, libp4est), Ptr{p4est_wrap_t}, (MPI_Comm, Ptr{p4est_connectivity_t}, Int32, Int32, p4est_connect_type_t, p4est_replace_t, Ptr{Void}), mpicomm, conn, initial_level, hollow, btype, replace_fn, user_pointer)
end

function p4est_wrap_new_copy(source, data_size::UInt, replace_fn::p4est_replace_t, user_pointer)
    ccall((:p4est_wrap_new_copy, libp4est), Ptr{p4est_wrap_t}, (Ptr{p4est_wrap_t}, UInt, p4est_replace_t, Ptr{Void}), source, data_size, replace_fn, user_pointer)
end

function p4est_wrap_new_unitsquare(mpicomm::MPI_Comm, initial_level::Int32)
    ccall((:p4est_wrap_new_unitsquare, libp4est), Ptr{p4est_wrap_t}, (MPI_Comm, Int32), mpicomm, initial_level)
end

function p4est_wrap_new_periodic(mpicomm::MPI_Comm, initial_level::Int32)
    ccall((:p4est_wrap_new_periodic, libp4est), Ptr{p4est_wrap_t}, (MPI_Comm, Int32), mpicomm, initial_level)
end

function p4est_wrap_new_rotwrap(mpicomm::MPI_Comm, initial_level::Int32)
    ccall((:p4est_wrap_new_rotwrap, libp4est), Ptr{p4est_wrap_t}, (MPI_Comm, Int32), mpicomm, initial_level)
end

function p4est_wrap_new_corner(mpicomm::MPI_Comm, initial_level::Int32)
    ccall((:p4est_wrap_new_corner, libp4est), Ptr{p4est_wrap_t}, (MPI_Comm, Int32), mpicomm, initial_level)
end

function p4est_wrap_new_pillow(mpicomm::MPI_Comm, initial_level::Int32)
    ccall((:p4est_wrap_new_pillow, libp4est), Ptr{p4est_wrap_t}, (MPI_Comm, Int32), mpicomm, initial_level)
end

function p4est_wrap_new_moebius(mpicomm::MPI_Comm, initial_level::Int32)
    ccall((:p4est_wrap_new_moebius, libp4est), Ptr{p4est_wrap_t}, (MPI_Comm, Int32), mpicomm, initial_level)
end

function p4est_wrap_new_cubed(mpicomm::MPI_Comm, initial_level::Int32)
    ccall((:p4est_wrap_new_cubed, libp4est), Ptr{p4est_wrap_t}, (MPI_Comm, Int32), mpicomm, initial_level)
end

function p4est_wrap_new_disk(mpicomm::MPI_Comm, initial_level::Int32)
    ccall((:p4est_wrap_new_disk, libp4est), Ptr{p4est_wrap_t}, (MPI_Comm, Int32), mpicomm, initial_level)
end

function p4est_wrap_new_brick(mpicomm::MPI_Comm, bx::Int32, by::Int32, px::Int32, py::Int32, initial_level::Int32)
    ccall((:p4est_wrap_new_brick, libp4est), Ptr{p4est_wrap_t}, (MPI_Comm, Int32, Int32, Int32, Int32, Int32), mpicomm, bx, by, px, py, initial_level)
end

function p4est_wrap_new_world(initial_level::Int32)
    ccall((:p4est_wrap_new_world, libp4est), Ptr{p4est_wrap_t}, (Int32,), initial_level)
end

function p4est_wrap_destroy(pp)
    ccall((:p4est_wrap_destroy, libp4est), Void, (Ptr{p4est_wrap_t},), pp)
end

function p4est_wrap_set_hollow(pp, hollow::Int32)
    ccall((:p4est_wrap_set_hollow, libp4est), Void, (Ptr{p4est_wrap_t}, Int32), pp, hollow)
end

function p4est_wrap_set_coarsen_delay(pp, coarsen_delay::Int32, coarsen_affect::Int32)
    ccall((:p4est_wrap_set_coarsen_delay, libp4est), Void, (Ptr{p4est_wrap_t}, Int32, Int32), pp, coarsen_delay, coarsen_affect)
end

function p4est_wrap_get_ghost(pp)
    ccall((:p4est_wrap_get_ghost, libp4est), Ptr{p4est_ghost_t}, (Ptr{p4est_wrap_t},), pp)
end

function p4est_wrap_get_mesh(pp)
    ccall((:p4est_wrap_get_mesh, libp4est), Ptr{p4est_mesh_t}, (Ptr{p4est_wrap_t},), pp)
end

function p4est_wrap_mark_refine(pp, which_tree::p4est_topidx_t, which_quad::p4est_locidx_t)
    ccall((:p4est_wrap_mark_refine, libp4est), Void, (Ptr{p4est_wrap_t}, p4est_topidx_t, p4est_locidx_t), pp, which_tree, which_quad)
end

function p4est_wrap_mark_coarsen(pp, which_tree::p4est_topidx_t, which_quad::p4est_locidx_t)
    ccall((:p4est_wrap_mark_coarsen, libp4est), Void, (Ptr{p4est_wrap_t}, p4est_topidx_t, p4est_locidx_t), pp, which_tree, which_quad)
end

function p4est_wrap_adapt(pp)
    ccall((:p4est_wrap_adapt, libp4est), Int32, (Ptr{p4est_wrap_t},), pp)
end

function p4est_wrap_partition(pp, weight_exponent::Int32, unchanged_first, unchanged_length, unchanged_old_first)
    ccall((:p4est_wrap_partition, libp4est), Int32, (Ptr{p4est_wrap_t}, Int32, Ptr{p4est_locidx_t}, Ptr{p4est_locidx_t}, Ptr{p4est_locidx_t}), pp, weight_exponent, unchanged_first, unchanged_length, unchanged_old_first)
end

function p4est_wrap_complete(pp)
    ccall((:p4est_wrap_complete, libp4est), Void, (Ptr{p4est_wrap_t},), pp)
end

function p4est_wrap_leaf_first(pp, track_mirrors::Int32)
    ccall((:p4est_wrap_leaf_first, libp4est), Ptr{p4est_wrap_leaf_t}, (Ptr{p4est_wrap_t}, Int32), pp, track_mirrors)
end

function p4est_wrap_leaf_next(leaf)
    ccall((:p4est_wrap_leaf_next, libp4est), Ptr{p4est_wrap_leaf_t}, (Ptr{p4est_wrap_leaf_t},), leaf)
end
# Julia wrapper for header: /home/iagbole/Codeentwicklung/p4est/local/include/p4est_algorithms.h
# Automatically generated using Clang.jl wrap_c, version 0.0.0


function p4est_quadrant_init_data(p4est, which_tree::p4est_topidx_t, quad, init_fn::p4est_init_t)
    ccall((:p4est_quadrant_init_data, libp4est), Void, (Ptr{p4est_t}, p4est_topidx_t, Ptr{p4est_quadrant_t}, p4est_init_t), p4est, which_tree, quad, init_fn)
end

function p4est_quadrant_free_data(p4est, quad)
    ccall((:p4est_quadrant_free_data, libp4est), Void, (Ptr{p4est_t}, Ptr{p4est_quadrant_t}), p4est, quad)
end

function p4est_quadrant_checksum(quadrants, checkarray, first_quadrant::UInt)
    ccall((:p4est_quadrant_checksum, libp4est), UInt32, (Ptr{sc_array_t}, Ptr{sc_array_t}, UInt), quadrants, checkarray, first_quadrant)
end

function p4est_tree_is_sorted(tree)
    ccall((:p4est_tree_is_sorted, libp4est), Int32, (Ptr{p4est_tree_t},), tree)
end

function p4est_tree_is_linear(tree)
    ccall((:p4est_tree_is_linear, libp4est), Int32, (Ptr{p4est_tree_t},), tree)
end

function p4est_tree_is_complete(tree)
    ccall((:p4est_tree_is_complete, libp4est), Int32, (Ptr{p4est_tree_t},), tree)
end

function p4est_tree_is_almost_sorted(tree, check_linearity::Int32)
    ccall((:p4est_tree_is_almost_sorted, libp4est), Int32, (Ptr{p4est_tree_t}, Int32), tree, check_linearity)
end

function p4est_tree_print(log_priority::Int32, tree)
    ccall((:p4est_tree_print, libp4est), Void, (Int32, Ptr{p4est_tree_t}), log_priority, tree)
end

function p4est_is_equal(p4est1, p4est2, compare_data::Int32)
    ccall((:p4est_is_equal, libp4est), Int32, (Ptr{p4est_t}, Ptr{p4est_t}, Int32), p4est1, p4est2, compare_data)
end

function p4est_is_valid(p4est)
    ccall((:p4est_is_valid, libp4est), Int32, (Ptr{p4est_t},), p4est)
end

function p4est_tree_compute_overlap(p4est, _in, out, balance::p4est_connect_type_t, borders, inseeds)
    ccall((:p4est_tree_compute_overlap, libp4est), Void, (Ptr{p4est_t}, Ptr{sc_array_t}, Ptr{sc_array_t}, p4est_connect_type_t, Ptr{sc_array_t}, Ptr{sc_array_t}), p4est, _in, out, balance, borders, inseeds)
end

function p4est_tree_uniqify_overlap(out)
    ccall((:p4est_tree_uniqify_overlap, libp4est), Void, (Ptr{sc_array_t},), out)
end

function p4est_tree_remove_nonowned()
    ccall((:p4est_tree_remove_nonowned, libp4est), Int32, ())
end

function p4est_complete_region(p4est, q1, include_q1::Int32, q2, include_q2::Int32, tree, which_tree::p4est_topidx_t, init_fn::p4est_init_t)
    ccall((:p4est_complete_region, libp4est), Void, (Ptr{p4est_t}, Ptr{p4est_quadrant_t}, Int32, Ptr{p4est_quadrant_t}, Int32, Ptr{p4est_tree_t}, p4est_topidx_t, p4est_init_t), p4est, q1, include_q1, q2, include_q2, tree, which_tree, init_fn)
end

function p4est_complete_subtree(p4est, which_tree::p4est_topidx_t, init_fn::p4est_init_t)
    ccall((:p4est_complete_subtree, libp4est), Void, (Ptr{p4est_t}, p4est_topidx_t, p4est_init_t), p4est, which_tree, init_fn)
end

function p4est_balance_subtree(p4est, btype::p4est_connect_type_t, which_tree::p4est_topidx_t, init_fn::p4est_init_t)
    ccall((:p4est_balance_subtree, libp4est), Void, (Ptr{p4est_t}, p4est_connect_type_t, p4est_topidx_t, p4est_init_t), p4est, btype, which_tree, init_fn)
end

function p4est_balance_border(p4est, btype::p4est_connect_type_t, which_tree::p4est_topidx_t, init_fn::p4est_init_t, replace_fn::p4est_replace_t, borders)
    ccall((:p4est_balance_border, libp4est), Void, (Ptr{p4est_t}, p4est_connect_type_t, p4est_topidx_t, p4est_init_t, p4est_replace_t, Ptr{sc_array_t}), p4est, btype, which_tree, init_fn, replace_fn, borders)
end

function p4est_linearize_tree()
    ccall((:p4est_linearize_tree, libp4est), Int32, ())
end

function p4est_partition_correction(partition, num_procs::Int32, rank::Int32, min_quadrant_id::p4est_gloidx_t, max_quadrant_id::p4est_gloidx_t)
    ccall((:p4est_partition_correction, libp4est), p4est_locidx_t, (Ptr{p4est_gloidx_t}, Int32, Int32, p4est_gloidx_t, p4est_gloidx_t), partition, num_procs, rank, min_quadrant_id, max_quadrant_id)
end

function p4est_next_nonempty_process(rank::Int32, num_procs::Int32, num_quadrants_in_proc)
    ccall((:p4est_next_nonempty_process, libp4est), Int32, (Int32, Int32, Ptr{p4est_locidx_t}), rank, num_procs, num_quadrants_in_proc)
end

function p4est_partition_given(p4est, num_quadrants_in_proc)
    ccall((:p4est_partition_given, libp4est), p4est_gloidx_t, (Ptr{p4est_t}, Ptr{p4est_locidx_t}), p4est, num_quadrants_in_proc)
end
# Julia wrapper for header: /home/iagbole/Codeentwicklung/p4est/local/include/p8est_nodes.h
# Automatically generated using Clang.jl wrap_c, version 0.0.0


function p8est_nodes_new(p8est, ghost)
    ccall((:p8est_nodes_new, libp4est), Ptr{p8est_nodes_t}, (Ptr{p8est_t}, Ptr{p8est_ghost_t}), p8est, ghost)
end

function p8est_nodes_destroy(nodes)
    ccall((:p8est_nodes_destroy, libp4est), Void, (Ptr{p8est_nodes_t},), nodes)
end

function p8est_nodes_is_valid(p8est, nodes)
    ccall((:p8est_nodes_is_valid, libp4est), Int32, (Ptr{p8est_t}, Ptr{p8est_nodes_t}), p8est, nodes)
end
# Julia wrapper for header: /home/iagbole/Codeentwicklung/p4est/local/include/p8est_bits.h
# Automatically generated using Clang.jl wrap_c, version 0.0.0


function p8est_quadrant_print(log_priority::Int32, q)
    ccall((:p8est_quadrant_print, libp4est), Void, (Int32, Ptr{p8est_quadrant_t}), log_priority, q)
end

function p8est_quadrant_is_equal(q1, q2)
    ccall((:p8est_quadrant_is_equal, libp4est), Int32, (Ptr{p8est_quadrant_t}, Ptr{p8est_quadrant_t}), q1, q2)
end

function p8est_quadrant_overlaps(q1, q2)
    ccall((:p8est_quadrant_overlaps, libp4est), Int32, (Ptr{p8est_quadrant_t}, Ptr{p8est_quadrant_t}), q1, q2)
end

function p8est_quadrant_is_equal_piggy(q1, q2)
    ccall((:p8est_quadrant_is_equal_piggy, libp4est), Int32, (Ptr{p8est_quadrant_t}, Ptr{p8est_quadrant_t}), q1, q2)
end

function p8est_quadrant_compare(v1, v2)
    ccall((:p8est_quadrant_compare, libp4est), Int32, (Ptr{Void}, Ptr{Void}), v1, v2)
end

function p8est_quadrant_disjoint(v1, v2)
    ccall((:p8est_quadrant_disjoint, libp4est), Int32, (Ptr{Void}, Ptr{Void}), v1, v2)
end

function p8est_quadrant_compare_piggy(v1, v2)
    ccall((:p8est_quadrant_compare_piggy, libp4est), Int32, (Ptr{Void}, Ptr{Void}), v1, v2)
end

function p8est_quadrant_compare_local_num(v1, v2)
    ccall((:p8est_quadrant_compare_local_num, libp4est), Int32, (Ptr{Void}, Ptr{Void}), v1, v2)
end

function p8est_quadrant_equal_fn(v1, v2, u)
    ccall((:p8est_quadrant_equal_fn, libp4est), Int32, (Ptr{Void}, Ptr{Void}, Ptr{Void}), v1, v2, u)
end

function p8est_quadrant_hash_fn(v, u)
    ccall((:p8est_quadrant_hash_fn, libp4est), UInt32, (Ptr{Void}, Ptr{Void}), v, u)
end

function p8est_node_equal_piggy_fn(v1, v2, u)
    ccall((:p8est_node_equal_piggy_fn, libp4est), Int32, (Ptr{Void}, Ptr{Void}, Ptr{Void}), v1, v2, u)
end

function p8est_node_hash_piggy_fn(v, u)
    ccall((:p8est_node_hash_piggy_fn, libp4est), UInt32, (Ptr{Void}, Ptr{Void}), v, u)
end

function p8est_node_clamp_inside(n, r)
    ccall((:p8est_node_clamp_inside, libp4est), Void, (Ptr{p8est_quadrant_t}, Ptr{p8est_quadrant_t}), n, r)
end

function p8est_node_unclamp(n)
    ccall((:p8est_node_unclamp, libp4est), Void, (Ptr{p8est_quadrant_t},), n)
end

function p8est_node_to_quadrant(n, level::Int32, q)
    ccall((:p8est_node_to_quadrant, libp4est), Void, (Ptr{p8est_quadrant_t}, Int32, Ptr{p8est_quadrant_t}), n, level, q)
end

function p8est_quadrant_contains_node(q, n)
    ccall((:p8est_quadrant_contains_node, libp4est), Int32, (Ptr{p8est_quadrant_t}, Ptr{p8est_quadrant_t}), q, n)
end

function p8est_quadrant_ancestor_id(q, level::Int32)
    ccall((:p8est_quadrant_ancestor_id, libp4est), Int32, (Ptr{p8est_quadrant_t}, Int32), q, level)
end

function p8est_quadrant_child_id(q)
    ccall((:p8est_quadrant_child_id, libp4est), Int32, (Ptr{p8est_quadrant_t},), q)
end

function p8est_quadrant_is_inside_root(q)
    ccall((:p8est_quadrant_is_inside_root, libp4est), Int32, (Ptr{p8est_quadrant_t},), q)
end

function p8est_quadrant_is_inside_3x3(q)
    ccall((:p8est_quadrant_is_inside_3x3, libp4est), Int32, (Ptr{p8est_quadrant_t},), q)
end

function p8est_quadrant_is_outside_face(q)
    ccall((:p8est_quadrant_is_outside_face, libp4est), Int32, (Ptr{p8est_quadrant_t},), q)
end

function p8est_quadrant_is_outside_edge(q)
    ccall((:p8est_quadrant_is_outside_edge, libp4est), Int32, (Ptr{p8est_quadrant_t},), q)
end

function p8est_quadrant_is_outside_edge_extra(q, edge)
    ccall((:p8est_quadrant_is_outside_edge_extra, libp4est), Int32, (Ptr{p8est_quadrant_t}, Ptr{Int32}), q, edge)
end

function p8est_quadrant_is_outside_corner(q)
    ccall((:p8est_quadrant_is_outside_corner, libp4est), Int32, (Ptr{p8est_quadrant_t},), q)
end

function p8est_quadrant_is_node(q, inside::Int32)
    ccall((:p8est_quadrant_is_node, libp4est), Int32, (Ptr{p8est_quadrant_t}, Int32), q, inside)
end

function p8est_quadrant_is_valid(q)
    ccall((:p8est_quadrant_is_valid, libp4est), Int32, (Ptr{p8est_quadrant_t},), q)
end

function p8est_quadrant_is_extended(q)
    ccall((:p8est_quadrant_is_extended, libp4est), Int32, (Ptr{p8est_quadrant_t},), q)
end

function p8est_quadrant_is_sibling(q1, q2)
    ccall((:p8est_quadrant_is_sibling, libp4est), Int32, (Ptr{p8est_quadrant_t}, Ptr{p8est_quadrant_t}), q1, q2)
end

function p8est_quadrant_child(q, r, child_id::Int32)
    ccall((:p8est_quadrant_child, libp4est), Void, (Ptr{p8est_quadrant_t}, Ptr{p8est_quadrant_t}, Int32), q, r, child_id)
end

function p8est_quadrant_is_sibling_D(q1, q2)
    ccall((:p8est_quadrant_is_sibling_D, libp4est), Int32, (Ptr{p8est_quadrant_t}, Ptr{p8est_quadrant_t}), q1, q2)
end

function p8est_quadrant_is_family(q0, q1, q2, q3, q4, q5, q6, q7)
    ccall((:p8est_quadrant_is_family, libp4est), Int32, (Ptr{p8est_quadrant_t}, Ptr{p8est_quadrant_t}, Ptr{p8est_quadrant_t}, Ptr{p8est_quadrant_t}, Ptr{p8est_quadrant_t}, Ptr{p8est_quadrant_t}, Ptr{p8est_quadrant_t}, Ptr{p8est_quadrant_t}), q0, q1, q2, q3, q4, q5, q6, q7)
end

function p8est_quadrant_is_familyv(q)
    ccall((:p8est_quadrant_is_familyv, libp4est), Int32, (Ptr{p8est_quadrant_t},), q)
end

function p8est_quadrant_is_familypv(q)
    ccall((:p8est_quadrant_is_familypv, libp4est), Int32, (Ptr{Ptr{p8est_quadrant_t}},), q)
end

function p8est_quadrant_is_parent(q, r)
    ccall((:p8est_quadrant_is_parent, libp4est), Int32, (Ptr{p8est_quadrant_t}, Ptr{p8est_quadrant_t}), q, r)
end

function p8est_quadrant_is_parent_D(q, r)
    ccall((:p8est_quadrant_is_parent_D, libp4est), Int32, (Ptr{p8est_quadrant_t}, Ptr{p8est_quadrant_t}), q, r)
end

function p8est_quadrant_is_ancestor(q, r)
    ccall((:p8est_quadrant_is_ancestor, libp4est), Int32, (Ptr{p8est_quadrant_t}, Ptr{p8est_quadrant_t}), q, r)
end

function p8est_quadrant_is_ancestor_D(q, r)
    ccall((:p8est_quadrant_is_ancestor_D, libp4est), Int32, (Ptr{p8est_quadrant_t}, Ptr{p8est_quadrant_t}), q, r)
end

function p8est_quadrant_is_next(q, r)
    ccall((:p8est_quadrant_is_next, libp4est), Int32, (Ptr{p8est_quadrant_t}, Ptr{p8est_quadrant_t}), q, r)
end

function p8est_quadrant_is_next_D(q, r)
    ccall((:p8est_quadrant_is_next_D, libp4est), Int32, (Ptr{p8est_quadrant_t}, Ptr{p8est_quadrant_t}), q, r)
end

function p8est_quadrant_overlaps_tree(tree, q)
    ccall((:p8est_quadrant_overlaps_tree, libp4est), Int32, (Ptr{p8est_tree_t}, Ptr{p8est_quadrant_t}), tree, q)
end

function p8est_quadrant_is_inside_tree(tree, q)
    ccall((:p8est_quadrant_is_inside_tree, libp4est), Int32, (Ptr{p8est_tree_t}, Ptr{p8est_quadrant_t}), tree, q)
end

function p8est_quadrant_ancestor(q, level::Int32, r)
    ccall((:p8est_quadrant_ancestor, libp4est), Void, (Ptr{p8est_quadrant_t}, Int32, Ptr{p8est_quadrant_t}), q, level, r)
end

function p8est_quadrant_parent(q, r)
    ccall((:p8est_quadrant_parent, libp4est), Void, (Ptr{p8est_quadrant_t}, Ptr{p8est_quadrant_t}), q, r)
end

function p8est_quadrant_sibling(q, r, sibling_id::Int32)
    ccall((:p8est_quadrant_sibling, libp4est), Void, (Ptr{p8est_quadrant_t}, Ptr{p8est_quadrant_t}, Int32), q, r, sibling_id)
end

function p8est_quadrant_face_neighbor(q, face::Int32, r)
    ccall((:p8est_quadrant_face_neighbor, libp4est), Void, (Ptr{p8est_quadrant_t}, Int32, Ptr{p8est_quadrant_t}), q, face, r)
end

function p8est_quadrant_face_neighbor_extra(q, t::p4est_topidx_t, face::Int32, r, nface, conn)
    ccall((:p8est_quadrant_face_neighbor_extra, libp4est), p4est_locidx_t, (Ptr{p8est_quadrant_t}, p4est_topidx_t, Int32, Ptr{p8est_quadrant_t}, Ptr{Int32}, Ptr{p8est_connectivity_t}), q, t, face, r, nface, conn)
end

function p8est_quadrant_half_face_neighbors(q, face::Int32, n, nur)
    ccall((:p8est_quadrant_half_face_neighbors, libp4est), Void, (Ptr{p8est_quadrant_t}, Int32, Ptr{p8est_quadrant_t}, Ptr{p8est_quadrant_t}), q, face, n, nur)
end

function p8est_quadrant_all_face_neighbors(q, face::Int32, n)
    ccall((:p8est_quadrant_all_face_neighbors, libp4est), Void, (Ptr{p8est_quadrant_t}, Int32, Ptr{p8est_quadrant_t}), q, face, n)
end

function p8est_quadrant_edge_neighbor(q, edge::Int32, r)
    ccall((:p8est_quadrant_edge_neighbor, libp4est), Void, (Ptr{p8est_quadrant_t}, Int32, Ptr{p8est_quadrant_t}), q, edge, r)
end

function p8est_quadrant_edge_neighbor_extra(q, t::p4est_locidx_t, edge::Int32, quads, treeids, nedges, conn)
    ccall((:p8est_quadrant_edge_neighbor_extra, libp4est), Void, (Ptr{p8est_quadrant_t}, p4est_locidx_t, Int32, Ptr{sc_array_t}, Ptr{sc_array_t}, Ptr{sc_array_t}, Ptr{p8est_connectivity_t}), q, t, edge, quads, treeids, nedges, conn)
end

function p8est_quadrant_corner_neighbor(q, corner::Int32, r)
    ccall((:p8est_quadrant_corner_neighbor, libp4est), Void, (Ptr{p8est_quadrant_t}, Int32, Ptr{p8est_quadrant_t}), q, corner, r)
end

function p8est_quadrant_corner_neighbor_extra(q, t::p4est_locidx_t, corner::Int32, quads, treeids, ncorners, conn)
    ccall((:p8est_quadrant_corner_neighbor_extra, libp4est), Void, (Ptr{p8est_quadrant_t}, p4est_locidx_t, Int32, Ptr{sc_array_t}, Ptr{sc_array_t}, Ptr{sc_array_t}, Ptr{p8est_connectivity_t}), q, t, corner, quads, treeids, ncorners, conn)
end

function p8est_quadrant_half_corner_neighbor(q, corner::Int32, r)
    ccall((:p8est_quadrant_half_corner_neighbor, libp4est), Void, (Ptr{p8est_quadrant_t}, Int32, Ptr{p8est_quadrant_t}), q, corner, r)
end

function p8est_quadrant_corner_node(q, corner::Int32, r)
    ccall((:p8est_quadrant_corner_node, libp4est), Void, (Ptr{p8est_quadrant_t}, Int32, Ptr{p8est_quadrant_t}), q, corner, r)
end

function p8est_quadrant_children(q, c0, c1, c2, c3, c4, c5, c6, c7)
    ccall((:p8est_quadrant_children, libp4est), Void, (Ptr{p8est_quadrant_t}, Ptr{p8est_quadrant_t}, Ptr{p8est_quadrant_t}, Ptr{p8est_quadrant_t}, Ptr{p8est_quadrant_t}, Ptr{p8est_quadrant_t}, Ptr{p8est_quadrant_t}, Ptr{p8est_quadrant_t}, Ptr{p8est_quadrant_t}), q, c0, c1, c2, c3, c4, c5, c6, c7)
end

function p8est_quadrant_childrenv(q, c)
    ccall((:p8est_quadrant_childrenv, libp4est), Void, (Ptr{p8est_quadrant_t}, Ptr{p8est_quadrant_t}), q, c)
end

function p8est_quadrant_childrenpv(q, c)
    ccall((:p8est_quadrant_childrenpv, libp4est), Void, (Ptr{p8est_quadrant_t}, Ptr{Ptr{p8est_quadrant_t}}), q, c)
end

function p8est_quadrant_first_descendant(q, fd, level::Int32)
    ccall((:p8est_quadrant_first_descendant, libp4est), Void, (Ptr{p8est_quadrant_t}, Ptr{p8est_quadrant_t}, Int32), q, fd, level)
end

function p8est_quadrant_last_descendant(q, ld, level::Int32)
    ccall((:p8est_quadrant_last_descendant, libp4est), Void, (Ptr{p8est_quadrant_t}, Ptr{p8est_quadrant_t}, Int32), q, ld, level)
end

function p8est_quadrant_corner_descendant(q, r, c::Int32, level::Int32)
    ccall((:p8est_quadrant_corner_descendant, libp4est), Void, (Ptr{p8est_quadrant_t}, Ptr{p8est_quadrant_t}, Int32, Int32), q, r, c, level)
end

function p8est_nearest_common_ancestor(q1, q2, r)
    ccall((:p8est_nearest_common_ancestor, libp4est), Void, (Ptr{p8est_quadrant_t}, Ptr{p8est_quadrant_t}, Ptr{p8est_quadrant_t}), q1, q2, r)
end

function p8est_nearest_common_ancestor_D(q1, q2, r)
    ccall((:p8est_nearest_common_ancestor_D, libp4est), Void, (Ptr{p8est_quadrant_t}, Ptr{p8est_quadrant_t}, Ptr{p8est_quadrant_t}), q1, q2, r)
end

function p8est_quadrant_transform_face(q, r, ftransform)
    ccall((:p8est_quadrant_transform_face, libp4est), Void, (Ptr{p8est_quadrant_t}, Ptr{p8est_quadrant_t}, Ptr{Int32}), q, r, ftransform)
end

function p8est_quadrant_touches_edge(q, edge::Int32, inside::Int32)
    ccall((:p8est_quadrant_touches_edge, libp4est), Int32, (Ptr{p8est_quadrant_t}, Int32, Int32), q, edge, inside)
end

function p8est_quadrant_transform_edge(q, r, ei, et, inside::Int32)
    ccall((:p8est_quadrant_transform_edge, libp4est), Void, (Ptr{p8est_quadrant_t}, Ptr{p8est_quadrant_t}, Ptr{p8est_edge_info_t}, Ptr{p8est_edge_transform_t}, Int32), q, r, ei, et, inside)
end

function p8est_quadrant_shift_edge(q, r, rup, rdown, edge::Int32)
    ccall((:p8est_quadrant_shift_edge, libp4est), Void, (Ptr{p8est_quadrant_t}, Ptr{p8est_quadrant_t}, Ptr{p8est_quadrant_t}, Ptr{p8est_quadrant_t}, Int32), q, r, rup, rdown, edge)
end

function p8est_quadrant_touches_corner(q, corner::Int32, inside::Int32)
    ccall((:p8est_quadrant_touches_corner, libp4est), Int32, (Ptr{p8est_quadrant_t}, Int32, Int32), q, corner, inside)
end

function p8est_quadrant_transform_corner(r, corner::Int32, inside::Int32)
    ccall((:p8est_quadrant_transform_corner, libp4est), Void, (Ptr{p8est_quadrant_t}, Int32, Int32), r, corner, inside)
end

function p8est_quadrant_shift_corner(q, r, corner::Int32)
    ccall((:p8est_quadrant_shift_corner, libp4est), Void, (Ptr{p8est_quadrant_t}, Ptr{p8est_quadrant_t}, Int32), q, r, corner)
end

function p8est_quadrant_linear_id(quadrant, level::Int32)
    ccall((:p8est_quadrant_linear_id, libp4est), UInt64, (Ptr{p8est_quadrant_t}, Int32), quadrant, level)
end

function p8est_quadrant_set_morton(quadrant, level::Int32, id::UInt64)
    ccall((:p8est_quadrant_set_morton, libp4est), Void, (Ptr{p8est_quadrant_t}, Int32, UInt64), quadrant, level, id)
end
# Julia wrapper for header: /home/iagbole/Codeentwicklung/p4est/local/include/p8est_communication.h
# Automatically generated using Clang.jl wrap_c, version 0.0.0


function p8est_comm_parallel_env_assign(p8est, mpicomm::MPI_Comm)
    ccall((:p8est_comm_parallel_env_assign, libp4est), Void, (Ptr{p8est_t}, MPI_Comm), p8est, mpicomm)
end

function p8est_comm_parallel_env_duplicate(p8est)
    ccall((:p8est_comm_parallel_env_duplicate, libp4est), Void, (Ptr{p8est_t},), p8est)
end

function p8est_comm_parallel_env_release(p8est)
    ccall((:p8est_comm_parallel_env_release, libp4est), Void, (Ptr{p8est_t},), p8est)
end

function p8est_comm_parallel_env_replace(p8est, mpicomm::MPI_Comm)
    ccall((:p8est_comm_parallel_env_replace, libp4est), Void, (Ptr{p8est_t}, MPI_Comm), p8est, mpicomm)
end

function p8est_comm_parallel_env_get_info(p8est)
    ccall((:p8est_comm_parallel_env_get_info, libp4est), Void, (Ptr{p8est_t},), p8est)
end

function p8est_comm_parallel_env_is_null(p8est)
    ccall((:p8est_comm_parallel_env_is_null, libp4est), Int32, (Ptr{p8est_t},), p8est)
end

function p8est_comm_parallel_env_reduce(p8est_supercomm)
    ccall((:p8est_comm_parallel_env_reduce, libp4est), Int32, (Ptr{Ptr{p8est_t}},), p8est_supercomm)
end

function p8est_comm_parallel_env_reduce_ext(p8est_supercomm, group_add::MPI_Group, add_to_beginning::Int32, ranks_subcomm)
    ccall((:p8est_comm_parallel_env_reduce_ext, libp4est), Int32, (Ptr{Ptr{p8est_t}}, MPI_Group, Int32, Ptr{Ptr{Int32}}), p8est_supercomm, group_add, add_to_beginning, ranks_subcomm)
end

function p8est_comm_count_quadrants(p8est)
    ccall((:p8est_comm_count_quadrants, libp4est), Void, (Ptr{p8est_t},), p8est)
end

function p8est_comm_global_partition(p8est, first_quad)
    ccall((:p8est_comm_global_partition, libp4est), Void, (Ptr{p8est_t}, Ptr{p8est_quadrant_t}), p8est, first_quad)
end

function p8est_comm_count_pertree(p8est, pertree)
    ccall((:p8est_comm_count_pertree, libp4est), Void, (Ptr{p8est_t}, Ptr{p4est_gloidx_t}), p8est, pertree)
end

function p8est_comm_is_empty(p8est, p::Int32)
    ccall((:p8est_comm_is_empty, libp4est), Int32, (Ptr{p8est_t}, Int32), p8est, p)
end

function p8est_comm_is_contained(p8est, which_tree::p4est_locidx_t, q, rank::Int32)
    ccall((:p8est_comm_is_contained, libp4est), Int32, (Ptr{p8est_t}, p4est_locidx_t, Ptr{p8est_quadrant_t}, Int32), p8est, which_tree, q, rank)
end

function p8est_comm_is_owner(p8est, which_tree::p4est_locidx_t, q, rank::Int32)
    ccall((:p8est_comm_is_owner, libp4est), Int32, (Ptr{p8est_t}, p4est_locidx_t, Ptr{p8est_quadrant_t}, Int32), p8est, which_tree, q, rank)
end

function p8est_comm_find_owner(p8est, which_tree::p4est_locidx_t, q, guess::Int32)
    ccall((:p8est_comm_find_owner, libp4est), Int32, (Ptr{p8est_t}, p4est_locidx_t, Ptr{p8est_quadrant_t}, Int32), p8est, which_tree, q, guess)
end

function p8est_comm_tree_info(p8est, which_tree::p4est_locidx_t, full_tree, tree_contact, firstq, nextq)
    ccall((:p8est_comm_tree_info, libp4est), Void, (Ptr{p8est_t}, p4est_locidx_t, Ptr{Int32}, Ptr{Int32}, Ptr{Ptr{p8est_quadrant_t}}, Ptr{Ptr{p8est_quadrant_t}}), p8est, which_tree, full_tree, tree_contact, firstq, nextq)
end

function p8est_comm_neighborhood_owned(p8est, which_tree::p4est_locidx_t, full_tree, tree_contact, q)
    ccall((:p8est_comm_neighborhood_owned, libp4est), Int32, (Ptr{p8est_t}, p4est_locidx_t, Ptr{Int32}, Ptr{Int32}, Ptr{p8est_quadrant_t}), p8est, which_tree, full_tree, tree_contact, q)
end

function p8est_comm_sync_flag(p8est, flag::Int32, operation::MPI_Op)
    ccall((:p8est_comm_sync_flag, libp4est), Int32, (Ptr{p8est_t}, Int32, MPI_Op), p8est, flag, operation)
end

function p8est_comm_checksum(p8est, local_crc::UInt32, local_bytes::UInt)
    ccall((:p8est_comm_checksum, libp4est), UInt32, (Ptr{p8est_t}, UInt32, UInt), p8est, local_crc, local_bytes)
end

function p8est_transfer_fixed(dest_gfq, src_gfq, mpicomm::MPI_Comm, tag::Int32, dest_data, src_data, data_size::UInt)
    ccall((:p8est_transfer_fixed, libp4est), Void, (Ptr{p4est_gloidx_t}, Ptr{p4est_gloidx_t}, MPI_Comm, Int32, Ptr{Void}, Ptr{Void}, UInt), dest_gfq, src_gfq, mpicomm, tag, dest_data, src_data, data_size)
end

function p8est_transfer_fixed_begin(dest_gfq, src_gfq, mpicomm::MPI_Comm, tag::Int32, dest_data, src_data, data_size::UInt)
    ccall((:p8est_transfer_fixed_begin, libp4est), Ptr{p8est_transfer_context_t}, (Ptr{p4est_gloidx_t}, Ptr{p4est_gloidx_t}, MPI_Comm, Int32, Ptr{Void}, Ptr{Void}, UInt), dest_gfq, src_gfq, mpicomm, tag, dest_data, src_data, data_size)
end

function p8est_transfer_fixed_end(tc)
    ccall((:p8est_transfer_fixed_end, libp4est), Void, (Ptr{p8est_transfer_context_t},), tc)
end

function p8est_transfer_custom(dest_gfq, src_gfq, mpicomm::MPI_Comm, tag::Int32, dest_data, dest_sizes, src_data, src_sizes)
    ccall((:p8est_transfer_custom, libp4est), Void, (Ptr{p4est_gloidx_t}, Ptr{p4est_gloidx_t}, MPI_Comm, Int32, Ptr{Void}, Ptr{Int32}, Ptr{Void}, Ptr{Int32}), dest_gfq, src_gfq, mpicomm, tag, dest_data, dest_sizes, src_data, src_sizes)
end

function p8est_transfer_custom_begin(dest_gfq, src_gfq, mpicomm::MPI_Comm, tag::Int32, dest_data, dest_sizes, src_data, src_sizes)
    ccall((:p8est_transfer_custom_begin, libp4est), Ptr{p8est_transfer_context_t}, (Ptr{p4est_gloidx_t}, Ptr{p4est_gloidx_t}, MPI_Comm, Int32, Ptr{Void}, Ptr{Int32}, Ptr{Void}, Ptr{Int32}), dest_gfq, src_gfq, mpicomm, tag, dest_data, dest_sizes, src_data, src_sizes)
end

function p8est_transfer_custom_end(tc)
    ccall((:p8est_transfer_custom_end, libp4est), Void, (Ptr{p8est_transfer_context_t},), tc)
end
# Julia wrapper for header: /home/iagbole/Codeentwicklung/p4est/local/include/p4est_plex.h
# Automatically generated using Clang.jl wrap_c, version 0.0.0


function p4est_get_plex_data(p4est, ctype::p4est_connect_type_t, overlap::Int32, first_local_quad, out_points_per_dim, out_cone_sizes, out_cones, out_cone_orientations, out_vertex_coords, out_children, out_parents, out_childids, out_leaves, out_remotes)
    ccall((:p4est_get_plex_data, libp4est), Void, (Ptr{p4est_t}, p4est_connect_type_t, Int32, Ptr{p4est_locidx_t}, Ptr{sc_array_t}, Ptr{sc_array_t}, Ptr{sc_array_t}, Ptr{sc_array_t}, Ptr{sc_array_t}, Ptr{sc_array_t}, Ptr{sc_array_t}, Ptr{sc_array_t}, Ptr{sc_array_t}, Ptr{sc_array_t}), p4est, ctype, overlap, first_local_quad, out_points_per_dim, out_cone_sizes, out_cones, out_cone_orientations, out_vertex_coords, out_children, out_parents, out_childids, out_leaves, out_remotes)
end
# Julia wrapper for header: /home/iagbole/Codeentwicklung/p4est/local/include/p4est_nodes.h
# Automatically generated using Clang.jl wrap_c, version 0.0.0


function p4est_nodes_new(p4est, ghost)
    ccall((:p4est_nodes_new, libp4est), Ptr{p4est_nodes_t}, (Ptr{p4est_t}, Ptr{p4est_ghost_t}), p4est, ghost)
end

function p4est_nodes_destroy(nodes)
    ccall((:p4est_nodes_destroy, libp4est), Void, (Ptr{p4est_nodes_t},), nodes)
end

function p4est_nodes_is_valid(p4est, nodes)
    ccall((:p4est_nodes_is_valid, libp4est), Int32, (Ptr{p4est_t}, Ptr{p4est_nodes_t}), p4est, nodes)
end
# Julia wrapper for header: /home/iagbole/Codeentwicklung/p4est/local/include/p4est_vtk.h
# Automatically generated using Clang.jl wrap_c, version 0.0.0


function p4est_vtk_write_file(p4est, geom, filename)
    ccall((:p4est_vtk_write_file, libp4est), Void, (Ptr{p4est_t}, Ptr{p4est_geometry_t}, Cstring), p4est, geom, filename)
end

function p4est_vtk_context_new(p4est, filename)
    ccall((:p4est_vtk_context_new, libp4est), Ptr{p4est_vtk_context_t}, (Ptr{p4est_t}, Cstring), p4est, filename)
end

function p4est_vtk_context_set_geom(cont, geom)
    ccall((:p4est_vtk_context_set_geom, libp4est), Void, (Ptr{p4est_vtk_context_t}, Ptr{p4est_geometry_t}), cont, geom)
end

function p4est_vtk_context_set_scale(cont, scale::Float64)
    ccall((:p4est_vtk_context_set_scale, libp4est), Void, (Ptr{p4est_vtk_context_t}, Float64), cont, scale)
end

function p4est_vtk_context_set_continuous(cont, continuous::Int32)
    ccall((:p4est_vtk_context_set_continuous, libp4est), Void, (Ptr{p4est_vtk_context_t}, Int32), cont, continuous)
end

function p4est_vtk_context_destroy(context)
    ccall((:p4est_vtk_context_destroy, libp4est), Void, (Ptr{p4est_vtk_context_t},), context)
end

function p4est_vtk_write_header(cont)
    ccall((:p4est_vtk_write_header, libp4est), Ptr{p4est_vtk_context_t}, (Ptr{p4est_vtk_context_t},), cont)
end

function p4est_vtk_write_cell_data(cont, write_tree::Int32, write_level::Int32, write_rank::Int32, wrap_rank::Int32, num_cell_scalars::Int32, num_cell_vectors::Int32, filenames, values)
    ccall((:p4est_vtk_write_cell_data, libp4est), Ptr{p4est_vtk_context_t}, (Ptr{p4est_vtk_context_t}, Int32, Int32, Int32, Int32, Int32, Int32, Ptr{Cstring}, Ptr{Ptr{sc_array_t}}), cont, write_tree, write_level, write_rank, wrap_rank, num_cell_scalars, num_cell_vectors, filenames, values)
end

function p4est_vtk_write_footer(cont)
    ccall((:p4est_vtk_write_footer, libp4est), Int32, (Ptr{p4est_vtk_context_t},), cont)
end
# Julia wrapper for header: /home/iagbole/Codeentwicklung/p4est/local/include/p6est_communication.h
# Automatically generated using Clang.jl wrap_c, version 0.0.0


function p6est_comm_parallel_env_assign(p6est, mpicomm::MPI_Comm)
    ccall((:p6est_comm_parallel_env_assign, libp4est), Void, (Ptr{p6est_t}, MPI_Comm), p6est, mpicomm)
end

function p6est_comm_parallel_env_duplicate(p6est)
    ccall((:p6est_comm_parallel_env_duplicate, libp4est), Void, (Ptr{p6est_t},), p6est)
end

function p6est_comm_parallel_env_release(p6est)
    ccall((:p6est_comm_parallel_env_release, libp4est), Void, (Ptr{p6est_t},), p6est)
end

function p6est_comm_parallel_env_replace(p6est, mpicomm::MPI_Comm)
    ccall((:p6est_comm_parallel_env_replace, libp4est), Void, (Ptr{p6est_t}, MPI_Comm), p6est, mpicomm)
end

function p6est_comm_parallel_env_get_info(p6est)
    ccall((:p6est_comm_parallel_env_get_info, libp4est), Void, (Ptr{p6est_t},), p6est)
end

function p6est_comm_parallel_env_is_null(p6est)
    ccall((:p6est_comm_parallel_env_is_null, libp4est), Int32, (Ptr{p6est_t},), p6est)
end

function p6est_comm_parallel_env_reduce(p6est_supercomm)
    ccall((:p6est_comm_parallel_env_reduce, libp4est), Int32, (Ptr{Ptr{p6est_t}},), p6est_supercomm)
end

function p6est_comm_parallel_env_reduce_ext(p6est_supercomm, group_add::MPI_Group, add_to_beginning::Int32, ranks_subcomm)
    ccall((:p6est_comm_parallel_env_reduce_ext, libp4est), Int32, (Ptr{Ptr{p6est_t}}, MPI_Group, Int32, Ptr{Ptr{Int32}}), p6est_supercomm, group_add, add_to_beginning, ranks_subcomm)
end
# Julia wrapper for header: /home/iagbole/Codeentwicklung/p4est/local/include/p8est_wrap.h
# Automatically generated using Clang.jl wrap_c, version 0.0.0


function p8est_wrap_new_conn(mpicomm::MPI_Comm, conn, initial_level::Int32)
    ccall((:p8est_wrap_new_conn, libp4est), Ptr{p8est_wrap_t}, (MPI_Comm, Ptr{p8est_connectivity_t}, Int32), mpicomm, conn, initial_level)
end

function p8est_wrap_new_p8est(p8est, hollow::Int32, btype::p8est_connect_type_t, replace_fn::p8est_replace_t, user_pointer)
    ccall((:p8est_wrap_new_p8est, libp4est), Ptr{p8est_wrap_t}, (Ptr{p8est_t}, Int32, p8est_connect_type_t, p8est_replace_t, Ptr{Void}), p8est, hollow, btype, replace_fn, user_pointer)
end

function p8est_wrap_new_ext(mpicomm::MPI_Comm, conn, initial_level::Int32, hollow::Int32, btype::p8est_connect_type_t, replace_fn::p8est_replace_t, user_pointer)
    ccall((:p8est_wrap_new_ext, libp4est), Ptr{p8est_wrap_t}, (MPI_Comm, Ptr{p8est_connectivity_t}, Int32, Int32, p8est_connect_type_t, p8est_replace_t, Ptr{Void}), mpicomm, conn, initial_level, hollow, btype, replace_fn, user_pointer)
end

function p8est_wrap_new_copy(source, data_size::UInt, replace_fn::p8est_replace_t, user_pointer)
    ccall((:p8est_wrap_new_copy, libp4est), Ptr{p8est_wrap_t}, (Ptr{p8est_wrap_t}, UInt, p8est_replace_t, Ptr{Void}), source, data_size, replace_fn, user_pointer)
end

function p8est_wrap_new_unitcube(mpicomm::MPI_Comm, initial_level::Int32)
    ccall((:p8est_wrap_new_unitcube, libp4est), Ptr{p8est_wrap_t}, (MPI_Comm, Int32), mpicomm, initial_level)
end

function p8est_wrap_new_rotwrap(mpicomm::MPI_Comm, initial_level::Int32)
    ccall((:p8est_wrap_new_rotwrap, libp4est), Ptr{p8est_wrap_t}, (MPI_Comm, Int32), mpicomm, initial_level)
end

function p8est_wrap_new_brick(mpicomm::MPI_Comm, bx::Int32, by::Int32, bz::Int32, px::Int32, py::Int32, pz::Int32, initial_level::Int32)
    ccall((:p8est_wrap_new_brick, libp4est), Ptr{p8est_wrap_t}, (MPI_Comm, Int32, Int32, Int32, Int32, Int32, Int32, Int32), mpicomm, bx, by, bz, px, py, pz, initial_level)
end

function p8est_wrap_new_world(initial_level::Int32)
    ccall((:p8est_wrap_new_world, libp4est), Ptr{p8est_wrap_t}, (Int32,), initial_level)
end

function p8est_wrap_destroy(pp)
    ccall((:p8est_wrap_destroy, libp4est), Void, (Ptr{p8est_wrap_t},), pp)
end

function p8est_wrap_set_hollow(pp, hollow::Int32)
    ccall((:p8est_wrap_set_hollow, libp4est), Void, (Ptr{p8est_wrap_t}, Int32), pp, hollow)
end

function p8est_wrap_set_coarsen_delay(pp, coarsen_delay::Int32, coarsen_affect::Int32)
    ccall((:p8est_wrap_set_coarsen_delay, libp4est), Void, (Ptr{p8est_wrap_t}, Int32, Int32), pp, coarsen_delay, coarsen_affect)
end

function p8est_wrap_get_ghost(pp)
    ccall((:p8est_wrap_get_ghost, libp4est), Ptr{p8est_ghost_t}, (Ptr{p8est_wrap_t},), pp)
end

function p8est_wrap_get_mesh(pp)
    ccall((:p8est_wrap_get_mesh, libp4est), Ptr{p8est_mesh_t}, (Ptr{p8est_wrap_t},), pp)
end

function p8est_wrap_mark_refine(pp, which_tree::p4est_topidx_t, which_quad::p4est_locidx_t)
    ccall((:p8est_wrap_mark_refine, libp4est), Void, (Ptr{p8est_wrap_t}, p4est_topidx_t, p4est_locidx_t), pp, which_tree, which_quad)
end

function p8est_wrap_mark_coarsen(pp, which_tree::p4est_topidx_t, which_quad::p4est_locidx_t)
    ccall((:p8est_wrap_mark_coarsen, libp4est), Void, (Ptr{p8est_wrap_t}, p4est_topidx_t, p4est_locidx_t), pp, which_tree, which_quad)
end

function p8est_wrap_adapt(pp)
    ccall((:p8est_wrap_adapt, libp4est), Int32, (Ptr{p8est_wrap_t},), pp)
end

function p8est_wrap_partition(pp, weight_exponent::Int32, unchanged_first, unchanged_length, unchanged_old_first)
    ccall((:p8est_wrap_partition, libp4est), Int32, (Ptr{p8est_wrap_t}, Int32, Ptr{p4est_locidx_t}, Ptr{p4est_locidx_t}, Ptr{p4est_locidx_t}), pp, weight_exponent, unchanged_first, unchanged_length, unchanged_old_first)
end

function p8est_wrap_complete(pp)
    ccall((:p8est_wrap_complete, libp4est), Void, (Ptr{p8est_wrap_t},), pp)
end

function p8est_wrap_leaf_first(pp, track_mirrors::Int32)
    ccall((:p8est_wrap_leaf_first, libp4est), Ptr{p8est_wrap_leaf_t}, (Ptr{p8est_wrap_t}, Int32), pp, track_mirrors)
end

function p8est_wrap_leaf_next(leaf)
    ccall((:p8est_wrap_leaf_next, libp4est), Ptr{p8est_wrap_leaf_t}, (Ptr{p8est_wrap_leaf_t},), leaf)
end
# Julia wrapper for header: /home/iagbole/Codeentwicklung/p4est/local/include/p6est_vtk.h
# Automatically generated using Clang.jl wrap_c, version 0.0.0


function p6est_vtk_write_file(p6est, filename)
    ccall((:p6est_vtk_write_file, libp4est), Void, (Ptr{p6est_t}, Cstring), p6est, filename)
end

function p6est_vtk_write_header(p6est, scale::Float64, write_tree::Int32, write_rank::Int32, wrap_rank::Int32, point_scalars, point_vectors, filename)
    ccall((:p6est_vtk_write_header, libp4est), Int32, (Ptr{p6est_t}, Float64, Int32, Int32, Int32, Cstring, Cstring, Cstring), p6est, scale, write_tree, write_rank, wrap_rank, point_scalars, point_vectors, filename)
end

function p6est_vtk_write_point_scalar(p6est, filename, scalar_name, values)
    ccall((:p6est_vtk_write_point_scalar, libp4est), Int32, (Ptr{p6est_t}, Cstring, Cstring, Ptr{Float64}), p6est, filename, scalar_name, values)
end

function p6est_vtk_write_point_vector(p6est, filename, vector_name, values)
    ccall((:p6est_vtk_write_point_vector, libp4est), Int32, (Ptr{p6est_t}, Cstring, Cstring, Ptr{Float64}), p6est, filename, vector_name, values)
end

function p6est_vtk_write_footer(p6est, filename)
    ccall((:p6est_vtk_write_footer, libp4est), Int32, (Ptr{p6est_t}, Cstring), p6est, filename)
end
# Julia wrapper for header: /home/iagbole/Codeentwicklung/p4est/local/include/p4est_communication.h
# Automatically generated using Clang.jl wrap_c, version 0.0.0


function p4est_comm_parallel_env_assign(p4est, mpicomm::MPI_Comm)
    ccall((:p4est_comm_parallel_env_assign, libp4est), Void, (Ptr{p4est_t}, MPI_Comm), p4est, mpicomm)
end

function p4est_comm_parallel_env_duplicate(p4est)
    ccall((:p4est_comm_parallel_env_duplicate, libp4est), Void, (Ptr{p4est_t},), p4est)
end

function p4est_comm_parallel_env_release(p4est)
    ccall((:p4est_comm_parallel_env_release, libp4est), Void, (Ptr{p4est_t},), p4est)
end

function p4est_comm_parallel_env_replace(p4est, mpicomm::MPI_Comm)
    ccall((:p4est_comm_parallel_env_replace, libp4est), Void, (Ptr{p4est_t}, MPI_Comm), p4est, mpicomm)
end

function p4est_comm_parallel_env_get_info(p4est)
    ccall((:p4est_comm_parallel_env_get_info, libp4est), Void, (Ptr{p4est_t},), p4est)
end

function p4est_comm_parallel_env_is_null(p4est)
    ccall((:p4est_comm_parallel_env_is_null, libp4est), Int32, (Ptr{p4est_t},), p4est)
end

function p4est_comm_parallel_env_reduce(p4est_supercomm)
    ccall((:p4est_comm_parallel_env_reduce, libp4est), Int32, (Ptr{Ptr{p4est_t}},), p4est_supercomm)
end

function p4est_comm_parallel_env_reduce_ext(p4est_supercomm, group_add::MPI_Group, add_to_beginning::Int32, ranks_subcomm)
    ccall((:p4est_comm_parallel_env_reduce_ext, libp4est), Int32, (Ptr{Ptr{p4est_t}}, MPI_Group, Int32, Ptr{Ptr{Int32}}), p4est_supercomm, group_add, add_to_beginning, ranks_subcomm)
end

function p4est_comm_count_quadrants(p4est)
    ccall((:p4est_comm_count_quadrants, libp4est), Void, (Ptr{p4est_t},), p4est)
end

function p4est_comm_global_partition(p4est, first_quad)
    ccall((:p4est_comm_global_partition, libp4est), Void, (Ptr{p4est_t}, Ptr{p4est_quadrant_t}), p4est, first_quad)
end

function p4est_comm_count_pertree(p4est, pertree)
    ccall((:p4est_comm_count_pertree, libp4est), Void, (Ptr{p4est_t}, Ptr{p4est_gloidx_t}), p4est, pertree)
end

function p4est_comm_is_empty(p4est, p::Int32)
    ccall((:p4est_comm_is_empty, libp4est), Int32, (Ptr{p4est_t}, Int32), p4est, p)
end

function p4est_comm_is_contained(p4est, which_tree::p4est_locidx_t, q, rank::Int32)
    ccall((:p4est_comm_is_contained, libp4est), Int32, (Ptr{p4est_t}, p4est_locidx_t, Ptr{p4est_quadrant_t}, Int32), p4est, which_tree, q, rank)
end

function p4est_comm_is_owner(p4est, which_tree::p4est_locidx_t, q, rank::Int32)
    ccall((:p4est_comm_is_owner, libp4est), Int32, (Ptr{p4est_t}, p4est_locidx_t, Ptr{p4est_quadrant_t}, Int32), p4est, which_tree, q, rank)
end

function p4est_comm_find_owner(p4est, which_tree::p4est_locidx_t, q, guess::Int32)
    ccall((:p4est_comm_find_owner, libp4est), Int32, (Ptr{p4est_t}, p4est_locidx_t, Ptr{p4est_quadrant_t}, Int32), p4est, which_tree, q, guess)
end

function p4est_comm_tree_info(p4est, which_tree::p4est_locidx_t, full_tree, tree_contact, firstq, nextq)
    ccall((:p4est_comm_tree_info, libp4est), Void, (Ptr{p4est_t}, p4est_locidx_t, Ptr{Int32}, Ptr{Int32}, Ptr{Ptr{p4est_quadrant_t}}, Ptr{Ptr{p4est_quadrant_t}}), p4est, which_tree, full_tree, tree_contact, firstq, nextq)
end

function p4est_comm_neighborhood_owned(p4est, which_tree::p4est_locidx_t, full_tree, tree_contact, q)
    ccall((:p4est_comm_neighborhood_owned, libp4est), Int32, (Ptr{p4est_t}, p4est_locidx_t, Ptr{Int32}, Ptr{Int32}, Ptr{p4est_quadrant_t}), p4est, which_tree, full_tree, tree_contact, q)
end

function p4est_comm_sync_flag(p4est, flag::Int32, operation::MPI_Op)
    ccall((:p4est_comm_sync_flag, libp4est), Int32, (Ptr{p4est_t}, Int32, MPI_Op), p4est, flag, operation)
end

function p4est_comm_checksum(p4est, local_crc::UInt32, local_bytes::UInt)
    ccall((:p4est_comm_checksum, libp4est), UInt32, (Ptr{p4est_t}, UInt32, UInt), p4est, local_crc, local_bytes)
end

function p4est_transfer_fixed(dest_gfq, src_gfq, mpicomm::MPI_Comm, tag::Int32, dest_data, src_data, data_size::UInt)
    ccall((:p4est_transfer_fixed, libp4est), Void, (Ptr{p4est_gloidx_t}, Ptr{p4est_gloidx_t}, MPI_Comm, Int32, Ptr{Void}, Ptr{Void}, UInt), dest_gfq, src_gfq, mpicomm, tag, dest_data, src_data, data_size)
end

function p4est_transfer_fixed_begin(dest_gfq, src_gfq, mpicomm::MPI_Comm, tag::Int32, dest_data, src_data, data_size::UInt)
    ccall((:p4est_transfer_fixed_begin, libp4est), Ptr{p4est_transfer_context_t}, (Ptr{p4est_gloidx_t}, Ptr{p4est_gloidx_t}, MPI_Comm, Int32, Ptr{Void}, Ptr{Void}, UInt), dest_gfq, src_gfq, mpicomm, tag, dest_data, src_data, data_size)
end

function p4est_transfer_fixed_end(tc)
    ccall((:p4est_transfer_fixed_end, libp4est), Void, (Ptr{p4est_transfer_context_t},), tc)
end

function p4est_transfer_custom(dest_gfq, src_gfq, mpicomm::MPI_Comm, tag::Int32, dest_data, dest_sizes, src_data, src_sizes)
    ccall((:p4est_transfer_custom, libp4est), Void, (Ptr{p4est_gloidx_t}, Ptr{p4est_gloidx_t}, MPI_Comm, Int32, Ptr{Void}, Ptr{Int32}, Ptr{Void}, Ptr{Int32}), dest_gfq, src_gfq, mpicomm, tag, dest_data, dest_sizes, src_data, src_sizes)
end

function p4est_transfer_custom_begin(dest_gfq, src_gfq, mpicomm::MPI_Comm, tag::Int32, dest_data, dest_sizes, src_data, src_sizes)
    ccall((:p4est_transfer_custom_begin, libp4est), Ptr{p4est_transfer_context_t}, (Ptr{p4est_gloidx_t}, Ptr{p4est_gloidx_t}, MPI_Comm, Int32, Ptr{Void}, Ptr{Int32}, Ptr{Void}, Ptr{Int32}), dest_gfq, src_gfq, mpicomm, tag, dest_data, dest_sizes, src_data, src_sizes)
end

function p4est_transfer_custom_end(tc)
    ccall((:p4est_transfer_custom_end, libp4est), Void, (Ptr{p4est_transfer_context_t},), tc)
end
# Julia wrapper for header: /home/iagbole/Codeentwicklung/p4est/local/include/p8est_vtk.h
# Automatically generated using Clang.jl wrap_c, version 0.0.0


function p8est_vtk_write_file(p8est, geom, filename)
    ccall((:p8est_vtk_write_file, libp4est), Void, (Ptr{p8est_t}, Ptr{p8est_geometry_t}, Cstring), p8est, geom, filename)
end

function p8est_vtk_context_new(p4est, filename)
    ccall((:p8est_vtk_context_new, libp4est), Ptr{p8est_vtk_context_t}, (Ptr{p8est_t}, Cstring), p4est, filename)
end

function p8est_vtk_context_set_geom(cont, geom)
    ccall((:p8est_vtk_context_set_geom, libp4est), Void, (Ptr{p8est_vtk_context_t}, Ptr{p8est_geometry_t}), cont, geom)
end

function p8est_vtk_context_set_scale(cont, scale::Float64)
    ccall((:p8est_vtk_context_set_scale, libp4est), Void, (Ptr{p8est_vtk_context_t}, Float64), cont, scale)
end

function p8est_vtk_context_set_continuous(cont, continuous::Int32)
    ccall((:p8est_vtk_context_set_continuous, libp4est), Void, (Ptr{p8est_vtk_context_t}, Int32), cont, continuous)
end

function p8est_vtk_context_destroy(context)
    ccall((:p8est_vtk_context_destroy, libp4est), Void, (Ptr{p8est_vtk_context_t},), context)
end

function p8est_vtk_write_header(cont)
    ccall((:p8est_vtk_write_header, libp4est), Ptr{p8est_vtk_context_t}, (Ptr{p8est_vtk_context_t},), cont)
end

function p8est_vtk_write_footer(cont)
    ccall((:p8est_vtk_write_footer, libp4est), Int32, (Ptr{p8est_vtk_context_t},), cont)
end
# Julia wrapper for header: /home/iagbole/Codeentwicklung/p4est/local/include/p8est_algorithms.h
# Automatically generated using Clang.jl wrap_c, version 0.0.0


function p8est_quadrant_init_data(p8est, which_tree::p4est_topidx_t, quad, init_fn::p8est_init_t)
    ccall((:p8est_quadrant_init_data, libp4est), Void, (Ptr{p8est_t}, p4est_topidx_t, Ptr{p8est_quadrant_t}, p8est_init_t), p8est, which_tree, quad, init_fn)
end

function p8est_quadrant_free_data(p8est, quad)
    ccall((:p8est_quadrant_free_data, libp4est), Void, (Ptr{p8est_t}, Ptr{p8est_quadrant_t}), p8est, quad)
end

function p8est_quadrant_checksum(quadrants, checkarray, first_quadrant::UInt)
    ccall((:p8est_quadrant_checksum, libp4est), UInt32, (Ptr{sc_array_t}, Ptr{sc_array_t}, UInt), quadrants, checkarray, first_quadrant)
end

function p8est_tree_is_sorted(tree)
    ccall((:p8est_tree_is_sorted, libp4est), Int32, (Ptr{p8est_tree_t},), tree)
end

function p8est_tree_is_linear(tree)
    ccall((:p8est_tree_is_linear, libp4est), Int32, (Ptr{p8est_tree_t},), tree)
end

function p8est_tree_is_complete(tree)
    ccall((:p8est_tree_is_complete, libp4est), Int32, (Ptr{p8est_tree_t},), tree)
end

function p8est_tree_is_almost_sorted(tree, check_linearity::Int32)
    ccall((:p8est_tree_is_almost_sorted, libp4est), Int32, (Ptr{p8est_tree_t}, Int32), tree, check_linearity)
end

function p8est_tree_print(log_priority::Int32, tree)
    ccall((:p8est_tree_print, libp4est), Void, (Int32, Ptr{p8est_tree_t}), log_priority, tree)
end

function p8est_is_equal(p8est1, p8est2, compare_data::Int32)
    ccall((:p8est_is_equal, libp4est), Int32, (Ptr{p8est_t}, Ptr{p8est_t}, Int32), p8est1, p8est2, compare_data)
end

function p8est_is_valid(p8est)
    ccall((:p8est_is_valid, libp4est), Int32, (Ptr{p8est_t},), p8est)
end

function p8est_tree_compute_overlap(p8est, _in, out, balance::p8est_connect_type_t, borders, inseeds)
    ccall((:p8est_tree_compute_overlap, libp4est), Void, (Ptr{p8est_t}, Ptr{sc_array_t}, Ptr{sc_array_t}, p8est_connect_type_t, Ptr{sc_array_t}, Ptr{sc_array_t}), p8est, _in, out, balance, borders, inseeds)
end

function p8est_tree_uniqify_overlap(out)
    ccall((:p8est_tree_uniqify_overlap, libp4est), Void, (Ptr{sc_array_t},), out)
end

function p8est_tree_remove_nonowned()
    ccall((:p8est_tree_remove_nonowned, libp4est), Int32, ())
end

function p8est_complete_region(p8est, q1, include_q1::Int32, q2, include_q2::Int32, tree, which_tree::p4est_topidx_t, init_fn::p8est_init_t)
    ccall((:p8est_complete_region, libp4est), Void, (Ptr{p8est_t}, Ptr{p8est_quadrant_t}, Int32, Ptr{p8est_quadrant_t}, Int32, Ptr{p8est_tree_t}, p4est_topidx_t, p8est_init_t), p8est, q1, include_q1, q2, include_q2, tree, which_tree, init_fn)
end

function p8est_complete_subtree(p8est, which_tree::p4est_topidx_t, init_fn::p8est_init_t)
    ccall((:p8est_complete_subtree, libp4est), Void, (Ptr{p8est_t}, p4est_topidx_t, p8est_init_t), p8est, which_tree, init_fn)
end

function p8est_balance_subtree(p8est, btype::p8est_connect_type_t, which_tree::p4est_topidx_t, init_fn::p8est_init_t)
    ccall((:p8est_balance_subtree, libp4est), Void, (Ptr{p8est_t}, p8est_connect_type_t, p4est_topidx_t, p8est_init_t), p8est, btype, which_tree, init_fn)
end

function p8est_balance_border(p8est, btype::p8est_connect_type_t, which_tree::p4est_topidx_t, init_fn::p8est_init_t, replace_fn::p8est_replace_t, borders)
    ccall((:p8est_balance_border, libp4est), Void, (Ptr{p8est_t}, p8est_connect_type_t, p4est_topidx_t, p8est_init_t, p8est_replace_t, Ptr{sc_array_t}), p8est, btype, which_tree, init_fn, replace_fn, borders)
end

function p8est_linearize_tree()
    ccall((:p8est_linearize_tree, libp4est), Int32, ())
end

function p8est_partition_correction(partition, num_procs::Int32, rank::Int32, min_quadrant_id::p4est_gloidx_t, max_quadrant_id::p4est_gloidx_t)
    ccall((:p8est_partition_correction, libp4est), p4est_locidx_t, (Ptr{p4est_gloidx_t}, Int32, Int32, p4est_gloidx_t, p4est_gloidx_t), partition, num_procs, rank, min_quadrant_id, max_quadrant_id)
end

function p8est_next_nonempty_process(rank::Int32, num_procs::Int32, num_quadrants_in_proc)
    ccall((:p8est_next_nonempty_process, libp4est), Int32, (Int32, Int32, Ptr{p4est_locidx_t}), rank, num_procs, num_quadrants_in_proc)
end

function p8est_partition_given(p8est, num_quadrants_in_proc)
    ccall((:p8est_partition_given, libp4est), p4est_gloidx_t, (Ptr{p8est_t}, Ptr{p4est_locidx_t}), p8est, num_quadrants_in_proc)
end
# Julia wrapper for header: /home/iagbole/Codeentwicklung/p4est/local/include/p8est_io.h
# Automatically generated using Clang.jl wrap_c, version 0.0.0


function p8est_deflate_quadrants(p8est, data)
    ccall((:p8est_deflate_quadrants, libp4est), Ptr{sc_array_t}, (Ptr{p8est_t}, Ptr{Ptr{sc_array_t}}), p8est, data)
end

function p8est_inflate(mpicomm::MPI_Comm, connectivity, global_first_quadrant, pertree, quadrants, data, user_pointer)
    ccall((:p8est_inflate, libp4est), Ptr{p8est_t}, (MPI_Comm, Ptr{p8est_connectivity_t}, Ptr{p4est_gloidx_t}, Ptr{p4est_gloidx_t}, Ptr{sc_array_t}, Ptr{sc_array_t}, Ptr{Void}), mpicomm, connectivity, global_first_quadrant, pertree, quadrants, data, user_pointer)
end
# Julia wrapper for header: /home/iagbole/Codeentwicklung/p4est/local/include/p8est_points.h
# Automatically generated using Clang.jl wrap_c, version 0.0.0


function p8est_new_points(mpicomm::MPI_Comm, connectivity, maxlevel::Int32, points, num_points::p4est_locidx_t, max_points::p4est_locidx_t, data_size::UInt, init_fn::p8est_init_t, user_pointer)
    ccall((:p8est_new_points, libp4est), Ptr{p8est_t}, (MPI_Comm, Ptr{p8est_connectivity_t}, Int32, Ptr{p8est_quadrant_t}, p4est_locidx_t, p4est_locidx_t, UInt, p8est_init_t, Ptr{Void}), mpicomm, connectivity, maxlevel, points, num_points, max_points, data_size, init_fn, user_pointer)
end
# Julia wrapper for header: /home/iagbole/Codeentwicklung/p4est/local/include/p6est_profile.h
# Automatically generated using Clang.jl wrap_c, version 0.0.0


function p6est_profile_new_local(p6est, ghost, ptype::p6est_profile_type_t, btype::p8est_connect_type_t, degree::Int32)
    ccall((:p6est_profile_new_local, libp4est), Ptr{p6est_profile_t}, (Ptr{p6est_t}, Ptr{p6est_ghost_t}, p6est_profile_type_t, p8est_connect_type_t, Int32), p6est, ghost, ptype, btype, degree)
end

function p6est_profile_destroy(profile)
    ccall((:p6est_profile_destroy, libp4est), Void, (Ptr{p6est_profile_t},), profile)
end

function p6est_profile_balance_local(profile)
    ccall((:p6est_profile_balance_local, libp4est), Void, (Ptr{p6est_profile_t},), profile)
end

function p6est_profile_sync(profile)
    ccall((:p6est_profile_sync, libp4est), Int32, (Ptr{p6est_profile_t},), profile)
end

function p6est_refine_to_profile(p6est, profile, init_fn::p6est_init_t, replace_fn::p6est_replace_t)
    ccall((:p6est_refine_to_profile, libp4est), Void, (Ptr{p6est_t}, Ptr{p6est_profile_t}, p6est_init_t, p6est_replace_t), p6est, profile, init_fn, replace_fn)
end

function p6est_profile_element_to_node(p6est, profile, offsets, elem_to_node, fc)
    ccall((:p6est_profile_element_to_node, libp4est), Void, (Ptr{p6est_t}, Ptr{p6est_profile_t}, Ptr{p4est_locidx_t}, Ptr{p4est_locidx_t}, Ptr{p6est_lnodes_code_t}), p6est, profile, offsets, elem_to_node, fc)
end
# Julia wrapper for header: /home/iagbole/Codeentwicklung/p4est/local/include/p4est_balance.h
# Automatically generated using Clang.jl wrap_c, version 0.0.0


function p4est_balance_seeds(q, p, balance::p4est_connect_type_t, seeds)
    ccall((:p4est_balance_seeds, libp4est), Int32, (Ptr{p4est_quadrant_t}, Ptr{p4est_quadrant_t}, p4est_connect_type_t, Ptr{sc_array_t}), q, p, balance, seeds)
end

function p4est_balance_seeds_face(q, p, face::Int32, balance::p4est_connect_type_t, seeds)
    ccall((:p4est_balance_seeds_face, libp4est), Int32, (Ptr{p4est_quadrant_t}, Ptr{p4est_quadrant_t}, Int32, p4est_connect_type_t, Ptr{sc_array_t}), q, p, face, balance, seeds)
end

function p4est_balance_seeds_corner(q, p, face::Int32, balance::p4est_connect_type_t, seeds)
    ccall((:p4est_balance_seeds_corner, libp4est), Int32, (Ptr{p4est_quadrant_t}, Ptr{p4est_quadrant_t}, Int32, p4est_connect_type_t, Ptr{sc_array_t}), q, p, face, balance, seeds)
end
# Julia wrapper for header: /home/iagbole/Codeentwicklung/p4est/local/include/p4est_io.h
# Automatically generated using Clang.jl wrap_c, version 0.0.0


function p4est_deflate_quadrants(p4est, data)
    ccall((:p4est_deflate_quadrants, libp4est), Ptr{sc_array_t}, (Ptr{p4est_t}, Ptr{Ptr{sc_array_t}}), p4est, data)
end

function p4est_inflate(mpicomm::MPI_Comm, connectivity, global_first_quadrant, pertree, quadrants, data, user_pointer)
    ccall((:p4est_inflate, libp4est), Ptr{p4est_t}, (MPI_Comm, Ptr{p4est_connectivity_t}, Ptr{p4est_gloidx_t}, Ptr{p4est_gloidx_t}, Ptr{sc_array_t}, Ptr{sc_array_t}, Ptr{Void}), mpicomm, connectivity, global_first_quadrant, pertree, quadrants, data, user_pointer)
end
# Julia wrapper for header: /home/iagbole/Codeentwicklung/p4est/local/include/p4est_points.h
# Automatically generated using Clang.jl wrap_c, version 0.0.0


function p4est_new_points(mpicomm::MPI_Comm, connectivity, maxlevel::Int32, points, num_points::p4est_locidx_t, max_points::p4est_locidx_t, data_size::UInt, init_fn::p4est_init_t, user_pointer)
    ccall((:p4est_new_points, libp4est), Ptr{p4est_t}, (MPI_Comm, Ptr{p4est_connectivity_t}, Int32, Ptr{p4est_quadrant_t}, p4est_locidx_t, p4est_locidx_t, UInt, p4est_init_t, Ptr{Void}), mpicomm, connectivity, maxlevel, points, num_points, max_points, data_size, init_fn, user_pointer)
end
# Julia wrapper for header: /home/iagbole/Codeentwicklung/p4est/local/include/p6est_extended.h
# Automatically generated using Clang.jl wrap_c, version 0.0.0


function p6est_new_ext(mpicomm::MPI_Comm, connectivity, min_quadrants::p4est_locidx_t, min_level::Int32, min_zlevel::Int32, num_zroot::Int32, fill_uniform::Int32, data_size::UInt, init_fn::p6est_init_t, user_pointer)
    ccall((:p6est_new_ext, libp4est), Ptr{p6est_t}, (MPI_Comm, Ptr{p6est_connectivity_t}, p4est_locidx_t, Int32, Int32, Int32, Int32, UInt, p6est_init_t, Ptr{Void}), mpicomm, connectivity, min_quadrants, min_level, min_zlevel, num_zroot, fill_uniform, data_size, init_fn, user_pointer)
end

function p6est_copy_ext(input, copy_data::Int32, duplicate_mpicomm::Int32)
    ccall((:p6est_copy_ext, libp4est), Ptr{p6est_t}, (Ptr{p6est_t}, Int32, Int32), input, copy_data, duplicate_mpicomm)
end

function p6est_save_ext(filename, p6est, save_data::Int32, save_partition::Int32)
    ccall((:p6est_save_ext, libp4est), Void, (Cstring, Ptr{p6est_t}, Int32, Int32), filename, p6est, save_data, save_partition)
end

function p6est_load_ext(filename, mpicomm::MPI_Comm, data_size::UInt, load_data::Int32, autopartition::Int32, broadcasthead::Int32, user_pointer, connectivity)
    ccall((:p6est_load_ext, libp4est), Ptr{p6est_t}, (Cstring, MPI_Comm, UInt, Int32, Int32, Int32, Ptr{Void}, Ptr{Ptr{p6est_connectivity_t}}), filename, mpicomm, data_size, load_data, autopartition, broadcasthead, user_pointer, connectivity)
end

function p6est_refine_columns_ext(p6est, refine_recursive::Int32, maxlevel::Int32, refine_fn::p6est_refine_column_t, init_fn::p6est_init_t, replace_fn::p6est_replace_t)
    ccall((:p6est_refine_columns_ext, libp4est), Void, (Ptr{p6est_t}, Int32, Int32, p6est_refine_column_t, p6est_init_t, p6est_replace_t), p6est, refine_recursive, maxlevel, refine_fn, init_fn, replace_fn)
end

function p6est_refine_layers_ext(p6est, refine_recursive::Int32, maxlevel::Int32, refine_fn::p6est_refine_layer_t, init_fn::p6est_init_t, replace_fn::p6est_replace_t)
    ccall((:p6est_refine_layers_ext, libp4est), Void, (Ptr{p6est_t}, Int32, Int32, p6est_refine_layer_t, p6est_init_t, p6est_replace_t), p6est, refine_recursive, maxlevel, refine_fn, init_fn, replace_fn)
end

function p6est_coarsen_columns_ext(p6est, coarsen_recursive::Int32, callback_orphans::Int32, coarsen_fn::p6est_coarsen_column_t, init_fn::p6est_init_t, replace_fn::p6est_replace_t)
    ccall((:p6est_coarsen_columns_ext, libp4est), Void, (Ptr{p6est_t}, Int32, Int32, p6est_coarsen_column_t, p6est_init_t, p6est_replace_t), p6est, coarsen_recursive, callback_orphans, coarsen_fn, init_fn, replace_fn)
end

function p6est_coarsen_layers_ext(p6est, coarsen_recursive::Int32, callback_orphans::Int32, coarsen_fn::p6est_coarsen_layer_t, init_fn::p6est_init_t, replace_fn::p6est_replace_t)
    ccall((:p6est_coarsen_layers_ext, libp4est), Void, (Ptr{p6est_t}, Int32, Int32, p6est_coarsen_layer_t, p6est_init_t, p6est_replace_t), p6est, coarsen_recursive, callback_orphans, coarsen_fn, init_fn, replace_fn)
end

function p6est_partition_ext(p6est, partition_for_coarsening::Int32, weight_fn::p6est_weight_t)
    ccall((:p6est_partition_ext, libp4est), p4est_gloidx_t, (Ptr{p6est_t}, Int32, p6est_weight_t), p6est, partition_for_coarsening, weight_fn)
end

function p6est_balance_ext(p6est, btype::p8est_connect_type_t, max_diff::Int32, min_diff::Int32, init_fn::p6est_init_t, replace_fn::p6est_replace_t)
    ccall((:p6est_balance_ext, libp4est), Void, (Ptr{p6est_t}, p8est_connect_type_t, Int32, Int32, p6est_init_t, p6est_replace_t), p6est, btype, max_diff, min_diff, init_fn, replace_fn)
end
