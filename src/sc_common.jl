# Automatically generated using Clang.jl wrap_c, version 0.0.0

using Compat

# begin enum ANONYMOUS_1
const ANONYMOUS_1 = UInt32
const SC_TAG_FIRST = (UInt32)(214)
const SC_TAG_AG_ALLTOALL = (UInt32)(214)
const SC_TAG_AG_RECURSIVE_A = (UInt32)(215)
const SC_TAG_AG_RECURSIVE_B = (UInt32)(216)
const SC_TAG_AG_RECURSIVE_C = (UInt32)(217)
const SC_TAG_NOTIFY_RECURSIVE = (UInt32)(218)
const SC_TAG_REDUCE = (UInt32)(250)
const SC_TAG_PSORT_LO = (UInt32)(251)
const SC_TAG_PSORT_HI = (UInt32)(252)
const SC_TAG_LAST = (UInt32)(253)
# end enum ANONYMOUS_1

const sc_tag_t = Void
const sc_hash_function_t = Ptr{Void}
const sc_equal_function_t = Ptr{Void}
const sc_hash_foreach_t = Ptr{Void}

mutable struct sc_array_t
    elem_size::Int32
    elem_count::Int32
    byte_alloc::Cssize_t
    array::Cstring
end

mutable struct sc_mempool_t
    elem_size::UInt
    elem_count::UInt
    zero_and_persist::Int32
    obstack::Void
    freed::sc_array_t
end

mutable struct sc_link_t
    data::Ptr{Void}
    next::Ptr{Void}
end

mutable struct sc_list_t
    elem_count::UInt
    first::Ptr{sc_link_t}
    last::Ptr{sc_link_t}
    allocator_owned::Int32
    allocator::Ptr{sc_mempool_t}
end

mutable struct sc_hash_t
    elem_count::UInt
    slots::Ptr{sc_array_t}
    user_data::Ptr{Void}
    hash_fn::sc_hash_function_t
    equal_fn::sc_equal_function_t
    resize_checks::UInt
    resize_actions::UInt
    allocator_owned::Int32
    allocator::Ptr{sc_mempool_t}
end

mutable struct sc_hash_array_data_t
    pa::Ptr{sc_array_t}
    hash_fn::sc_hash_function_t
    equal_fn::sc_equal_function_t
    user_data::Ptr{Void}
    current_item::Ptr{Void}
end

mutable struct sc_hash_array_t
    a::sc_array_t
    internal_data::sc_hash_array_data_t
    h::Ptr{sc_hash_t}
end

mutable struct sc_recycle_array_t
    elem_count::UInt
    a::sc_array_t
    f::sc_array_t
end

const sc_keyvalue_entry_type_t = Void

mutable struct sc_keyvalue_t
end

const sc_keyvalue_foreach_t = Ptr{Void}

mutable struct sc_statinfo_t
    dirty::Int32
    count::Int64
    sum_values::Float64
    sum_squares::Float64
    min::Float64
    max::Float64
    min_at_rank::Int32
    max_at_rank::Int32
    average::Float64
    variance::Float64
    standev::Float64
    variance_mean::Float64
    standev_mean::Float64
    variable::Cstring
end

mutable struct sc_stats_t
    mpicomm::MPI_Comm
    kv::Ptr{sc_keyvalue_t}
    sarray::Ptr{sc_array_t}
end

const sc_statistics_t = Void
const sc_bint_t = Int32
const sc_buint_t = UInt32

# begin enum sc_trans
const sc_trans = UInt32
const SC_NO_TRANS = (UInt32)(0)
const SC_TRANS = (UInt32)(1)
const SC_TRANS_ANCHOR = (UInt32)(2)
# end enum sc_trans

const sc_trans_t = Void

# begin enum sc_uplo
const sc_uplo = UInt32
const SC_UPPER = (UInt32)(0)
const SC_LOWER = (UInt32)(1)
const SC_UPLO_ANCHOR = (UInt32)(2)
# end enum sc_uplo

const sc_uplo_t = Void

# begin enum sc_cmach
const sc_cmach = UInt32
const SC_CMACH_EPS = (UInt32)(0)
const SC_CMACH_SFMIN = (UInt32)(1)
const SC_CMACH_BASE = (UInt32)(2)
const SC_CMACH_PREC = (UInt32)(3)
const SC_CMACH_T = (UInt32)(4)
const SC_CMACH_RND = (UInt32)(5)
const SC_CMACH_EMIN = (UInt32)(6)
const SC_CMACH_RMIN = (UInt32)(7)
const SC_CMACH_EMAX = (UInt32)(8)
const SC_CMACH_RMAX = (UInt32)(9)
const SC_CMACH_ANCHOR = (UInt32)(10)
# end enum sc_cmach

const sc_cmach_t = Void

mutable struct sc_dmatrix_t
    e::Ptr{Ptr{Float64}}
    m::sc_bint_t
    n::sc_bint_t
    view::Int32
end

mutable struct sc_dmatrix_pool_t
    m::sc_bint_t
    n::sc_bint_t
    elem_count::UInt
    freed::sc_array_t
end

mutable struct sc_darray_work_t
    data::Ptr{Float64}
    n_threads::Int32
    n_blocks::Int32
    n_entries::Int32
end

mutable struct sc_unique_counter_t
    start_value::Int32
    mempool::Ptr{sc_mempool_t}
end

mutable struct sc_warp_interval_t
end

mutable struct sc_amr_control_t
    errors::Ptr{Float64}
    estats::sc_statinfo_t
    mpicomm::MPI_Comm
    num_procs_long::Int64
    num_total_elements::Int64
    coarsen_threshold::Float64
    refine_threshold::Float64
    num_total_coarsen::Int64
    num_total_refine::Int64
    num_total_estimated::Int64
end

const sc_amr_count_coarsen_fn = Ptr{Void}
const sc_amr_count_refine_fn = Ptr{Void}
const sc_io_error_t = Void
const sc_io_mode_t = Void
const sc_io_encode_t = Void
const sc_io_type_t = Void

mutable struct sc_io_sink_t
    iotype::sc_io_type_t
    mode::sc_io_mode_t
    encode::sc_io_encode_t
    buffer::Ptr{sc_array_t}
    buffer_bytes::UInt
    file::Ptr{FILE}
    bytes_in::UInt
    bytes_out::UInt
end

mutable struct sc_io_source_t
    iotype::sc_io_type_t
    encode::sc_io_encode_t
    buffer::Ptr{sc_array_t}
    buffer_bytes::UInt
    file::Ptr{FILE}
    bytes_in::UInt
    bytes_out::UInt
    mirror::Ptr{sc_io_sink_t}
    mirror_buffer::Ptr{sc_array_t}
end

const sc_function1_t = Ptr{Void}
const sc_function3_t = Ptr{Void}

mutable struct sc_function3_meta_t
    f1::sc_function3_t
    f2::sc_function3_t
    parameter2::Float64
    f3::sc_function3_t
    data::Ptr{Void}
end

mutable struct sc_refcount_t
    package_id::Int32
    refcount::Int32
end

const sc_shmem_type_t = Void

mutable struct sc_options_t
end

const sc_options_callback_t = Ptr{Void}
const avl_compare_t = Ptr{Void}
const avl_freeitem_t = Ptr{Void}
const avl_foreach_t = Ptr{Void}

mutable struct avl_node_t
    next::Ptr{Void}
    prev::Ptr{Void}
    parent::Ptr{Void}
    left::Ptr{Void}
    right::Ptr{Void}
    item::Ptr{Void}
    count::UInt32
end

mutable struct avl_tree_t
    head::Ptr{avl_node_t}
    tail::Ptr{avl_node_t}
    top::Ptr{avl_node_t}
    cmp::avl_compare_t
    freeitem::avl_freeitem_t
end

const sc_bspline_t = Void

mutable struct sc_flopinfo_t
    seconds::Float64
    cwtime::Float64
    crtime::Cfloat
    cptime::Cfloat
    cflpops::Int64
    iwtime::Float64
    irtime::Cfloat
    iptime::Cfloat
    iflpops::Int64
    mflops::Cfloat
end

# begin enum sc_jobz
const sc_jobz = UInt32
const SC_EIGVALS_ONLY = (UInt32)(0)
const SC_EIGVALS_AND_EIGVECS = (UInt32)(1)
const SC_JOBZ_ANCHOR = (UInt32)(2)
# end enum sc_jobz

const sc_jobz_t = Void
const sc_reduce_t = Ptr{Void}

mutable struct sc_string_t
    printed::Int32
    buffer::NTuple{4088, UInt8}
end
